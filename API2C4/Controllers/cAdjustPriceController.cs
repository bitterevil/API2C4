﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API2C4.Models.WebService.Response.Base;
using API2C4.Models.WebService.Response.AdjustPrice.Insert;
using API2C4.Models.WebService.Request.AdjustPrice;
using API2C4.Models.WebService.Response.AdjustPrice.Cancle;
using API2C4.Class.Standard;
using System.Text;
using API2C4.Class;
using API2C4.Models.Database;
using System.Threading;
using System.Globalization;
using System.Reflection;
using API2C4.Class.Product;
using API2C4.Class.AdjustPrice;
using API2PosV4Master.Class.Standard;
using System.Web;

namespace API2C4.Controllers
{
    /// <summary>
    /// Magnage AdjustPrice
    /// </summary>
    [RoutePrefix("V1/AdjustPrice")]
    public class cAdjustPriceController : ApiController
    {
        /// <summary>
        /// Insert adjust price List. 
        /// </summary>
        /// <param name="poParam"> Adjusct price Information.</param>
        /// <returns>1 : success. <br/>
        ///          701 : validate parameter model false. <br/>
        ///          704 : branch code not found in branch code master. <br/>
        ///          705 : zone code not found in zone code master. <br/>
        ///          706 : barcode duplicate in this document. <br/>
        ///          707 : product code duplicate in this document. <br/>
        ///          708 : product unit not found in unit master. <br/>
        ///          800 : data not found (product code).  <br/>
        ///          900 : service process false. <br/>
        ///          904 : key not allowed to use method. <br/>
        ///          905 : cannot connect database. <br/>
        ///          906 : this time not allowed to use method. <br/>
        /// </returns>
        [Route("Insert/List")]
        [HttpPost]
        public cmlResList<cmlResPdtAdjInsList> POST_AJPoInsAdjustPrice([FromBody] cmlReqPdtAdj poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;
            cProduct oPdt;
            StringBuilder oSql;
            cDatabase oDatabase;
            cAdjustPrice oAdp;
            List<cmlTSysConfig> aoSysConfig;

            cmlResList<cmlResPdtAdjInsList> oResponse;
            List<cmlResPdtAdjInsList> oAddResponse;
            cmlResPdtInsList oItemAdj;
            List<cmlResPdtInsList> aoAdjItemDT;
            cmlResPdtAdjInsInto oResTitle;
            List<cmlResPdtAdjInsIntoExe> aoExe;
            cmlResPdtAdjInsList oResHeader;
            List<cmlResPdtAdjInsInto> aoListTitle;


            bool bMaster = true; //Check สถานะ Error master

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tAjpHD;

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oPdt = new cProduct();
                oAdp = new cAdjustPrice();
                oDatabase = new cDatabase();
                aoSysConfig = new List<cmlTSysConfig>();
               
                //For Response
                aoExe = new List<cmlResPdtAdjInsIntoExe>();
                oAddResponse = new List<cmlResPdtAdjInsList>();
                oResHeader = new cmlResPdtAdjInsList();
                aoListTitle = new List<cmlResPdtAdjInsInto>();
                oResponse = new cmlResList<cmlResPdtAdjInsList>();
                bool bStatus = true;
                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Check model error
                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            try
                            {
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                                foreach (var oArr in poParam.paoPdtAdjInsList)
                                {
                                    var oAjpList = oArr as cmlReqPdtAdjInfo;
                                    if (oAdp.C_CHKbChekDataAdjust(poParam, oAjpList) == false)
                                    {
                                        oResTitle = new cmlResPdtAdjInsInto();
                                        oResTitle.rtCode = oMsg.tMS_RespCode709Adjust;
                                        oResTitle.rtDesc = oMsg.tMS_RespDesc709Adjust;
                                        oItemAdj = new cmlResPdtInsList();
                                        aoAdjItemDT = new List<cmlResPdtInsList>();
                                        aoAdjItemDT.Add(oItemAdj);
                                        oResTitle.raoPdtList = aoAdjItemDT;
                                        aoListTitle.Add(oResTitle);

                                        oResHeader.rtIphDocRef = poParam.ptIphDocRef;
                                        oResHeader.rdIphAffect = poParam.pdIphAffect;
                                        oResHeader.rtIphBchTo = poParam.ptIphBchTo;
                                        oResHeader.raoPdtAdjList = aoListTitle;
                                        oAddResponse.Add(oResHeader);
                                        oResponse.roItem = oAddResponse;
                                        oAdp.C_DATxDropTableTmp();
                                        return oResponse;

                                    }

                                }

                                // Check Error ทั้งหมด 

                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTpdtCode AS ptPdtCode ,FtpdtBarcode AS ptPdtBarCode ,");
                                oSql.AppendLine(" FTIphDocRefRmk AS ptIphDocRefRmkE , FTErrorBchCode AS ptErrorBchCode, ");
                                oSql.AppendLine(" FTErrorZneCode AS ptErrorZneCode, FTErrorBarcodeDup AS ptErrorBarcodeDup, ");
                                oSql.AppendLine(" FTErrorProductDup AS ptErrorProductDup, FTErrorPdtUnit AS ptErrorPdtUnit,");
                                oSql.AppendLine(" FTErrorData AS ptErrorData , FTErrorPdtNotFound AS ptPdtNotFound");
                                oSql.AppendLine(" FROM     TCNTPdtAjpTmp ");
                                oSql.AppendLine(" WHERE (FTErrorBchCode IS NOT NULL OR FTErrorZneCode IS NOT NULL OR FTErrorBarcodeDup IS NOT NULL OR");
                                oSql.AppendLine(" FTErrorProductDup IS NOT NULL OR  FTErrorPdtUnit IS NOT NULL OR  FTErrorData IS NOT NULL ");
                                oSql.AppendLine(" OR FTErrorPdtNotFound IS NOT NULL )");
                                aoExe = oDatabase.C_DATaSqlQuery<cmlResPdtAdjInsIntoExe>(oSql.ToString());

                                if (aoExe != null && aoExe.Count > 0)
                                {
                                    bStatus = false;

                                    // กรณีที่ Product not found จะ Reject ค่า ทันที และไม่นำเข้าทั้งเอกสาร
                                    var oPdtNotfound = (from oItem in aoExe
                                                        where oItem.ptPdtNotFound != null
                                                        select oItem).ToList();
                                    if (oPdtNotfound.Count != 0)
                                    {
                                        oResTitle = new cmlResPdtAdjInsInto();
                                        oResTitle.rtCode = oMsg.tMS_RespCode710Adjust;
                                        oResTitle.rtDesc = oMsg.tMS_RespDesc710Adjust;

                                        for (int nRow = 0; nRow < oPdtNotfound.Count(); nRow++)
                                        {
                                            oItemAdj = new cmlResPdtInsList();
                                            aoAdjItemDT = new List<cmlResPdtInsList>();
                                            oItemAdj.rtPdtCode = oPdtNotfound[nRow].ptPdtCode;
                                            oItemAdj.rtIpdBarCode = oPdtNotfound[nRow].ptPdtBarCode;
                                            aoAdjItemDT.Add(oItemAdj);
                                            oResTitle.raoPdtList = aoAdjItemDT;
                                        }
                                        aoListTitle.Add(oResTitle);


                                    }

                                    var oBchErr = (from oItem in aoExe
                                                   where oItem.ptErrorBchCode != null
                                                   select oItem).ToList();
                                    if (oBchErr.Count != 0)
                                    {
                                        oResTitle = new cmlResPdtAdjInsInto();
                                        oResTitle.rtCode = oMsg.tMS_RespCode704Adjust;
                                        oResTitle.rtDesc = oMsg.tMS_RespDesc704Adjust;

                                        for (int nRow = 0; nRow < oBchErr.Count(); nRow++)
                                        {
                                            oItemAdj = new cmlResPdtInsList();
                                            aoAdjItemDT = new List<cmlResPdtInsList>();
                                            oItemAdj.rtPdtCode = oBchErr[nRow].ptPdtCode;
                                            oItemAdj.rtIpdBarCode = oBchErr[nRow].ptPdtBarCode;
                                            aoAdjItemDT.Add(oItemAdj);
                                            oResTitle.raoPdtList = aoAdjItemDT;
                                        }
                                        aoListTitle.Add(oResTitle);
                                    }

                                    var oZneErr = (from oItem in aoExe
                                                   where oItem.ptErrorZneCode != null
                                                   select oItem).ToList();
                                    if (oZneErr.Count != 0)
                                    {
                                        oResTitle = new cmlResPdtAdjInsInto();
                                        oResTitle.rtCode = oMsg.tMS_RespCode705Adjust;
                                        oResTitle.rtDesc = oMsg.tMS_RespDesc705Adjust;

                                        for (int nRow = 0; nRow < oZneErr.Count(); nRow++)
                                        {
                                            oItemAdj = new cmlResPdtInsList();
                                            aoAdjItemDT = new List<cmlResPdtInsList>();
                                            oItemAdj.rtPdtCode = oZneErr[nRow].ptPdtCode;
                                            oItemAdj.rtIpdBarCode = oZneErr[nRow].ptPdtBarCode;
                                            aoAdjItemDT.Add(oItemAdj);
                                            oResTitle.raoPdtList = aoAdjItemDT;
                                        }
                                        aoListTitle.Add(oResTitle);
                                    }

                                    var oBarcodeDup = (from oItem in aoExe
                                                       where oItem.ptErrorBarcodeDup != null
                                                       select oItem).ToList();
                                    if (oBarcodeDup.Count != 0)
                                    {
                                        oResTitle = new cmlResPdtAdjInsInto();
                                        aoAdjItemDT = new List<cmlResPdtInsList>();
                                        oResTitle.rtCode = oMsg.tMS_RespCode706Adjust;
                                        oResTitle.rtDesc = oMsg.tMS_RespDesc706Adjust;

                                        for (int nRow = 0; nRow < oBarcodeDup.Count(); nRow++)
                                        {
                                            oItemAdj = new cmlResPdtInsList();
                                            oItemAdj.rtPdtCode = oBarcodeDup[nRow].ptPdtCode;
                                            oItemAdj.rtIpdBarCode = oBarcodeDup[nRow].ptPdtBarCode;
                                            aoAdjItemDT.Add(oItemAdj);
                                            oResTitle.raoPdtList = aoAdjItemDT;
                                        }
                                        aoListTitle.Add(oResTitle);
                                    }

                                    var oPdtUnit = (from oItem in aoExe
                                                    where oItem.ptErrorPdtUnit != null
                                                    select oItem).ToList();
                                    if (oPdtUnit.Count != 0)
                                    {
                                        oResTitle = new cmlResPdtAdjInsInto();
                                        aoAdjItemDT = new List<cmlResPdtInsList>();
                                        oResTitle.rtCode = oMsg.tMS_RespCode708Adjust;
                                        oResTitle.rtDesc = oMsg.tMS_RespDesc708Adjust;

                                        for (int nRow = 0; nRow < oPdtUnit.Count(); nRow++)
                                        {
                                            oItemAdj = new cmlResPdtInsList();
                                            oItemAdj.rtPdtCode = oPdtUnit[nRow].ptPdtCode;
                                            oItemAdj.rtIpdBarCode = oPdtUnit[nRow].ptPdtBarCode;
                                            aoAdjItemDT.Add(oItemAdj);
                                            oResTitle.raoPdtList = aoAdjItemDT;
                                        }
                                        aoListTitle.Add(oResTitle);
                                    }

                                    var oErrorData = (from oItem in aoExe
                                                      where oItem.ptErrorData != null
                                                      select oItem).ToList();
                                    if (oErrorData.Count != 0)
                                    {
                                        oResTitle = new cmlResPdtAdjInsInto();
                                        aoAdjItemDT = new List<cmlResPdtInsList>();
                                        oResTitle.rtCode = oMsg.tMS_RespCode800;
                                        oResTitle.rtDesc = oMsg.tMS_RespDesc800;

                                        for (int nRow = 0; nRow < oErrorData.Count(); nRow++)
                                        {
                                            oItemAdj = new cmlResPdtInsList();

                                            oItemAdj.rtPdtCode = oErrorData[nRow].ptPdtCode;
                                            oItemAdj.rtIpdBarCode = oErrorData[nRow].ptPdtBarCode;
                                            aoAdjItemDT.Add(oItemAdj);
                                            oResTitle.raoPdtList = aoAdjItemDT;
                                        }
                                        aoListTitle.Add(oResTitle);
                                    }
                                }
                                oDatabase.C_DATxConnectiosClose();


                                if (bStatus == true)
                                {
                                    oAdp.C_DATbInsAjpDT();   //Insert ข้อมูลจาก Temp ลง Detail
                                    oAdp.C_DATbInsAjpHD(); // Insert  ข้อมูลจาก Temp ลง Header
                                    if (poParam.ptIphPriType != "2")
                                    {
                                        oSql = new StringBuilder();
                                        oSql.AppendLine(" SELECT FTIphStaDoc  FROM TCNTPdtAjpHD ");
                                        oSql.AppendLine(" WHERE FTIphRmk = '" + poParam.ptIphDocRef + "' ");
                                        oSql.AppendLine(" AND FTIphStaDoc = N'1' ");
                                        oSql.AppendLine(" AND FTIphStaPrcDoc = N'1' ");
                                        oSql.AppendLine(" AND FTIphStaPrcPri = N'1' ");
                                        tAjpHD = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                        if (!string.IsNullOrEmpty(tAjpHD))
                                        {
                                            oAdp.C_DATbUpdateProduct(); // ปรับราคา ที่ Master
                                        }
                                    }

                                    oResTitle = new cmlResPdtAdjInsInto();
                                    oResTitle.rtCode = oMsg.tMS_RespCode1;
                                    oResTitle.rtDesc = oMsg.tMS_RespDesc1;
                                    oItemAdj = new cmlResPdtInsList();
                                    aoAdjItemDT = new List<cmlResPdtInsList>();
                                    aoAdjItemDT.Add(oItemAdj);
                                    oResTitle.raoPdtList = aoAdjItemDT;
                                    aoListTitle.Add(oResTitle);
                                }

                                oAdp.C_DATxDropTableTmp();

                            }
                            catch (Exception oExn)
                            {
                                oResTitle = new cmlResPdtAdjInsInto();
                                oResTitle.rtCode = oMsg.tMS_RespCode900;
                                oResTitle.rtDesc = oMsg.tMS_RespDesc900;
                                oItemAdj = new cmlResPdtInsList();
                                aoAdjItemDT = new List<cmlResPdtInsList>();
                                aoAdjItemDT.Add(oItemAdj);
                                oResTitle.raoPdtList = aoAdjItemDT;
                                aoListTitle.Add(oResTitle);
                            }

                        }
                        else
                        {
                            oResTitle = new cmlResPdtAdjInsInto();
                            oResTitle.rtCode = oMsg.tMS_RespCode904;
                            oResTitle.rtDesc = oMsg.tMS_RespDesc904;
                            oItemAdj = new cmlResPdtInsList();
                            aoAdjItemDT = new List<cmlResPdtInsList>();
                            aoAdjItemDT.Add(oItemAdj);
                            oResTitle.raoPdtList = aoAdjItemDT;
                            aoListTitle.Add(oResTitle);
                        }
                    }
                    else
                    {
                        oResTitle = new cmlResPdtAdjInsInto();
                        oResTitle.rtCode = oMsg.tMS_RespCode906;
                        oResTitle.rtDesc = oMsg.tMS_RespDesc906;
                        oItemAdj = new cmlResPdtInsList();
                        aoAdjItemDT = new List<cmlResPdtInsList>();
                        aoAdjItemDT.Add(oItemAdj);
                        oResTitle.raoPdtList = aoAdjItemDT;
                        aoListTitle.Add(oResTitle);
                    }
                 
                }
                else
                {
                    oResTitle = new cmlResPdtAdjInsInto();
                    oResTitle.rtCode = oMsg.tMS_RespCode701;
                    oResTitle.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    oItemAdj = new cmlResPdtInsList();
                    aoAdjItemDT = new List<cmlResPdtInsList>();
                    aoAdjItemDT.Add(oItemAdj);
                    oResTitle.raoPdtList = aoAdjItemDT;
                    aoListTitle.Add(oResTitle);
                }

                oResHeader.rtIphDocRef = poParam.ptIphDocRef;
                oResHeader.rdIphAffect = poParam.pdIphAffect;
                oResHeader.rtIphBchTo = poParam.ptIphBchTo;
                oResHeader.raoPdtAdjList = aoListTitle;
                oAddResponse.Add(oResHeader);
                oResponse.roItem = oAddResponse;
                return oResponse;
            }

            catch (Exception oExn)
            {
               
                oAdp = new cAdjustPrice();
                oMsg = new cMS();
                aoListTitle = new List<cmlResPdtAdjInsInto>();
                oResponse = new cmlResList<cmlResPdtAdjInsList>();
                oResTitle = new cmlResPdtAdjInsInto();
                oAdp.C_DATxDropTableTmp();
                oResTitle.rtCode = oMsg.tMS_RespCode900;
                oResTitle.rtDesc = oMsg.tMS_RespDesc900;
                oItemAdj = new cmlResPdtInsList();
                aoAdjItemDT = new List<cmlResPdtInsList>();
                aoAdjItemDT.Add(oItemAdj);
                oResTitle.raoPdtList = aoAdjItemDT;
                aoListTitle.Add(oResTitle);
               
                return oResponse;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oCons = null;
                oPdt = null;
                oAdp = null;
                oDatabase = null;
                aoSysConfig = null;

                aoExe = null;
                oAddResponse = null;
                oResHeader = null;
                aoListTitle = null;
                oResponse = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

    }
}

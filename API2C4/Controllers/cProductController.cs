﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API2C4.Class.Standard;
using API2C4.Models;
using API2C4.Class.Product;
using API2C4.Models.Database;
using API2C4.Models.WebService.Response.Product.Insert;
using API2C4.Models.WebService.Response.Product.Update;
using API2C4.Models.WebService.Request.Product.Update;
using API2C4.Models.WebService.Request.Product.Barcode;
using API2C4.Models.WebService.Response.Product.Barcode;
using API2C4.Models.WebService.Request.Product.Delete;
using API2C4.Models.WebService.Request.Product.StatusActive;
using API2C4.Models.WebService.Response.Product.StatusActive;
using API2C4.Models.WebService.Request.Product.Image;
using API2C4.Models.WebService.Response.Product.Image;
using API2C4.Models.WebService.Request.Product;
using API2C4.Models.WebService.Response.Base;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Reflection;
using System.Web;
using API2C4.Class;
using System.Data;
using API2C4.Models.WebService.Request.Product.Insert;
using API2PosV4Master.Class.Standard;

namespace API2C4.Controllers
{
    /// <summary>
    /// Management product information.
    /// </summary>
    
    [RoutePrefix("V1/Product")]
    public class cProductController : ApiController
    {
        /// <summary>
        /// Insert product list.
        /// </summary>
        /// <param name="poParam">Product Information.</param>
        /// <returns> 1 : success. <br/>
        ///         701 : validate parameter model false. <br/>
        ///         702 : barcode  duplicate in yourself. <br/>
        ///         703 : product  unit not found in unit master. <br/>
        ///         704 : product  type not found in type master. <br/>
        ///         705 : product  brand not found in brand master. <br/>
        ///         706 : product  DCS not found in DCS master. <br/>
        ///         707 : product  depart not found in depart master. <br/>
        ///         708 : product  class not found in class master. <br/>
        ///         709 : product  sub class not found in sub class master. <br/>
        ///         900 : service process false. <br/> 
        ///         904 : key not allowed to use method. <br/>
        ///         905 : cannot connect database. <br/>
        ///         906 : this time not allowed to use method. <br/>
        ///</returns>
        ///
        [Route("Insert/ListMultiBar")]
        [HttpPost]
        public cmlResList<cmlResPdtInsListBarInto> POST_PDToInsProductList([FromBody] cmlReqPdtInsList poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;
            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            cProduct oProduct;

            //For response
           //cmlResPdtList oPdtList = new cmlResPdtList();          // สำหรับใส่ข้อมูล ทั้ง Header และ Detail
            List<cmlResPdtListInsExcute> aoPdtListBarExcModel;     // สำหรับ execute ค่า Error ลงใน Model กรณี มีข้อมูลผิดพลาด
            cmlResPdtInsDT oPdtDetail;                            // สำหรับ Add ข้อมูล Detail product 
            List<cmlResPdtInsDT> aoPdtDetail;                    // สำหรับ Add ข้อมูล Detail product ให้เป็น List
            cmlResPdtInsListBarInto oResHeader;                 // สำหรับ Add ข้อมูลในส่วน Header [code , description] เพื่อ Response
            List<cmlResPdtInsListBarInto> aoResDetail;         // สำหรับ Add ข้อมูล ทั้งหมด ของ product ใส่ List เพื่อ Response
            cmlResList<cmlResPdtInsListBarInto> oResponse;    // สำหรับ Response ข้อมูล

            //******

            bool bVerifyPara;
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName;

            try
            {

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();
                oDatabase = new cDatabase();
                oResponse = new cmlResList<cmlResPdtInsListBarInto>();
                aoResDetail = new List<cmlResPdtInsListBarInto>();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Check model error
                tModelErr = "";
                if(oFunc.SP_CHKbPrmModel(ref tModelErr,ModelState))
                {
                   aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        //Check key API
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            try
                            {
                                // Confuguration database.
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                                oProduct = new cProduct();

                                foreach (var oArr in poParam.paoPdtInsList)
                                {
                                    var oPdt = oArr as cmlReqPdtInsInfo;

                                    foreach (var oArrBar in oPdt.paoPdtBarInsList)
                                    {
                                        var oBc = oArrBar as cmlReqPdtBarInfo;
                                        bVerifyPara = oProduct.C_DATbChkProduct(oPdt, oBc);

                                        if (bVerifyPara == true)
                                        {
                                            oProduct.C_DATbInsProduct(oPdt.ptPdtCode, oBc.ptPdtBarCode);
                                        }
                                    }
                                }

                                oSql = new StringBuilder();
                                oSql.AppendLine("  SELECT  FTPdtCode AS rtPdtCode, FTPdtBarCode AS rtPdtBarCode,  ");
                                oSql.AppendLine("  FTErrorPdtDupicate AS rtPdtDupicate,");
                                oSql.AppendLine("  FTErrorPdtLUnit AS rtPdtLUnit,FTErrorPdtType AS rtPdtType,");
                                oSql.AppendLine("  FTErrorPbnCode AS rtPbnCode,FTErrorDcsCode AS rtPdtDcs , ");
                                oSql.AppendLine("  FTErrorDepCode AS rtPdtDepart ,FTErrorClsCode AS rtPdtClass,");
                                oSql.AppendLine("  FTErrorSclCode AS rtPdtSubClass ");
                                oSql.AppendLine("  FROM TCNTPdtTemp");
                                aoPdtListBarExcModel = oDatabase.C_DATaSqlQuery<cmlResPdtListInsExcute>(oSql.ToString());

                                if (aoPdtListBarExcModel != null && aoPdtListBarExcModel.Count != 0)
                                {
                                    // Case : product & barcode dupicate yourself.
                                    var oPdtdup = (from oItem in aoPdtListBarExcModel
                                                   where oItem.rtPdtDupicate != null
                                                   select oItem).ToList();

                                    if (oPdtdup.Count != 0)
                                    {
                                        oResHeader = new cmlResPdtInsListBarInto();
                                        aoPdtDetail = new List<cmlResPdtInsDT>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode702;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc702;

                                        for (int nRow = 0; nRow < oPdtdup.Count(); nRow++)
                                        {
                                            oPdtDetail = new cmlResPdtInsDT();
                                            oPdtDetail.rtPdtCode = oPdtdup[nRow].rtPdtCode;
                                            oPdtDetail.rtPdtBarCode = oPdtdup[nRow].rtPdtBarCode;
                                            aoPdtDetail.Add(oPdtDetail);
                                            oResHeader.raoPdtList = aoPdtDetail;
                                        }

                                        aoResDetail.Add(oResHeader);
                                    }

                                    //Case : Unit not found.
                                    var oPdtUnit = (from oItem in aoPdtListBarExcModel
                                                    where oItem.rtPdtLUnit != null
                                                    select oItem).ToList();

                                    if (oPdtUnit.Count != 0)
                                    {
                                        oResHeader = new cmlResPdtInsListBarInto();
                                        aoPdtDetail = new List<cmlResPdtInsDT>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode703;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc703;

                                        for (int nRow = 0; nRow < oPdtUnit.Count(); nRow++)
                                        {
                                            oPdtDetail = new cmlResPdtInsDT();
                                            oPdtDetail.rtPdtCode = oPdtUnit[nRow].rtPdtCode;
                                            oPdtDetail.rtPdtBarCode = oPdtUnit[nRow].rtPdtBarCode;
                                            aoPdtDetail.Add(oPdtDetail);
                                            oResHeader.raoPdtList = aoPdtDetail;
                                        }

                                        aoResDetail.Add(oResHeader);
                                    }

                                    // Product type not found;
                                    var oPdtType = (from oItem in aoPdtListBarExcModel
                                                    where oItem.rtPdtType != null
                                                    select oItem).ToList();
                                    if (oPdtType.Count != 0)
                                    {
                                        oResHeader = new cmlResPdtInsListBarInto();
                                        aoPdtDetail = new List<cmlResPdtInsDT>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode704;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc704;

                                        for (int nRow = 0; nRow < oPdtType.Count(); nRow++)
                                        {
                                            oPdtDetail = new cmlResPdtInsDT();
                                            oPdtDetail.rtPdtCode = oPdtType[nRow].rtPdtCode;
                                            oPdtDetail.rtPdtBarCode = oPdtType[nRow].rtPdtBarCode;
                                            oResHeader.raoPdtList = aoPdtDetail;
                                        }

                                        aoResDetail.Add(oResHeader);
                                    }

                                    //Product brand not found.
                                    var oPdtBrand = (from oItem in aoPdtListBarExcModel
                                                     where oItem.rtPbnCode != null
                                                     select oItem).ToList();

                                    if (oPdtBrand.Count != 0)
                                    {
                                        oResHeader = new cmlResPdtInsListBarInto();
                                        aoPdtDetail = new List<cmlResPdtInsDT>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode705;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc705;

                                        for (int nRow = 0; nRow < oPdtBrand.Count(); nRow++)
                                        {
                                            oPdtDetail = new cmlResPdtInsDT();
                                            oPdtDetail.rtPdtCode = oPdtBrand[nRow].rtPdtCode;
                                            oPdtDetail.rtPdtBarCode = oPdtBrand[nRow].rtPdtBarCode;
                                            aoPdtDetail.Add(oPdtDetail);
                                            oResHeader.raoPdtList = aoPdtDetail;
                                        }
                                        aoResDetail.Add(oResHeader);
                                    }

                                    //Product Dcs not found .
                                    var oPdtDcs = (from oItem in aoPdtListBarExcModel
                                                   where oItem.rtPdtDcs != null
                                                   select oItem).ToList();

                                    if (oPdtDcs.Count != 0)
                                    {
                                        oResHeader = new cmlResPdtInsListBarInto();
                                        aoPdtDetail = new List<cmlResPdtInsDT>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode706;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc706;
                                        for (int nRow = 0; nRow < oPdtDcs.Count(); nRow++)
                                        {
                                            oPdtDetail = new cmlResPdtInsDT();
                                            oPdtDetail.rtPdtCode = oPdtDcs[nRow].rtPdtCode;
                                            oPdtDetail.rtPdtBarCode = oPdtDcs[nRow].rtPdtBarCode;
                                            aoPdtDetail.Add(oPdtDetail);
                                            oResHeader.raoPdtList = aoPdtDetail;
                                        }
                                        aoResDetail.Add(oResHeader);
                                    }

                                    // Product department not found.
                                    var oPdtDep = (from oItem in aoPdtListBarExcModel
                                                   where oItem.rtPdtDepart != null
                                                   select oItem).ToList();

                                    if (oPdtDep.Count != 0)
                                    {
                                        oResHeader = new cmlResPdtInsListBarInto();
                                        aoPdtDetail = new List<cmlResPdtInsDT>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode707;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc707;

                                        for (int nRow = 0; nRow < oPdtDep.Count(); nRow++)
                                        {
                                            oPdtDetail = new cmlResPdtInsDT();
                                            oPdtDetail.rtPdtCode = oPdtDep[nRow].rtPdtCode;
                                            oPdtDetail.rtPdtCode = oPdtDep[nRow].rtPdtBarCode;
                                            aoPdtDetail.Add(oPdtDetail);
                                            oResHeader.raoPdtList = aoPdtDetail;
                                        }

                                        aoResDetail.Add(oResHeader);
                                    }

                                    //Product class not found
                                    var oPdtCls = (from oItem in aoPdtListBarExcModel
                                                   where oItem.rtPdtClass != null
                                                   select oItem).ToList();

                                    if (oPdtCls.Count != 0)
                                    {
                                        oResHeader = new cmlResPdtInsListBarInto();
                                        aoPdtDetail = new List<cmlResPdtInsDT>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode708;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc708;

                                        for (int nRow = 0; nRow > oPdtCls.Count(); nRow++)
                                        {
                                            oPdtDetail = new cmlResPdtInsDT();
                                            oPdtDetail.rtPdtCode = oPdtCls[nRow].rtPdtCode;
                                            oPdtDetail.rtPdtBarCode = oPdtCls[nRow].rtPdtBarCode;
                                            aoPdtDetail.Add(oPdtDetail);
                                            oResHeader.raoPdtList = aoPdtDetail;
                                        }
                                        aoResDetail.Add(oResHeader);
                                    }

                                    //Product subclass not found
                                    var oPdtScl = (from oItem in aoPdtListBarExcModel
                                                   where oItem.rtPdtSubClass != null
                                                   select oItem).ToList();

                                    if (oPdtScl.Count != 0)
                                    {
                                        oResHeader = new cmlResPdtInsListBarInto();
                                        aoPdtDetail = new List<cmlResPdtInsDT>();

                                        oResHeader.rtCode = oMsg.tMS_RespCode709;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc709;

                                        for (int nRow = 0; nRow > oPdtScl.Count(); nRow++)
                                        {
                                            oPdtDetail = new cmlResPdtInsDT();
                                            oPdtDetail.rtPdtCode = oPdtScl[nRow].rtPdtCode;
                                            oPdtDetail.rtPdtBarCode = oPdtScl[nRow].rtPdtBarCode;
                                            aoPdtDetail.Add(oPdtDetail);
                                            oResHeader.raoPdtList = aoPdtDetail;
                                        }
                                        aoResDetail.Add(oResHeader);
                                    }

                                    //oPdtList.raoPdtInsListBarInto = aoResDetail;
                                }
                                else
                                {
                                    oResHeader = new cmlResPdtInsListBarInto();
                                    oResHeader.rtCode = oMsg.tMS_RespCode1;
                                    oResHeader.rtDesc = oMsg.tMS_RespDesc1;
                                    aoResDetail.Add(oResHeader);
                                }


                                oSql = new StringBuilder();
                                oSql.AppendLine(" DROP TABLE TCNTPdtTemp");
                                oDatabase.C_DATnExecuteSql(oSql.ToString());
                            }
                            catch (Exception oExn)
                            {
                                oResHeader = new cmlResPdtInsListBarInto();
                                oResHeader.rtCode = oMsg.tMS_RespCode900;
                                oResHeader.rtDesc = oMsg.tMS_RespDesc900;
                                aoResDetail.Add(oResHeader);
                                oResponse.roItem = aoResDetail;
                            }
                        }
                        else
                        {
                            oResHeader = new cmlResPdtInsListBarInto();
                            oResHeader.rtCode = oMsg.tMS_RespCode904;
                            oResHeader.rtDesc = oMsg.tMS_RespDesc904;
                            aoResDetail.Add(oResHeader);
                        }
                    }
                    else
                    {
                        oResHeader = new cmlResPdtInsListBarInto();
                        oResHeader.rtCode = oMsg.tMS_RespCode906;
                        oResHeader.rtDesc = oMsg.tMS_RespDesc906;
                        aoResDetail.Add(oResHeader);
                    }
             
                   
                }
                else
                {
                    oResHeader = new cmlResPdtInsListBarInto();
                    oResHeader.rtCode = oMsg.tMS_RespCode701;
                    oResHeader.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    aoResDetail.Add(oResHeader);

                }

                
                oResponse.roItem = aoResDetail;
                return oResponse;
            }
            catch(Exception oExn)
            {
                oResponse = new cmlResList<cmlResPdtInsListBarInto>();
                oResHeader = new cmlResPdtInsListBarInto();
                aoResDetail = new List<cmlResPdtInsListBarInto>();
                oMsg = new cMS();
                oResHeader.rtCode = oMsg.tMS_RespCode900;
                oResHeader.rtDesc = oMsg.tMS_RespDesc900;
                aoResDetail.Add(oResHeader);
                oResponse.roItem = aoResDetail;
                return oResponse;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oCons = null;
                oSql = null; ;
                oDatabase = null; ;
                aoSysConfig = null; ;
                oProduct = null; ;
                aoPdtListBarExcModel = null; ;     
                oPdtDetail = null; ;                         
                aoPdtDetail = null; ;                      
                oResHeader = null; ;                 
                aoResDetail = null; ;          
                oResponse = null; ;    

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Update product item.
        /// </summary>
        /// <param name="poParam">Product for update Information.</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 706 : product type not found in type master. <br/>
        /// 707 : product brand not found in brand master. <br/>
        /// 708 : product DCS not found in DCS master. <br/>
        /// 709 : product depart not found in depart master. <br/>
        /// 710 : product class not found in class master. <br/>
        /// 711 : product sub class not found in sub class master. <br/>
        /// 800 : data not found. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database.<br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Update/Item")]
        [HttpPost]
        public cmlResponseList<cmlResPdtUpdItem> POST_PDToUptProduct([FromBody] cmlReqPdtUptInfo poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;
            cProduct oPdt;
            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;

            cmlResBase oItemUpt;
            List<cmlResBase> aoItemUpt;
            List<cmlResPdtUpdItem> aoListItem;
            cmlResPdtUpdItem oAddResponse;
            cmlResponseList<cmlResPdtUpdItem> oResponse;
            List<cmlResPdtUpdItemlist> oItemAddList;

            bool bMaster = true; //Check สถานะ Error master

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName;

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oPdt = new cProduct();
                oDatabase = new cDatabase();
                aoSysConfig = new List<cmlTSysConfig>();

                oItemAddList = new List<cmlResPdtUpdItemlist>();   // Set ค่าที่จะ Return
                aoListItem = new List<cmlResPdtUpdItem>();         //เก็บค่า ใส่ List
                oAddResponse = new cmlResPdtUpdItem();             // Add ค่าเข้า List หลักเพื่อจะ Response
                oResponse = new cmlResponseList<cmlResPdtUpdItem>();    // Response ค่า
                oItemUpt = new cmlResBase();
                aoItemUpt = new List<cmlResBase>();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Check model error
                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();
                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {

                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            try
                            {
                                // Configuration database.
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                                //Check ข้อมูล Master ก่อน Update
                                //Product type.

                                if (poParam.ptPtyCode != null && poParam.ptPtyCode != "")
                                {
                                    if (oPdt.C_DATbChkMaster("PdtType", poParam.ptPtyCode) == false)
                                    {
                                        bMaster = false;
                                        oItemUpt.rtCode = oMsg.tMS_RespCode706;
                                        oItemUpt.rtDesc = oMsg.tMS_RespDesc706PdtUpdate;
                                        aoItemUpt.Add(oItemUpt);
                                    }
                                    oAddResponse.rtPtyCode = poParam.ptPtyCode;
                                }
                                //Product brand
                                if (poParam.ptPbnCode != null && poParam.ptPbnCode != "")
                                {
                                    oItemUpt = new cmlResBase();
                                    if (oPdt.C_DATbChkMaster("PdtBrand", poParam.ptPbnCode) == false)
                                    {
                                        bMaster = false;
                                        oItemUpt.rtCode = oMsg.tMS_RespCode707;
                                        oItemUpt.rtDesc = oMsg.tMS_RespDesc707PdtUpdate;
                                        aoItemUpt.Add(oItemUpt);
                                        //oResponse.raUpate = aoItemUpt;
                                    }
                                    oAddResponse.rtPbnCode = poParam.ptPbnCode;
                                }
                                //Product Dcs code.
                                if (poParam.ptDcsCode != null && poParam.ptDcsCode != "")
                                {
                                    oItemUpt = new cmlResBase();

                                    if (oPdt.C_DATbChkMaster("DcsCode", poParam.ptDcsCode) == false)
                                    {
                                        bMaster = false;
                                        oItemUpt.rtCode = oMsg.tMS_RespCode708;
                                        oItemUpt.rtDesc = oMsg.tMS_RespDesc708PdtUpdate;
                                        aoItemUpt.Add(oItemUpt);
                                        //oResponse.raUpate = aoItemUpt;
                                    }
                                    oAddResponse.rtDcsCode = poParam.ptDcsCode;
                                }
                                //Product depart.
                                if (poParam.ptDepCode != null && poParam.ptDepCode != "")
                                {
                                    //oItem = new cmlResPdtUpdItemlist();

                                    if (oPdt.C_DATbChkMaster("DepCode", poParam.ptDepCode) == false)
                                    {
                                        bMaster = false;
                                        oItemUpt.rtCode = oMsg.tMS_RespCode709;
                                        oItemUpt.rtDesc = oMsg.tMS_RespDesc709PdtUpdate;
                                        aoItemUpt.Add(oItemUpt);
                                        //oResponse.raUpate = aoItemUpt;
                                    }
                                    oAddResponse.rtFTDepCode = poParam.ptDepCode;
                                }

                                //Product Class.
                                if (poParam.ptClsCode != null && poParam.ptClsCode != "")
                                {
                                    //oItem = new cmlResPdtUpdItemlist();

                                    if (oPdt.C_DATbChkMaster("ClsCode", poParam.ptClsCode) == false)
                                    {
                                        bMaster = false;
                                        oItemUpt.rtCode = oMsg.tMS_RespCode710;
                                        oItemUpt.rtDesc = oMsg.tMS_RespDesc710PdtUpdate;
                                        aoItemUpt.Add(oItemUpt);
                                        //oResponse.raUpate = aoItemUpt;
                                    }
                                    oAddResponse.rtClsCode = poParam.ptClsCode;
                                }

                                //Product sub class
                                if (poParam.ptSclCode != null && poParam.ptSclCode != "")
                                {
                                    //oItem = new cmlResPdtUpdItemlist();

                                    if (oPdt.C_DATbChkMaster("SubClass", poParam.ptSclCode) == false)
                                    {
                                        bMaster = false;
                                        oItemUpt.rtCode = oMsg.tMS_RespCode711;
                                        oItemUpt.rtDesc = oMsg.tMS_RespDesc711PdtUpdate;
                                        aoItemUpt.Add(oItemUpt);
                                        //oResponse.raUpate = aoItemUpt;
                                    }
                                    oAddResponse.rtSclCode = poParam.ptSclCode;
                                }

                                if (bMaster == true)
                                {
                                    oSql = new StringBuilder();
                                    oSql.AppendLine(oFunc.SP_SQLtGeneralCmdUpdate<cmlReqPdtUptInfo>(poParam, "cProduct"));
                                    oSql.AppendLine(" ,FDDateUpd = CONVERT(VARCHAR(10), GETDATE(), 121),");
                                    oSql.AppendLine(" FTTimeUpd = CONVERT(VARCHAR(8),GETDATE(),114),");
                                    oSql.AppendLine(" FTWhoUpd  = 'AdaLink'");
                                    oSql.AppendLine(" WHERE (FTPdtStkCode = '" + poParam.ptPdtCode + "')");
                                    int nSucc = oDatabase.C_DATnExecuteSql(oSql.ToString());
                                    if (nSucc != 0)
                                    {

                                        oItemUpt.rtCode = oMsg.tMS_RespCode1;
                                        oItemUpt.rtDesc = oMsg.tMS_RespDesc1;
                                        aoItemUpt.Add(oItemUpt);
                                    }
                                    else
                                    {
                                        oItemUpt.rtCode = oMsg.tMS_RespCode800;
                                        oItemUpt.rtDesc = oMsg.tMS_RespDesc800;
                                        aoItemUpt.Add(oItemUpt);
                                    }
                                }

                            }
                            catch (Exception oExn)
                            {
                                oItemUpt.rtCode = oMsg.tMS_RespCode900;
                                oItemUpt.rtDesc = oMsg.tMS_RespDesc900;
                                aoItemUpt.Add(oItemUpt);
                            }
                        }
                        else
                        {
                            oItemUpt.rtCode = oMsg.tMS_RespCode904;
                            oItemUpt.rtDesc = oMsg.tMS_RespDesc904;
                            aoItemUpt.Add(oItemUpt);
                        }

                    }
                    else
                    {
                        oItemUpt.rtCode = oMsg.tMS_RespCode906;
                        oItemUpt.rtDesc = oMsg.tMS_RespDesc906;
                        aoItemUpt.Add(oItemUpt);
                    }

                   
                }
                else
                {
                    oItemUpt.rtCode = oMsg.tMS_RespCode701;
                    oItemUpt.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    aoItemUpt.Add(oItemUpt);
                }

                oAddResponse.rtPdtCode = poParam.ptPdtCode;
                oAddResponse.rtPdtCode = poParam.ptPdtName;
                aoListItem.Add(oAddResponse);
                oResponse.roItem = aoListItem;
                oResponse.raUpate = aoItemUpt;

                return oResponse;

            }
            catch(Exception oExn)
            {
                oMsg = new cMS();
                aoListItem = new List<cmlResPdtUpdItem>();
                oResponse = new cmlResponseList<cmlResPdtUpdItem>();
                oItemUpt = new cmlResBase();
                aoItemUpt = new List<cmlResBase>();
                oItemUpt.rtCode = oMsg.tMS_RespCode900;
                oItemUpt.rtDesc = oMsg.tMS_RespDesc900;
                aoItemUpt.Add(oItemUpt);
                oResponse.roItem = aoListItem;
                oResponse.raUpate = aoItemUpt;
                

                return oResponse;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oCons = null;
                oPdt = null;
                oDatabase = null;
                aoSysConfig = null;

                oItemAddList = null;
                aoListItem = null;
                oAddResponse = null;
                oResponse = null;
                oItemUpt = null;
                aoItemUpt = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Update barcode item
        /// </summary>
        /// <param name="poParam"> Product Information for update barcode </param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 702 : barcode duplicate.<br/>
        /// 704 : product unit not found in unit master. <br/>
        /// 800 : data not found. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method.<br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method. 
        /// </returns>
        [Route("Update/Barcode/Item")]
        [HttpPost]
        public cmlResponse<cmlResPdtUpdBarcodeItem> POST_PDToUptProduct([FromBody] cmlReqPdtUpdBarcode poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;
            cProduct oPdt;
            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            cmlResPdtUpdItemlist oItem;
            List<cmlResPdtUpdItemlist> oItemAddList;
            DataTable oDbtbl;
            cmlResPdtUpdBarcodeItem oAddResponse;
            cmlResponse<cmlResPdtUpdBarcodeItem> oResponse;
            List<cmlResPdtUpdBarcodeItem> aoListItem;


            bool bMaster = true; //Check สถานะ Error master

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName;

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oPdt = new cProduct();
                oDatabase = new cDatabase();
                aoSysConfig = new List<cmlTSysConfig>();
                oItemAddList = new List<cmlResPdtUpdItemlist>();
                oAddResponse = new cmlResPdtUpdBarcodeItem();
                oResponse = new cmlResponse<cmlResPdtUpdBarcodeItem>();
                aoListItem = new List<cmlResPdtUpdBarcodeItem>();
                string tConditions = "";
                string tWhere = "";
                string tUnit = "";


                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Check model error
                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig(); 

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            try
                            {
                                // Confuguration database.
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                                //Check ข้อมูล Master
                                if (poParam.ptPdtUnit != null && poParam.ptPdtUnit != "")
                                {

                                    oItem = new cmlResPdtUpdItemlist();
                                    if (oPdt.C_DATbChkMaster("Unit", poParam.ptPdtUnit) == false)
                                    {
                                        bMaster = false;
                                        oItem.rtCode = oMsg.tMS_RespCode704;
                                        oItem.rtDesc = oMsg.tMS_RespDesc704PdtUpdateBar;
                                        oItemAddList.Add(oItem);
                                        oAddResponse.aoPdtMaster = oItemAddList;
                                    }
                                }

                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPdtCode,FTPdtStkCode ");
                                oSql.AppendLine(" FROM TCNMPdt");
                                oSql.AppendLine(" WHERE (FTPdtStkCode = '" + poParam.ptPdtCode + "') ");
                                oSql.AppendLine(" AND ( FTPdtBarCode1 = '" + poParam.ptPdtBarCodeUpdate + "' OR ");
                                oSql.AppendLine(" FTPdtBarCode2 = '" + poParam.ptPdtBarCodeUpdate + "' OR FTPdtBarCode3 = '" + poParam.ptPdtBarCodeUpdate + "') ");
                                oDbtbl = new DataTable();
                                oDbtbl = oDatabase.C_DAToSqlQueryDt(oSql.ToString());
                                if (oDbtbl != null && oDbtbl.Rows.Count > 0)
                                {
                                    bMaster = false;
                                    oResponse.rtCode = oMsg.tMS_RespCode702;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc702;
                                }

                                if (bMaster == true)
                                {

                                    oSql = new StringBuilder();
                                    oSql.AppendLine(" SELECT FTPdtBarCode1,FTPdtBarCode2,FTPdtBarCode3");
                                    oSql.AppendLine(" FROM TCNMPdt ");
                                    oSql.AppendLine(" WHERE FTPdtStkCode = '" + poParam.ptPdtCode + "' ");
                                    oSql.AppendLine(" AND  FTPdtBarCode1 = '" + poParam.ptPdtBarCode + "' OR");
                                    oSql.AppendLine(" FTPdtBarCode2 = '" + poParam.ptPdtBarCode + "' OR  ");
                                    oSql.AppendLine(" FTPdtBarCode3 = '" + poParam.ptPdtBarCode + "' ");
                                    oDbtbl = oDatabase.C_DAToSqlQueryDt(oSql.ToString());

                                    if (oDbtbl.Rows[0]["FTPdtBarCode1"].ToString() == poParam.ptPdtBarCode)
                                    {
                                        tConditions += "FTPdtBarCode1  = '" + poParam.ptPdtBarCodeUpdate + "'";
                                        tUnit += " , FTPdtSUnit = '" + poParam.ptPdtUnit + "' ";
                                        tWhere += " AND  FTPdtBarCode1 = '" + poParam.ptPdtBarCode + "'";
                                    }
                                    else if (oDbtbl.Rows[0]["FTPdtBarCode2"].ToString() == poParam.ptPdtBarCode)
                                    {
                                        tConditions += "FTPdtBarCode2  = '" + poParam.ptPdtBarCodeUpdate + "'";
                                        tUnit += " , FTPdtMUnit = '" + poParam.ptPdtUnit + "' ";
                                        tWhere += " AND  FTPdtBarCode2 = '" + poParam.ptPdtBarCode + "'";
                                    }
                                    else if (oDbtbl.Rows[0]["FTPdtBarCode3"].ToString() == poParam.ptPdtBarCode)
                                    {
                                        tConditions += "FTPdtBarCode3  = '" + poParam.ptPdtBarCodeUpdate + "'";
                                        tUnit += " , FTPdtLUnit = '" + poParam.ptPdtUnit + "' ";
                                        tWhere += " AND  FTPdtBarCode3 = '" + poParam.ptPdtBarCode + "'";
                                    }


                                    oSql = new StringBuilder();
                                    oSql.AppendLine(" UPDATE ");
                                    oSql.AppendLine(" TCNMPdt WITH(ROWLOCK)");
                                    oSql.AppendLine(" SET " + tConditions + " ");
                                    oSql.AppendLine(" " + tUnit + "");
                                    oSql.AppendLine(" WHERE FTPdtStkCode = '" + poParam.ptPdtCode + "' ");
                                    oSql.AppendLine(" " + tWhere + " ");
                                    int nSucc = oDatabase.C_DATnExecuteSql(oSql.ToString());

                                    if (nSucc != 0)
                                    {
                                        oResponse.rtCode = oMsg.tMS_RespCode1;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc1;
                                    }
                                    else
                                    {
                                        oResponse.rtCode = oMsg.tMS_RespCode800;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                    }
                                }

                                oAddResponse.rtPdtBarCode = poParam.ptPdtBarCode;
                                oAddResponse.rtPdtCode = poParam.ptPdtCode;
                                oAddResponse.rtPdtBarCodeUpdate = poParam.ptPdtBarCodeUpdate;
                                oAddResponse.rtPdtUnit = poParam.ptPdtUnit;
                                aoListItem.Add(oAddResponse);
                                oResponse.roItem = aoListItem;
                            }
                            catch (Exception oExn)
                            {
                                oResponse.rtCode = oMsg.tMS_RespCode800;
                                oResponse.rtDesc = oMsg.tMS_RespDesc800;
                            }

                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }

                
                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }


                return oResponse;
            }

            catch (Exception oExn)
            {
                oMsg = new cMS();
                oResponse = new cmlResponse<cmlResPdtUpdBarcodeItem>();
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;
                return oResponse;
            }
            finally
            {
               
            }
        }

        /// <summary>
        /// Insert image list
        /// </summary>
        /// <param name="poParam">Image information</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 706 : image size not found.  <br/>
        /// 707 : cannot convert base64 to image. <br/>
        /// 708 : path folder image not found. <br/>
        /// 709 : product code parameter duplicate in yourself. <br/>
        /// 800 : data not found. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method. <br/>
        /// </returns>
        [Route("Insert/Image/List")]
        [HttpPost]
        public cmlResList<cmlResPdtImg> POST_PDToInsImgProduct([FromBody] cmlReqPdtImgList poParam)
        {
            DataTable oDbtbl;
            cSP oFunc;
            cMS oMsg;
            cCS oCons;
            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            cProduct oProduct;

            List<cmlResPdtExe> aoExecute;
            cmlResPdtImgList oError;
            List<cmlResPdtImgList> oErrorList;
            cmlResPdtImg oResHeader;
            List<cmlResPdtImg> oResHeaderList;
            cmlResList<cmlResPdtImg> oResponse;
            List<string> atPdtDup;


            bool bStatus = true;

            //******

            bool bVerifyPara;
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tPathFolImg;

            try
            {

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();
                oDatabase = new cDatabase();
                oDbtbl = new DataTable();
                aoExecute = new List<cmlResPdtExe>();
                oResHeaderList = new List<cmlResPdtImg>();
                oResponse = new cmlResList<cmlResPdtImg>();
                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;
                atPdtDup = new List<string>();

                // Check model error
                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();
                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        //Check key API
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            try
                            {
                                // Confuguration database.
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                                oProduct = new cProduct();
                                oProduct.C_DATtGenTableTmpImg();  //สร้าง temp สำหรับเก็บ Product ที่ Error

                                //Check Product dupicat
                                atPdtDup = poParam.paoPdtImgList.GroupBy(
                                oItemGrp => oItemGrp.ptPdtCode).Where(oItemWhe => oItemWhe.Count() > 1).Select(oItem => oItem.Key).ToList();

                                if (atPdtDup != null && atPdtDup.Count() > 0)
                                {
                                    foreach (string tPdtDup in atPdtDup)
                                    {
                                        oProduct.C_DATbInsTableTmpImg(tPdtDup, "709");
                                    }
                                }

                                foreach (var oArr in poParam.paoPdtImgList)
                                {
                                    var oPdt = oArr as cmlReqPdtImgInfo;

                                    if (oPdt.ptPdtPicBase64 == null)
                                    {
                                        oErrorList = new List<cmlResPdtImgList>();
                                        oResHeader = new cmlResPdtImg();
                                        oError = new cmlResPdtImgList();
                                        oResHeader.rtCode = oMsg.tMS_RespCode701;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc701 + "ptPdtPicBase64 can't not null";
                                        oErrorList.Add(oError);
                                        oResHeaderList.Add(oResHeader);
                                        oResponse.roItem = oResHeaderList;
                                        return oResponse;
                                    }

                                    tPathFolImg = "";
                                    oFunc.SP_DATxGetConfigFromMem<string>(ref tPathFolImg, "", aoSysConfig, "006");
                                    string tPathPdt = tPathFolImg + "Product\\";

                                    if (!(System.IO.Directory.Exists(tPathPdt)))
                                    {
                                        System.IO.Directory.CreateDirectory(tPathPdt);
                                    }

                                    // กรณี ลบรูปภาพ โดยที่ผู้ใช้ส่ง ค่า " " มาที่ ฟิลด์
                                    if (oPdt.ptPdtPicBase64 == "")
                                    {
                                        string tPathFull = "";
                                        if (oProduct.C_DATbDelImageonTable(oPdt.ptPdtCode) == true)
                                        {
                                            tPathFull = tPathPdt + oPdt.ptPdtCode + ".jpg";
                                            oProduct.C_IMGbDeleteImage(tPathFull);
                                        }
                                    }

                                    string bImg = oProduct.C_IMGbSaveStr2Img(oPdt.ptPdtCode, tPathPdt, oPdt.ptPdtPicBase64, tPathPdt + oPdt.ptPdtCode);
                                    switch (bImg)
                                    {
                                        case "Success":
                                            oSql = new StringBuilder();
                                            oSql.AppendLine(" UPDATE ");
                                            oSql.AppendLine(" TCNMPdt WITH(ROWLOCK) ");
                                            oSql.AppendLine(" SET FTPdtPic = '" + oProduct.C_IMGtGetPath(tPathPdt, oPdt.ptPdtCode) + "' ");
                                            oSql.AppendLine(" WHERE FTPdtStkCode = '" + oPdt.ptPdtCode + "' ");
                                            int nSuccess = oDatabase.C_DATnExecuteSql(oSql.ToString());

                                            if (nSuccess == 0)
                                            {
                                                oProduct.C_DATbInsTableTmpImg(oPdt.ptPdtCode, "800");
                                            }
                                            break;
                                        case "Sizeimagenotfound":
                                            oProduct.C_DATbInsTableTmpImg(oPdt.ptPdtCode, "706");
                                            break;
                                        case "Fail":
                                            oProduct.C_DATbInsTableTmpImg(oPdt.ptPdtCode, "707");
                                            break;
                                    }
                                }

                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPdtCode AS ptPdtCode,FTErrorDataNotfound AS ptErrorDataNotfound,FTErrorSize AS ptErrorSize ,");
                                oSql.AppendLine(" FTErrorProductDup AS ptErrorProductDup,");
                                oSql.AppendLine(" FTErrorBase64 AS ptErrorBase64");
                                oSql.AppendLine(" FROM TCNTImgErrorTmp");
                                aoExecute = oDatabase.C_DATaSqlQuery<cmlResPdtExe>(oSql.ToString());

                                if (aoExecute != null && aoExecute.Count > 0)
                                {
                                    bStatus = false;

                                    var oPdtDup = (from oItem in aoExecute
                                                   where oItem.ptErrorProductDup != null
                                                   select oItem).ToList();
                                    if (oPdtDup.Count > 0)
                                    {
                                        oResHeader = new cmlResPdtImg();
                                        oErrorList = new List<cmlResPdtImgList>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode709;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc709Img;
                                        for (int nRow = 0; nRow < oPdtDup.Count(); nRow++)
                                        {
                                            oError = new cmlResPdtImgList();
                                            oError.rtPdtCode = oPdtDup[nRow].ptPdtCode;
                                            oErrorList.Add(oError);
                                            oResHeader.roPdtList = oErrorList;
                                        }
                                        oResHeaderList.Add(oResHeader);
                                    }

                                    var oPdtNotfound = (from oItem in aoExecute
                                                        where oItem.ptErrorDataNotfound != null
                                                        select oItem).ToList();
                                    if (oPdtNotfound.Count > 0)
                                    {
                                        oResHeader = new cmlResPdtImg();
                                        oErrorList = new List<cmlResPdtImgList>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode800;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc800;
                                        for (int nRow = 0; nRow < oPdtNotfound.Count(); nRow++)
                                        {
                                            oError = new cmlResPdtImgList();
                                            oError.rtPdtCode = oPdtNotfound[nRow].ptPdtCode;
                                            oErrorList.Add(oError);
                                            oResHeader.roPdtList = oErrorList;
                                        }
                                        oResHeaderList.Add(oResHeader);
                                    }

                                    var oPdtdB64 = (from oItem in aoExecute
                                                    where oItem.ptErrorBase64 != null
                                                    select oItem).ToList();

                                    if (oPdtdB64.Count > 0)
                                    {
                                        oResHeader = new cmlResPdtImg();
                                        oErrorList = new List<cmlResPdtImgList>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode707;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc707Img;

                                        for (int nRow = 0; nRow < oPdtdB64.Count(); nRow++)
                                        {
                                            oError = new cmlResPdtImgList();
                                            oError.rtPdtCode = oPdtdB64[nRow].ptPdtCode;
                                            oErrorList.Add(oError);
                                            oResHeader.roPdtList = oErrorList;
                                        }
                                        oResHeaderList.Add(oResHeader);
                                    }

                                    var oPdtdSize = (from oItem in aoExecute
                                                     where oItem.ptErrorSize != null
                                                     select oItem).ToList();

                                    if (oPdtdSize.Count > 0)
                                    {
                                        oResHeader = new cmlResPdtImg();
                                        oErrorList = new List<cmlResPdtImgList>();
                                        oResHeader.rtCode = oMsg.tMS_RespCode706;
                                        oResHeader.rtDesc = oMsg.tMS_RespDesc706Img;

                                        for (int nRow = 0; nRow < oPdtdSize.Count(); nRow++)
                                        {
                                            oError = new cmlResPdtImgList();
                                            oError.rtPdtCode = oPdtdSize[nRow].ptPdtCode;
                                            oErrorList.Add(oError);
                                            oResHeader.roPdtList = oErrorList;
                                        }
                                        oResHeaderList.Add(oResHeader);
                                    }

                                }

                                if (bStatus == true)
                                {
                                    oErrorList = new List<cmlResPdtImgList>();
                                    oResHeader = new cmlResPdtImg();
                                    oError = new cmlResPdtImgList();
                                    oResHeader.rtCode = oMsg.tMS_RespCode1;
                                    oResHeader.rtDesc = oMsg.tMS_RespDesc1;
                                    oErrorList.Add(oError);
                                    oResHeaderList.Add(oResHeader);
                                }
                                oResponse.roItem = oResHeaderList;
                                oSql = new StringBuilder();
                                oSql.AppendLine(" DROP TABLE TCNTImgErrorTmp");
                                oDatabase.C_DATnExecuteSql(oSql.ToString());
                            }
                            catch (Exception oExn)
                            {
                                oErrorList = new List<cmlResPdtImgList>();
                                oResHeader = new cmlResPdtImg();
                                oError = new cmlResPdtImgList();
                                oResHeader.rtCode = oMsg.tMS_RespCode800;
                                oResHeader.rtDesc = oMsg.tMS_RespDesc800;
                                oErrorList.Add(oError);
                                oResHeaderList.Add(oResHeader);
                            }

                        }
                        else
                        {
                            oErrorList = new List<cmlResPdtImgList>();
                            oResHeader = new cmlResPdtImg();
                            oError = new cmlResPdtImgList();
                            oResHeader.rtCode = oMsg.tMS_RespCode904;
                            oResHeader.rtDesc = oMsg.tMS_RespDesc904;
                            oErrorList.Add(oError);
                            oResHeaderList.Add(oResHeader);
                            oResponse.roItem = oResHeaderList;

                        }
                    }
                    else
                    {
                        oErrorList = new List<cmlResPdtImgList>();
                        oResHeader = new cmlResPdtImg();
                        oError = new cmlResPdtImgList();
                        oResHeader.rtCode = oMsg.tMS_RespCode906;
                        oResHeader.rtDesc = oMsg.tMS_RespDesc906;
                        oErrorList.Add(oError);
                        oResHeaderList.Add(oResHeader);
                        oResponse.roItem = oResHeaderList;

                    }

                }

                else
                {
                    oErrorList = new List<cmlResPdtImgList>();
                    oResHeader = new cmlResPdtImg();
                    oError = new cmlResPdtImgList();
                    oResHeader.rtCode = oMsg.tMS_RespCode701;
                    oResHeader.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                    oErrorList.Add(oError);
                    oResHeaderList.Add(oResHeader);
                    oResponse.roItem = oResHeaderList;

                }
                return oResponse;
            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                oResHeaderList = new List<cmlResPdtImg>();
                oErrorList = new List<cmlResPdtImgList>();
                oResHeader = new cmlResPdtImg();
                oError = new cmlResPdtImgList();
                oResponse = new cmlResList<cmlResPdtImg>();

                oResHeader.rtCode = oMsg.tMS_RespCode900;
                oResHeader.rtDesc = oMsg.tMS_RespDesc900;
                oErrorList.Add(oError);
                oResHeaderList.Add(oResHeader);

                return oResponse;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;
                oDbtbl = null;
                aoExecute = null;
                oResHeaderList = null;
                oResponse = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Delete product item
        /// </summary>
        /// <param name="poParam">Product informations for delete</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (product code). <br/>
        /// 813 : data is referent in another data. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method. <br/>
        /// </returns>
        [Route("Delete/Item")]
        [HttpPost]
        public cmlResBase POST_PDToDelProduct([FromBody] cmlReqPdtDel poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;
            cProduct oPdt;
            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            cmlResBase oResponse;


            bool bMaster = true; //Check สถานะ Error master

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tPdtCode;

            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oPdt = new cProduct();
                oDatabase = new cDatabase();
                oResponse = new cmlResBase();
                aoSysConfig = new List<cmlTSysConfig>();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Check model error
                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            try
                            {
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");


                                oSql = new StringBuilder();
                                oSql = new StringBuilder();
                                oSql.AppendLine("SELECT TOP 1 FTPdtCode");
                                oSql.AppendLine("FROM TPSTSalDT WITH(NOLOCK)");
                                oSql.AppendLine("WHERE FTSdtStkCode = '" + poParam.ptPdtCode + "'");

                                tPdtCode = oDatabase.C_DAToSqlQuery<string>(oSql.ToString(), nCmdTme);

                                if (string.IsNullOrEmpty(tPdtCode))
                                {
                                    oSql = new StringBuilder();
                                    oSql.AppendLine(" DELETE ");
                                    oSql.AppendLine(" FROM TCNMPdt WITH(ROWLOCK) ");
                                    oSql.AppendLine(" WHERE FTPdtStkCode = '" + poParam.ptPdtCode + "' ");
                                    int nSucc = oDatabase.C_DATnExecuteSql(oSql.ToString());

                                    if (nSucc != 0)
                                    {
                                        oResponse.rtCode = oMsg.tMS_RespCode1;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc1;
                                    }
                                    else
                                    {
                                        oResponse.rtCode = oMsg.tMS_RespCode800;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                    }
                                }

                                else
                                {
                                    oResponse.rtCode = oMsg.tMS_RespCode813;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc813;
                                    return oResponse;
                                }


                            }
                            catch (Exception oExn)
                            {
                                oResponse.rtCode = oMsg.tMS_RespCode800;
                                oResponse.rtDesc = oMsg.tMS_RespDesc800;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }

                 
                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }
                return oResponse;
            }

            catch (Exception oExn)
            {
                oMsg = new cMS();
                oResponse = new cmlResponse<cmlResPdtUpdBarcodeItem>();
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;
                return oResponse;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oCons = null;
                oPdt = null;
                oDatabase = null;
                oResponse = null;
                aoSysConfig = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Set status active
        /// </summary>
        /// <param name="poParam"> Product Information for update status active.</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 709 : product code parameter duplicate in yourself. <br/>
        /// 800 : data not found. (product code). <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method. <br/>
        /// </returns>
        [Route("Set/Active/List")]
        [HttpPost]
        public cmlResList<cmlResPdtSetInfo> POST_PDToUptProduct([FromBody] cmlReqPdtSetActiveList poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;
            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            cProduct oProduct;
            cmlResList<cmlResPdtSetInfo> oResponse;
            cmlResPdtSetInfo oAddResponse;
            cmlResPdtSetDetail oPdtUpt;
            List<cmlResPdtSetInfo> oListResponse;
            List<cmlResPdtSetDetail> oPdtList;
            bool bStatus = true;

            //******

            bool bVerifyPara;
            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName;

            try
            {

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                oFunc = new cSP();
                oCons = new cCS();
                oMsg = new cMS();
                oDatabase = new cDatabase();

                oPdtUpt = new cmlResPdtSetDetail();
                oAddResponse = new cmlResPdtSetInfo();
                oResponse = new cmlResList<cmlResPdtSetInfo>();
                oPdtList = new List<cmlResPdtSetDetail>();
                oListResponse = new List<cmlResPdtSetInfo>();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                // Check model error
                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        //Check key API
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            try
                            {
                                // Confuguration database.
                                nConTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                                nCmdTme = 0;
                                oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");
                                oProduct = new cProduct();

                                foreach (var oArr in poParam.paoPdtSetList)
                                {
                                    var oPdt = oArr as cmlReqPdtSetInfo;

                                    //foreach (var oArrBar in oPdt.ptPdtCode)
                                    //{
                                    oSql = new StringBuilder();
                                    oSql.AppendLine(" UPDATE TCNMPdt WITH(ROWLOCK)");
                                    oSql.AppendLine(" SET FTPdtStaActive = '" + poParam.ptPdtStaActive + "' ");
                                    oSql.AppendLine(" WHERE FTPdtStkCode = '" + oPdt.ptPdtCode + "' ");
                                    int nSuccess = oDatabase.C_DATnExecuteSql(oSql.ToString());
                                    if (nSuccess == 0)
                                    {
                                        bStatus = false;
                                        oPdtUpt.rtPdtCode = oPdt.ptPdtCode;
                                        oPdtList.Add(oPdtUpt);
                                    }
                                }
                                oAddResponse.raoPdtList = oPdtList;

                                if (bStatus == false)
                                {
                                    oAddResponse.rtCode = oMsg.tMS_RespCode800;
                                    oAddResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }
                                else
                                {
                                    oAddResponse.rtCode = oMsg.tMS_RespCode1;
                                    oAddResponse.rtDesc = oMsg.tMS_RespDesc1;
                                }

                                oListResponse.Add(oAddResponse);
                                oResponse.roItem = oListResponse;

                                return oResponse;

                            }
                            catch (Exception oExn)
                            {
                                oResponse = new cmlResList<cmlResPdtSetInfo>();
                                oAddResponse.rtCode = oMsg.tMS_RespCode800;
                                oAddResponse.rtDesc = oMsg.tMS_RespDesc800;
                                return oResponse;
                            }
                        }
                        else
                        {
                            oAddResponse.rtCode = oMsg.tMS_RespCode904;
                            oAddResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oAddResponse.rtCode = oMsg.tMS_RespCode906;
                        oAddResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
                 
                }
                else
                {
                    oAddResponse.rtCode = oMsg.tMS_RespCode701;
                    oAddResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                oListResponse.Add(oAddResponse);
                oResponse.roItem = oListResponse;

                return oResponse;
            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                oResponse = new cmlResList<cmlResPdtSetInfo>();
                oListResponse = new List<cmlResPdtSetInfo>();
                oAddResponse = new cmlResPdtSetInfo();
                oAddResponse.rtCode = oMsg.tMS_RespCode900;
                oAddResponse.rtDesc = oMsg.tMS_RespCode900;
                oListResponse.Add(oAddResponse);
                oResponse.roItem = oListResponse;
                return oResponse;
            }
            finally
            {
                oFunc = null;
                oCons = null;
                oMsg = null;
                oDatabase = null;

                oPdtUpt = null;
                oAddResponse = null;
                oResponse = null;
                oPdtList = null;
                oListResponse = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();

            }
        }
        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API2C4.Models.WebService.Request.Type;
using API2C4.Models.WebService.Response.Type;
using API2C4.Models.WebService.Response.Base;
using API2C4.Class.Standard;
using API2PosV4Master.Class.Standard;
using System.Text;
using API2C4.Class;
using API2C4.Models.Database;
using API2C4.Models.WebService.Response.Unit;
using System.Globalization;
using System.Threading;
using System.Reflection;
using System.Web;
using System.Data;

namespace API2C4.Controllers
{
    /// <summary>
    /// Management Product type
    /// </summary>
    [RoutePrefix("V1/Type")]
    public class cTypeController : ApiController
    {
        /// <summary>
        /// Insert Product Type
        /// </summary>
        /// <param name="poParam"> Information type product.</param>
        /// <returns>
        ///   1 : success. <br/>
        ///   701 : validate parameter model false.  <br/>
        ///   702 : product type code duplicate.  <br/>
        ///   900 : service process false.  <br/>
        ///   904 : key not allowed to use method.  <br/>
        ///   905 : cannot connect database.  <br/>
        ///   906 : this time not allowed to use method.
        /// </returns>
        [Route("Insert/Item")]
        [HttpPost]
        public cmlResponse<cmlResType> POST_TYPoInstype([FromBody] cmlReqType poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;

            List<cmlTSysConfig> aoSysConfig;
            List<cmlResType> aoType;
            cmlResponse<cmlResType> oResponse;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit;
            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oDatabase = new cDatabase();
                aoType = new List<cmlResType>();
                oResponse = new cmlResponse<cmlResType>();


                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");


                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();
                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPtyCode ");
                                oSql.AppendLine(" FROM TCNMPdtType ");
                                oSql.AppendLine(" WHERE FTPtyCode = '" + poParam.ptPtyCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" INSERT ");
                                    oSql.AppendLine(" INTO TCNMPdtType WITH(ROWLOCK) (FTPtyCode, FTPtyName, ");
                                    oSql.AppendLine(" FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns) ");
                                    oSql.AppendLine(" OUTPUT INSERTED.FTPtyCode AS rtPtyCode , INSERTED.FTPtyName AS rtPtyName ");
                                    oSql.AppendLine(" VALUES( ");
                                    oSql.AppendLine(" '" + poParam.ptPtyCode + "', '" + poParam.ptPtyName + "', ");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink',");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink'");
                                    oSql.AppendLine(" )");

                                    aoType = oDatabase.C_DATaSqlQuery<cmlResType>(oSql.ToString());

                                    oResponse.roItem = aoType;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;


                                }
                                else
                                {
                                    oResponse.roItem = aoType;
                                    oResponse.rtCode = oMsg.tMS_RespCode702;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc702PdtType;
                                }

                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoType;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }

                

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoType = new List<cmlResType>();
                oResponse = new cmlResponse<cmlResType>();
                oResponse.roItem = aoType;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoType = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

        }

        /// <summary>
        /// Update Product Type
        /// </summary>
        /// <param name="poParam">Information type product.</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (product type code). <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method. <br/>
        /// </returns>
        [Route("Update/Item")]
        [HttpPost]
        public cmlResponse<cmlResType> POST_TYPoUpttype([FromBody] cmlReqTypeUpt poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;

            cmlResponse<cmlResType> oResponse;
            List<cmlResType> aoType;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResType>();
                aoType = new List<cmlResType>();
                oDatabase = new cDatabase();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPtyCode ");
                                oSql.AppendLine(" FROM TCNMPdtType ");
                                oSql.AppendLine(" WHERE FTPtyCode = '" + poParam.ptPtyCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine("  UPDATE TCNMPdtType ");
                                    oSql.AppendLine("  WITH(ROWLOCK) ");
                                    oSql.AppendLine("  SET  FTPtyName = '" + poParam.ptPtyName + "', ");
                                    oSql.AppendLine("  FDDateUpd = CONVERT(VARCHAR(10), GETDATE(), 121) , ");
                                    oSql.AppendLine("  FTTimeUpd = CONVERT(VARCHAR(8),GETDATE(),114),");
                                    oSql.AppendLine("  FTWhoUpd = 'AdaLink' ");
                                    oSql.AppendLine("  OUTPUT INSERTED.FTPtyCode AS rtPtyCode , INSERTED.FTPtyName AS rtPtyName  ");
                                    oSql.AppendLine("  WHERE FTPtyCode = '" + poParam.ptPtyCode + "'");


                                    aoType = oDatabase.C_DATaSqlQuery<cmlResType>(oSql.ToString());

                                    oResponse.roItem = aoType;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;


                                }
                                else
                                {
                                    oResponse.roItem = aoType;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }

                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoType;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
                 

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoType = new List<cmlResType>();
                oResponse = new cmlResponse<cmlResType>();
                oResponse.roItem = aoType;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoType = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

        }

        /// <summary>
        /// Delete Product Type
        /// </summary>
        /// <param name="poParam"> Information type product.</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (product type code). <br/>
        /// 813 : data is referent in another data. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Delete/Item")]
        [HttpPost]
        public cmlResponse<cmlResTypeDel> POST_TYPoDelType([FromBody] cmlReqTypeDel poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            DataTable oDbtbl;

            cmlResponse<cmlResTypeDel> oResponse;
            cmlResTypeDel oItem;
            List<cmlResTypeDel> aoType;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit, tPdt;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResTypeDel>();
                aoType = new List<cmlResTypeDel>();
                oDatabase = new cDatabase();
                oItem = new cmlResTypeDel();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPtyCode ");
                                oSql.AppendLine(" FROM TCNMPdtType ");
                                oSql.AppendLine(" WHERE FTPtyCode = '" + poParam.ptPtyCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT TOP(1) FTPtyCode ");
                                    oSql.AppendLine(" FROM TCNMPdt ");
                                    oSql.AppendLine(" WHERE FTPtyCode = '" + poParam.ptPtyCode + "'");
                                    tPdt = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                    if (string.IsNullOrEmpty(tPdt))
                                    {

                                        oSql.Clear();
                                        oSql.AppendLine(" DELETE  FROM TCNMPdtType  ");
                                        oSql.AppendLine(" WITH(ROWLOCK) ");
                                        oSql.AppendLine(" WHERE FTPtyCode = '" + poParam.ptPtyCode + "' ");
                                        oDatabase.C_DATnExecuteSql(oSql.ToString());
                                        oItem.rtPtyCode = poParam.ptPtyCode;
                                        aoType.Add(oItem);

                                        oResponse.roItem = aoType;
                                        oResponse.rtCode = oMsg.tMS_RespCode1;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc1;
                                    }
                                    else
                                    {
                                        oResponse.roItem = aoType;
                                        oResponse.rtCode = oMsg.tMS_RespCode813;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc813PdtUnit;
                                    }


                                }
                                else
                                {
                                    oResponse.roItem = aoType;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }
                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoType;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }

             

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoType = new List<cmlResTypeDel>();
                oResponse = new cmlResponse<cmlResTypeDel>();
                oResponse.roItem = aoType;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoType = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

        }
    }
}

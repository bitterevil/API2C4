﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API2C4.Class.Standard;
using API2C4.Models.WebService.Request.Brand;
using API2C4.Models.WebService.Response.Brand;
using API2C4.Models.WebService.Response.Base;
using API2PosV4Master.Class.Standard;
using System.Text;
using API2C4.Class;
using API2C4.Models.Database;
using System.Threading;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Data;

namespace API2C4.Controllers
{
    /// <summary>
    /// Management Product Brand
    /// </summary>
    [RoutePrefix("V1/Brand")]
    public class cBrandController : ApiController
    {
        /// <summary>
        /// Insert Product Brand
        /// </summary>
        /// <param name="poParam">Information brand product</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 702 : product brand duplicate. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Insert/Item")]
        [HttpPost]
        public cmlResponse<cmlResBrandInsItem> POST_PBNoInsBrandProduct([FromBody] cmlReqBrand poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;

            List<cmlTSysConfig> aoSysConfig;
            List<cmlResBrandInsItem> aoType;
            cmlResponse<cmlResBrandInsItem> oResponse;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit;
            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oDatabase = new cDatabase();
                aoType = new List<cmlResBrandInsItem>();
                oResponse = new cmlResponse<cmlResBrandInsItem>();


                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");


                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPbnCode ");
                                oSql.AppendLine(" FROM TCNMPdtBrand ");
                                oSql.AppendLine(" WHERE FTPbnCode = '" + poParam.ptPbnCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" INSERT ");
                                    oSql.AppendLine(" INTO TCNMPdtBrand WITH(ROWLOCK) (FTPbnCode, FTPbnName, ");
                                    oSql.AppendLine(" FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns) ");
                                    oSql.AppendLine(" OUTPUT INSERTED.FTPbnCode AS rtPbnCode , INSERTED.FTPbnName AS rtPbnName ");
                                    oSql.AppendLine(" VALUES( ");
                                    oSql.AppendLine(" '" + poParam.ptPbnCode + "', '" + poParam.ptPbnName + "', ");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink',");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink'");
                                    oSql.AppendLine(" )");

                                    aoType = oDatabase.C_DATaSqlQuery<cmlResBrandInsItem>(oSql.ToString());

                                    oResponse.roItem = aoType;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;


                                }
                                else
                                {
                                    oResponse.roItem = aoType;
                                    oResponse.rtCode = oMsg.tMS_RespCode702;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc702PdtPbn;
                                }

                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoType;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
                 

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoType = new List<cmlResBrandInsItem>();
                oResponse = new cmlResponse<cmlResBrandInsItem>();
                oResponse.roItem = aoType;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoType = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Update Product Brand 
        /// </summary>
        /// <param name="poParam">Information brand product</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 702 : product brand duplicate. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Update/Item")]
        [HttpPost]
        public cmlResponse<cmlResBrandInsItem> POST_PBNoUptBrandProduct([FromBody] cmlReqBrandUpt poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;

            cmlResponse<cmlResBrandInsItem> oResponse;
            List<cmlResBrandInsItem> aoBrand;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResBrandInsItem>();
                aoBrand = new List<cmlResBrandInsItem>();
                oDatabase = new cDatabase();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPbnCode ");
                                oSql.AppendLine(" FROM TCNMPdtBrand ");
                                oSql.AppendLine(" WHERE FTPbnCode = '" + poParam.ptPbnCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine("  UPDATE TCNMPdtBrand ");
                                    oSql.AppendLine("  WITH(ROWLOCK) ");
                                    oSql.AppendLine("  SET  FTPbnName = '" + poParam.ptPbnName + "', ");
                                    oSql.AppendLine("  FDDateUpd = CONVERT(VARCHAR(10), GETDATE(), 121) , ");
                                    oSql.AppendLine("  FTTimeUpd = CONVERT(VARCHAR(8),GETDATE(),114),");
                                    oSql.AppendLine("  FTWhoUpd = 'AdaLink' ");
                                    oSql.AppendLine("  OUTPUT INSERTED.FTPbnCode AS rtPbnCode , INSERTED.FTPbnName AS rtPbnName  ");
                                    oSql.AppendLine("  WHERE FTPbnCode = '" + poParam.ptPbnCode + "'");


                                    aoBrand = oDatabase.C_DATaSqlQuery<cmlResBrandInsItem>(oSql.ToString());

                                    oResponse.roItem = aoBrand;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;


                                }
                                else
                                {
                                    oResponse.roItem = aoBrand;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }

                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoBrand;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
                  

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoBrand = new List<cmlResBrandInsItem>();
                oResponse = new cmlResponse<cmlResBrandInsItem>();
                oResponse.roItem = aoBrand;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoBrand = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Delete Product Brand
        /// </summary>
        /// <param name="poParam">Information Brand</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (product brand code). <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Delete/Item")]
        [HttpPost]
        public cmlResponse<cmlResBrandDelItem> POST_TYPoDelType([FromBody] cmlReqBrandDel poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            DataTable oDbtbl;

            cmlResponse<cmlResBrandDelItem> oResponse;
            cmlResBrandDelItem oItem;
            List<cmlResBrandDelItem> aoBrand;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit, tPdt;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResBrandDelItem>();
                aoBrand = new List<cmlResBrandDelItem>();
                oDatabase = new cDatabase();
                oItem = new cmlResBrandDelItem();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPbnCode ");
                                oSql.AppendLine(" FROM TCNMPdtBrand ");
                                oSql.AppendLine(" WHERE FTPbnCode = '" + poParam.ptPbnCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT TOP(1) FTPbnCode ");
                                    oSql.AppendLine(" FROM TCNMPdt ");
                                    oSql.AppendLine(" WHERE FTPbnCode = '" + poParam.ptPbnCode + "'");
                                    tPdt = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                    if (string.IsNullOrEmpty(tPdt))
                                    {

                                        oSql.Clear();
                                        oSql.AppendLine(" DELETE  FROM TCNMPdtBrand  ");
                                        oSql.AppendLine(" WITH(ROWLOCK) ");
                                        oSql.AppendLine(" WHERE FTPbnCode = '" + poParam.ptPbnCode + "' ");
                                        oDatabase.C_DATnExecuteSql(oSql.ToString());

                                        oItem.rtPbnCode = poParam.ptPbnCode;
                                        aoBrand.Add(oItem);

                                        oResponse.roItem = aoBrand;
                                        oResponse.rtCode = oMsg.tMS_RespCode1;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc1;
                                    }
                                    else
                                    {
                                        oResponse.roItem = aoBrand;
                                        oResponse.rtCode = oMsg.tMS_RespCode813;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc813PdtUnit;
                                    }


                                }
                                else
                                {
                                    oResponse.roItem = aoBrand;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }
                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoBrand;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
       

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoBrand = new List<cmlResBrandDelItem>();
                oResponse = new cmlResponse<cmlResBrandDelItem>();
                oResponse.roItem = aoBrand;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoBrand = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }


        }
    }
}

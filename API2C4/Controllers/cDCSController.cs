﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API2C4.Class;
using API2C4.Class.Standard;
using API2C4.Models.WebService.Request.DCS;
using API2C4.Models.WebService.Response.DCS;
using API2C4.Models.WebService.Response.Base;
using API2PosV4Master.Class.Standard;
using API2C4.Models.Database;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Data;

namespace API2C4.Controllers
{
    /// <summary>
    /// Management Product DCS
    /// </summary>
    [RoutePrefix("V1/DCS")]
    public class cDCSController : ApiController
    {
        /// <summary>
        /// Insert Group Product DCS
        /// </summary>
        /// <param name="poParam">Information DCS</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 702 : DCS code duplicate. <br/>
        /// 706 : product depart not found in depart master. <br/>
        /// 707 : product class not found in class master. <br/>
        /// 708 : product sub class not found in sub class master. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Insert/Item")]
        [HttpPost]
        public cmlResponse<cmlResDCSUpdItem> POST_DCSoInsertDCS([FromBody] cmlReqDcs poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;

            List<cmlTSysConfig> aoSysConfig;
            List<cmlResDCSUpdItem> aoDcs;
            cmlResDCSUpdItem oItem;
            cmlResponse<cmlResDCSUpdItem> oResponse;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tScl, tCls, tDep,tDCS;
            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oDatabase = new cDatabase();
                aoDcs = new List<cmlResDCSUpdItem>();
                oResponse = new cmlResponse<cmlResDCSUpdItem>();
                oItem = new cmlResDCSUpdItem();


                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");


                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();
                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();

                                // Check Clas
                                if (poParam.ptClsCode != null && poParam.ptClsCode != "")
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT FTClsCode");
                                    oSql.AppendLine(" FROM TCNMPdtClass");
                                    oSql.AppendLine(" WHERE FTClsCode = '" + poParam.ptClsCode + "'");
                                    tCls = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                    if (string.IsNullOrEmpty(tCls))
                                    {
                                        oResponse.roItem = aoDcs;
                                        oResponse.rtCode = oMsg.tMS_RespCode707;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc707DCSPdt;
                                        return oResponse;
                                    }
                                }

                                // Check Sub class
                                if (poParam.ptSclCode != null && poParam.ptSclCode != "")
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT FTSclCode");
                                    oSql.AppendLine(" FROM TCNMPdtSubClass");
                                    oSql.AppendLine(" WHERE FTSclCode = '" + poParam.ptSclCode + "'");
                                    tScl = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                    if (string.IsNullOrEmpty(tScl))
                                    {
                                        oResponse.roItem = aoDcs;
                                        oResponse.rtCode = oMsg.tMS_RespCode708;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc708DCSPdt;
                                        return oResponse;
                                    }
                                }

                                // Check Department 
                                oSql.Clear();
                                oSql.AppendLine(" SELECT  FTDepCode");
                                oSql.AppendLine(" FROM TCNMPdtDepart");
                                oSql.AppendLine(" WHERE FTDepCode ='" + poParam.ptDepCode + "'");
                                tDep = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (!string.IsNullOrEmpty(tDep))
                                {
                                    //Check Dupicate DCS
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT FTDcsCode");
                                    oSql.AppendLine(" FROM TCNMPdtDCS");
                                    oSql.AppendLine(" WHERE FTDcsCode = '" + poParam.ptDcsCode + "'");
                                    tDCS = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                    if (string.IsNullOrEmpty(tDCS))
                                    {

                                        //Insert DCS
                                        oSql.Clear();
                                        oSql.AppendLine(" " + oFunc.SP_SQLtGeneralCmdInsert<cmlReqDcs>(poParam) + "");
                                        oDatabase.C_DATnExecuteSql(oSql.ToString());

                                        oItem.rtDcsCode = poParam.ptDcsCode;
                                        oItem.rtDcsName = poParam.ptDcsName;
                                        oItem.rtDepCode = poParam.ptDepCode;
                                        oItem.rtClsCode = poParam.ptClsCode;
                                        oItem.rtSclCode = poParam.ptSclCode;
                                        aoDcs.Add(oItem);

                                        oResponse.roItem = aoDcs;
                                        oResponse.rtCode = oMsg.tMS_RespCode1;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc1;


                                    }
                                    else
                                    {
                                        oResponse.roItem = aoDcs;
                                        oResponse.rtCode = oMsg.tMS_RespCode702;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc702DCSPdt;
                                    }
                                }
                                else
                                {
                                    oResponse.roItem = aoDcs;
                                    oResponse.rtCode = oMsg.tMS_RespCode706;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc706DCSPdt;
                                }



                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoDcs;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
               

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoDcs = new List<cmlResDCSUpdItem>();
                oResponse = new cmlResponse<cmlResDCSUpdItem>();
                oResponse.roItem = aoDcs;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoDcs = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Update Group Product DCS
        /// </summary>
        /// <param name="poParam">Information DCS</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 706 : product depart not found in depart master. <br/>
        /// 707 : product class not found in class master. <br/>
        /// 708 : product sub class not found in sub class master. <br/>
        /// 800 : data not found. (product DCS code). <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Update/Item")]
        public cmlResponse<cmlResDCSUpdItem> POST_DCSoUptDCS([FromBody] cmlReqUptDcs poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;

            List<cmlTSysConfig> aoSysConfig;
            List<cmlResDCSUpdItem> aoDcs;
            cmlResDCSUpdItem oItem;
            cmlResponse<cmlResDCSUpdItem> oResponse;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tScl, tCls, tDep, tDCS,tUpt;
            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oDatabase = new cDatabase();
                aoDcs = new List<cmlResDCSUpdItem>();
                oResponse = new cmlResponse<cmlResDCSUpdItem>();
                oItem = new cmlResDCSUpdItem();
                tUpt = "";


                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");


                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();

                                if (poParam.ptDcsName != null && poParam.ptDcsName != "" )
                                {
                                    tUpt += " FTDcsName ='" + poParam.ptDcsName + "',";
                                }

                                // Check Class
                                if (poParam.ptClsCode != null && poParam.ptClsCode != "" )
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT FTClsCode");
                                    oSql.AppendLine(" FROM TCNMPdtClass");
                                    oSql.AppendLine(" WHERE FTClsCode = '" + poParam.ptClsCode + "'");
                                    tCls = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                    if (string.IsNullOrEmpty(tCls))
                                    {
                                        oResponse.roItem = aoDcs;
                                        oResponse.rtCode = oMsg.tMS_RespCode707;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc707DCSPdt;
                                        return oResponse;
                                    }
                                    else
                                    {
                                        tUpt += " FTClsCode ='" + poParam.ptClsCode + "', ";
                                    }
                                }

                                // Check Sub class
                                if (poParam.ptSclCode != null && poParam.ptSclCode != "")
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT FTSclCode");
                                    oSql.AppendLine(" FROM TCNMPdtSubClass");
                                    oSql.AppendLine(" WHERE FTSclCode = '" + poParam.ptSclCode + "'");
                                    tScl = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                    if (string.IsNullOrEmpty(tScl))
                                    {
                                        oResponse.roItem = aoDcs;
                                        oResponse.rtCode = oMsg.tMS_RespCode708;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc708DCSPdt;
                                        return oResponse;
                                    }
                                    else
                                    {
                                        tUpt += " FTSclCode ='" + poParam.ptSclCode + "', ";
                                    }
                                }

                                // Check Department 
                                if (poParam.ptDepCode != "")
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT  FTDepCode");
                                    oSql.AppendLine(" FROM TCNMPdtDepart");
                                    oSql.AppendLine(" WHERE FTDepCode ='" + poParam.ptDepCode + "'");
                                    tDep = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                    if (string.IsNullOrEmpty(tDep))
                                    {
                                        oResponse.roItem = aoDcs;
                                        oResponse.rtCode = oMsg.tMS_RespCode706;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc706DCSPdt;
                                        return oResponse;
                                    }
                                    else
                                    {
                                        tUpt += " FTDepCode ='" + poParam.ptDepCode + "', ";
                                    }
                                }

                                //Check Dupicate DCS
                                oSql.Clear();
                                oSql.AppendLine(" SELECT FTDcsCode");
                                oSql.AppendLine(" FROM TCNMPdtDCS");
                                oSql.AppendLine(" WHERE FTDcsCode = '" + poParam.ptDcsCode + "'");
                                tDCS = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (!string.IsNullOrEmpty(tDCS))
                                {

                                    //Insert DCS
                                    oSql.Clear();
                                    oSql.AppendLine(" UPDATE TCNMPdtDCS ");
                                    oSql.AppendLine(" SET ");
                                    oSql.AppendLine(" " + tUpt + "");
                                    oSql.AppendLine("  FDDateUpd = CONVERT(VARCHAR(10), GETDATE(), 121) , ");
                                    oSql.AppendLine("  FTTimeUpd = CONVERT(VARCHAR(8),GETDATE(),114),");
                                    oSql.AppendLine("  FTWhoUpd = 'AdaLink' ");
                                    oSql.AppendLine(" WHERE FTDcsCode = '" + poParam.ptDcsCode + "'");
                                    oDatabase.C_DATnExecuteSql(oSql.ToString());

                                    oItem.rtDcsCode = poParam.ptDcsCode;
                                    oItem.rtDcsName = poParam.ptDcsName;
                                    oItem.rtDepCode = poParam.ptDepCode;
                                    oItem.rtClsCode = poParam.ptClsCode;
                                    oItem.rtSclCode = poParam.ptSclCode;
                                    aoDcs.Add(oItem);

                                    oResponse.roItem = aoDcs;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;

                                }
                                else
                                {
                                    oResponse.roItem = aoDcs;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }

                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoDcs;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
                

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoDcs = new List<cmlResDCSUpdItem>();
                oResponse = new cmlResponse<cmlResDCSUpdItem>();
                oResponse.roItem = aoDcs;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoDcs = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Delete Group Product DCS
        /// </summary>
        /// <param name="poParam">Information DCS</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (product DCS code). <br/>
        /// 813 : data is referent in another data. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method. <br/>
        /// </returns>
        [Route("Delete/Item")]
        public cmlResponse<cmlResDCSDelItem> POST_DCSoDelDCS([FromBody] cmlReqDelDcs poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;

            cmlResponse<cmlResDCSDelItem> oResponse;
            cmlResDCSDelItem oItem;
            List<cmlResDCSDelItem> aoDCS;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit, tPdt;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResDCSDelItem>();
                aoDCS = new List<cmlResDCSDelItem>();
                oDatabase = new cDatabase();
                oItem = new cmlResDCSDelItem();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTDcsCode ");
                                oSql.AppendLine(" FROM TCNMPdtDCS ");
                                oSql.AppendLine(" WHERE FTDcsCode = '" + poParam.ptDcsCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT TOP(1) FTDcsCode ");
                                    oSql.AppendLine(" FROM TCNMPdt ");
                                    oSql.AppendLine(" WHERE FTDcsCode = '" + poParam.ptDcsCode + "'");
                                    tPdt = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                    if (string.IsNullOrEmpty(tPdt))
                                    {

                                        oSql.Clear();
                                        oSql.AppendLine(" DELETE  FROM TCNMPdtDCS  ");
                                        oSql.AppendLine(" WITH(ROWLOCK) ");
                                        oSql.AppendLine(" WHERE FTDcsCode = '" + poParam.ptDcsCode + "' ");
                                        oDatabase.C_DATnExecuteSql(oSql.ToString());
                                        oItem.rtDcsCode = poParam.ptDcsCode;
                                        aoDCS.Add(oItem);

                                        oResponse.roItem = aoDCS;
                                        oResponse.rtCode = oMsg.tMS_RespCode1;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc1;
                                    }
                                    else
                                    {
                                        oResponse.roItem = aoDCS;
                                        oResponse.rtCode = oMsg.tMS_RespCode813;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc813PdtUnit;
                                    }
                                }
                                else
                                {
                                    oResponse.roItem = aoDCS;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }
                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoDCS;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
                  

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoDCS = new List<cmlResDCSDelItem>();
                oResponse = new cmlResponse<cmlResDCSDelItem>();
                oResponse.roItem = aoDCS;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoDCS = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        
    }
}

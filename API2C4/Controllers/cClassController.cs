﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API2C4.Class;
using API2C4.Class.Standard;
using API2C4.Models.Database;
using API2C4.Models.WebService.Request.Class;
using API2C4.Models.WebService.Response.Class;
using API2C4.Models.WebService.Response.Base;
using API2PosV4Master.Class.Standard;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Data;

namespace API2C4.Controllers
{
    /// <summary>
    ///  Management Product Class
    /// </summary>
    [RoutePrefix("V1/Class")]
    public class cClassController : ApiController
    {

        /// <summary>
        /// Insert Product Class
        /// </summary>
        /// <param name="poParam">Information class product</param>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 702 : product class duplicate. <br/> 
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// <returns>
        /// </returns>
        [Route("Insert/Item")]
        [HttpPost]
        public cmlResponse<cmlResClass>POST_CLSoInsClass([FromBody] cmlReqClass poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;

            List<cmlTSysConfig> aoSysConfig;
            List<cmlResClass> aoCls;
            cmlResponse<cmlResClass> oResponse;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit;
            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oDatabase = new cDatabase();
                aoCls = new List<cmlResClass>();
                oResponse = new cmlResponse<cmlResClass>();


                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");


                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTClsCode ");
                                oSql.AppendLine(" FROM TCNMPdtClass ");
                                oSql.AppendLine(" WHERE FTClsCode = '" + poParam.ptClsCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" INSERT ");
                                    oSql.AppendLine(" INTO TCNMPdtClass WITH(ROWLOCK) (FTClsCode, FTClsName, ");
                                    oSql.AppendLine(" FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns) ");
                                    oSql.AppendLine(" OUTPUT INSERTED.FTClsCode AS rtClsCode , INSERTED.FTClsName AS rtClsName ");
                                    oSql.AppendLine(" VALUES( ");
                                    oSql.AppendLine(" '" + poParam.ptClsCode + "', '" + poParam.ptClsName + "', ");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink',");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink'");
                                    oSql.AppendLine(" )");

                                    aoCls = oDatabase.C_DATaSqlQuery<cmlResClass>(oSql.ToString());

                                    oResponse.roItem = aoCls;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;

                                }
                                else
                                {
                                    oResponse.roItem = aoCls;
                                    oResponse.rtCode = oMsg.tMS_RespCode702;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc702ClsPdt;
                                }

                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoCls;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;

                    }
                

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoCls = new List<cmlResClass>();
                oResponse = new cmlResponse<cmlResClass>();
                oResponse.roItem = aoCls;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoCls = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Update Product Class
        /// </summary>
        /// <param name="poParam">Information class product</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (product class code). <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method. <br/>
        /// </returns>
        [Route("Update/Item")]
        [HttpPost]
        public cmlResponse<cmlResClass>POST_CLSoUptClass([FromBody] cmlReqClassUpt poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;

            cmlResponse<cmlResClass> oResponse;
            List<cmlResClass> aoCls;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tCls;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResClass>();
                aoCls = new List<cmlResClass>();
                oDatabase = new cDatabase();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTClsCode ");
                                oSql.AppendLine(" FROM TCNMPdtClass ");
                                oSql.AppendLine(" WHERE FTClsCode = '" + poParam.ptClsCode + "'");
                                tCls = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                if (!string.IsNullOrEmpty(tCls))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine("  UPDATE TCNMPdtClass ");
                                    oSql.AppendLine("  WITH(ROWLOCK) ");
                                    oSql.AppendLine("  SET  FTClsName = '" + poParam.ptClsName + "', ");
                                    oSql.AppendLine("  FDDateUpd = CONVERT(VARCHAR(10), GETDATE(), 121) , ");
                                    oSql.AppendLine("  FTTimeUpd = CONVERT(VARCHAR(8),GETDATE(),114),");
                                    oSql.AppendLine("  FTWhoUpd = 'AdaLink' ");
                                    oSql.AppendLine("  OUTPUT INSERTED.FTClsCode AS rtClsCode , INSERTED.FTClsName AS rtClsName  ");
                                    oSql.AppendLine("  WHERE FTClsCode = '" + poParam.ptClsCode + "'");


                                    aoCls = oDatabase.C_DATaSqlQuery<cmlResClass>(oSql.ToString());

                                    oResponse.roItem = aoCls;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;


                                }
                                else
                                {
                                    oResponse.roItem = aoCls;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }

                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoCls;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }

                

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoCls = new List<cmlResClass>();
                oResponse = new cmlResponse<cmlResClass>();
                oResponse.roItem = aoCls;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoCls = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Delete Product Class
        /// </summary>
        /// <param name="poParam">Information Product class</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (product class code). <br/>
        /// 813 : data is referent in another data. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Delete/Item")]
        [HttpPost]
        public cmlResponse<cmlResClassDelItem>POST_CLSoDelClass([FromBody] cmlReqDelClass poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            DataTable oDbtbl;

            cmlResponse<cmlResClassDelItem> oResponse;
            cmlResClassDelItem oItem;
            List<cmlResClassDelItem> aoType;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit, tPdt;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResClassDelItem>();
                aoType = new List<cmlResClassDelItem>();
                oDatabase = new cDatabase();
                oItem = new cmlResClassDelItem();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTClsCode ");
                                oSql.AppendLine(" FROM TCNMPdtClass ");
                                oSql.AppendLine(" WHERE FTClsCode = '" + poParam.ptClsCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT TOP(1) FTClsCode ");
                                    oSql.AppendLine(" FROM TCNMPdtDCS ");
                                    oSql.AppendLine(" WHERE FTClsCode = '" + poParam.ptClsCode + "'");
                                    tPdt = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                    if (string.IsNullOrEmpty(tPdt))
                                    {

                                        oSql.Clear();
                                        oSql.AppendLine(" DELETE  FROM TCNMPdtClass  ");
                                        oSql.AppendLine(" WITH(ROWLOCK) ");
                                        oSql.AppendLine(" WHERE FTClsCode = '" + poParam.ptClsCode + "' ");
                                        oDatabase.C_DATnExecuteSql(oSql.ToString());
                                        oItem.rtClsCode = poParam.ptClsCode;
                                        aoType.Add(oItem);

                                        oResponse.roItem = aoType;
                                        oResponse.rtCode = oMsg.tMS_RespCode1;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc1;
                                    }
                                    else
                                    {
                                        oResponse.roItem = aoType;
                                        oResponse.rtCode = oMsg.tMS_RespCode813;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc813PdtUnit;
                                    }


                                }
                                else
                                {
                                    oResponse.roItem = aoType;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }
                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoType;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }

                

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoType = new List<cmlResClassDelItem>();
                oResponse = new cmlResponse<cmlResClassDelItem>();
                oResponse.roItem = aoType;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoType = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API2C4.Models.WebService.Request.Unit;
using API2C4.Models.WebService.Response.Unit;
using API2C4.Models.WebService.Response.Base;
using API2C4.Models.Database;
using API2C4.Class.Standard;
using API2PosV4Master.Class.Standard;
using System.Text;
using API2C4.Class;
using System.Threading;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Data;

namespace API2C4.Controllers
{
    /// <summary>
    /// Information Product Unit
    /// </summary>
    [RoutePrefix("V1/Unit")]
    public class cUnitController : ApiController
    {
        /// <summary>
        /// Insert Product Unit
        /// </summary>
        /// <param name="poParam">Information Unit</param>
        /// <returns> 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 702 : unit code duplicate.   <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method. <br/>
        /// </returns>
        [Route("Insert/Item")]
        [HttpPost]
        public cmlResponse<cmlResUnitUpdItem> POST_UNToInsertUnit([FromBody] cmlReqUnit poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;

            List<cmlTSysConfig> aoSysConfig;
            List<cmlResUnitUpdItem> aoUnitRes;
            cmlResponse<cmlResUnitUpdItem> oResponse;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit;
            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new  cCS();
                oDatabase = new cDatabase();
                aoUnitRes = new List<cmlResUnitUpdItem>();
                oResponse = new cmlResponse<cmlResUnitUpdItem>();
 

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

               
                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();
                  
                    if (oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {

                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPunCode ");
                                oSql.AppendLine(" FROM TCNMPdtUnit ");
                                oSql.AppendLine(" WHERE FTPunCode = '" + poParam.ptPunCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" INSERT ");
                                    oSql.AppendLine(" INTO TCNMPdtUnit WITH(ROWLOCK) (FTPunCode, FTPunName, FDDateUpd, ");
                                    oSql.AppendLine(" FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns) ");
                                    oSql.AppendLine(" OUTPUT INSERTED.FTPunCode AS rtPunCode , INSERTED.FTPunName AS rtPunName ");
                                    oSql.AppendLine(" VALUES( ");
                                    oSql.AppendLine(" '" + poParam.ptPunCode + "', '" + poParam.ptPunName + "', ");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink',");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink'");
                                    oSql.AppendLine(" )");

                                    aoUnitRes = oDatabase.C_DATaSqlQuery<cmlResUnitUpdItem>(oSql.ToString());

                                    oResponse.roItem = aoUnitRes;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;


                                }
                                else
                                {
                                    oResponse.roItem = aoUnitRes;
                                    oResponse.rtCode = oMsg.tMS_RespCode702;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc702PdtUnit;
                                }




                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoUnitRes;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }




                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }


                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }


                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoUnitRes = new List<cmlResUnitUpdItem>();
                oResponse = new cmlResponse<cmlResUnitUpdItem>();
                oResponse.roItem = aoUnitRes;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoUnitRes = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

        }

        /// <summary>
        /// Update Product Unit
        /// </summary>
        /// <param name="poParam"> Information Unit</param>
        /// <returns> 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (unit code). <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.</returns>
        [Route("Update/Item")]
        [HttpPost]
        public cmlResponse<cmlResUnitUpdItem> POST_UNToUptUnit([FromBody] cmlReqUnitUpt poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;

            cmlResponse<cmlResUnitUpdItem> oResponse;
            List<cmlResUnitUpdItem> aoUint;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResUnitUpdItem>();
                aoUint = new List<cmlResUnitUpdItem>(); 
                oDatabase = new cDatabase();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();
                    
                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPunCode ");
                                oSql.AppendLine(" FROM TCNMPdtUnit ");
                                oSql.AppendLine(" WHERE FTPunCode = '" + poParam.ptPunCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine("  UPDATE TCNMPdtUnit ");
                                    oSql.AppendLine("  WITH(ROWLOCK) ");
                                    oSql.AppendLine("  SET  FTPunName = '" + poParam.ptPunName + "', ");
                                    oSql.AppendLine("  FDDateUpd = CONVERT(VARCHAR(10), GETDATE(), 121) , ");
                                    oSql.AppendLine("  FTTimeUpd = CONVERT(VARCHAR(8),GETDATE(),114),");
                                    oSql.AppendLine("  FTWhoUpd = 'AdaLink' ");
                                    oSql.AppendLine("  OUTPUT INSERTED.FTPunCode AS rtPunCode , INSERTED.FTPunName AS rtPunName  ");
                                    oSql.AppendLine("  WHERE FTPunCode = '" + poParam.ptPunCode + "'");


                                    aoUint = oDatabase.C_DATaSqlQuery<cmlResUnitUpdItem>(oSql.ToString());

                                    oResponse.roItem = aoUint;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;


                                }
                                else
                                {
                                    oResponse.roItem = aoUint;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }




                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoUint;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
              

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoUint = new List<cmlResUnitUpdItem>();
                oResponse = new cmlResponse<cmlResUnitUpdItem>();
                oResponse.roItem = aoUint;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoUint = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Delete Product Unit
        /// </summary>
        /// <param name="poParam"> Information Unit </param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (unit code). <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        /// 
        [Route("Delete/Item")]
        [HttpPost]
        public cmlResponse<cmlReqUnitDel> POST_UNToDelUnit([FromBody] cmlReqUnitDel poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            DataTable oDbtbl;

            cmlResponse<cmlReqUnitDel> oResponse;
            cmlReqUnitDel oItem;
            List<cmlReqUnitDel> aoUint;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit,tPdt;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlReqUnitDel>();
                aoUint = new List<cmlReqUnitDel>();
                oDatabase = new cDatabase();
                oItem = new cmlReqUnitDel();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();
                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTPunCode ");
                                oSql.AppendLine(" FROM TCNMPdtUnit ");
                                oSql.AppendLine(" WHERE FTPunCode = '" + poParam.ptPunCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT TOP(1) FTPdtSUnit,FTPdtMUnit,FTPdtLUnit");
                                    oSql.AppendLine(" FROM TCNMPdt ");
                                    oSql.AppendLine(" WHERE FTPdtSUnit = '" + poParam.ptPunCode + "' OR ");
                                    oSql.AppendLine(" FTPdtMUnit = '" + poParam.ptPunCode + "' OR");
                                    oSql.AppendLine(" FTPdtLUnit = '" + poParam.ptPunCode + "'");
                                    oDbtbl = new DataTable();
                                    oDbtbl = oDatabase.C_DAToSqlQueryDt(oSql.ToString());

                                    if (oDbtbl.Rows.Count == 0)
                                    {

                                        oSql.Clear();
                                        oSql.AppendLine(" DELETE  FROM TCNMPdtUnit  ");
                                        oSql.AppendLine(" WITH(ROWLOCK) ");
                                        oSql.AppendLine(" WHERE FTPunCode = '" + poParam.ptPunCode + "' ");
                                        oDatabase.C_DATnExecuteSql(oSql.ToString());
                                        oItem.ptPunCode = poParam.ptPunCode;
                                        aoUint.Add(oItem);

                                        oResponse.roItem = aoUint;
                                        oResponse.rtCode = oMsg.tMS_RespCode1;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc1;
                                    }
                                    else
                                    {
                                        oResponse.roItem = aoUint;
                                        oResponse.rtCode = oMsg.tMS_RespCode813;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc813PdtUnit;
                                    }


                                }
                                else
                                {
                                    oResponse.roItem = aoUint;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }
                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoUint;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
               

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoUint = new List<cmlReqUnitDel>();
                oResponse = new cmlResponse<cmlReqUnitDel>();
                oResponse.roItem = aoUint;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoUint = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API2C4.Models.WebService.Request.Scl;
using API2C4.Models.WebService.Response.Scl;
using API2C4.Models.WebService.Response.Base;
using API2C4.Class.Standard;
using API2C4.Class;
using API2PosV4Master.Class.Standard;
using System.Text;
using API2C4.Models.Database;
using System.Threading;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Data;

namespace API2C4.Controllers
{

    /// <summary>
    /// Management Product Subclass
    /// </summary>
    [RoutePrefix("V1/SubClass")]
    public class cSubClassController : ApiController
    {
        /// <summary>
        /// Insert Product SubClass
        /// </summary>
        /// <param name="poParam">Information SubClass</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 702 : product sub class duplicate. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Insert/Item")]
        public cmlResponse<cmlResSubClassInsItem> POST_SCLoInsSubclass([FromBody] cmlReqScl poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;

            List<cmlTSysConfig> aoSysConfig;
            List<cmlResSubClassInsItem> aoScl;
            cmlResponse<cmlResSubClassInsItem> oResponse;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit;
            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();
                oDatabase = new cDatabase();
                aoScl = new List<cmlResSubClassInsItem>();
                oResponse = new cmlResponse<cmlResSubClassInsItem>();


                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");


                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();
                    
                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTSclCode ");
                                oSql.AppendLine(" FROM TCNMPdtSubClass ");
                                oSql.AppendLine(" WHERE FTSclCode = '" + poParam.ptSclCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" INSERT ");
                                    oSql.AppendLine(" INTO TCNMPdtSubClass WITH(ROWLOCK) (FTSclCode, FTSclName, ");
                                    oSql.AppendLine(" FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns) ");
                                    oSql.AppendLine(" OUTPUT INSERTED.FTSclCode AS rtSclCode , INSERTED.FTSclName AS rtSclName ");
                                    oSql.AppendLine(" VALUES( ");
                                    oSql.AppendLine(" '" + poParam.ptSclCode + "', '" + poParam.ptSclName + "', ");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink',");
                                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114), ");
                                    oSql.AppendLine(" 'AdaLink'");
                                    oSql.AppendLine(" )");

                                    aoScl = oDatabase.C_DATaSqlQuery<cmlResSubClassInsItem>(oSql.ToString());

                                    oResponse.roItem = aoScl;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;

                                }
                                else
                                {
                                    oResponse.roItem = aoScl;
                                    oResponse.rtCode = oMsg.tMS_RespCode702;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc702SclPdt;
                                }

                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoScl;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
         

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoScl = new List<cmlResSubClassInsItem>();
                oResponse = new cmlResponse<cmlResSubClassInsItem>();
                oResponse.roItem = aoScl;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoScl = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Update Product SubClass
        /// </summary>
        /// <param name="poParam">Information SubClass</param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 800 : data not found. (product sub class code). <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database.<br/>
        /// 906 : this time not allowed to use method.
        /// </returns>
        [Route("Update/Item")]
        public cmlResponse<cmlResSubClassInsItem> POST_SCLoUptSubclass([FromBody] cmlReqSclUpt poParam)
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;

            cmlResponse<cmlResSubClassInsItem> oResponse;
            List<cmlResSubClassInsItem> aoScl;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResSubClassInsItem>();
                aoScl = new List<cmlResSubClassInsItem>();
                oDatabase = new cDatabase();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTSclCode ");
                                oSql.AppendLine(" FROM TCNMPdtSubClass ");
                                oSql.AppendLine(" WHERE FTSclCode = '" + poParam.ptSclCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine("  UPDATE TCNMPdtSubClass ");
                                    oSql.AppendLine("  WITH(ROWLOCK) ");
                                    oSql.AppendLine("  SET  FTSclName = '" + poParam.ptSclName + "', ");
                                    oSql.AppendLine("  FDDateUpd = CONVERT(VARCHAR(10), GETDATE(), 121) , ");
                                    oSql.AppendLine("  FTTimeUpd = CONVERT(VARCHAR(8),GETDATE(),114),");
                                    oSql.AppendLine("  FTWhoUpd = 'AdaLink' ");
                                    oSql.AppendLine("  OUTPUT INSERTED.FTSclCode AS rtSclCode , INSERTED.FTSclName AS rtSclName  ");
                                    oSql.AppendLine("  WHERE FTSclCode = '" + poParam.ptSclCode + "'");


                                    aoScl = oDatabase.C_DATaSqlQuery<cmlResSubClassInsItem>(oSql.ToString());

                                    oResponse.roItem = aoScl;
                                    oResponse.rtCode = oMsg.tMS_RespCode1;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc1;


                                }
                                else
                                {
                                    oResponse.roItem = aoScl;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }

                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoScl;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
                

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoScl = new List<cmlResSubClassInsItem>();
                oResponse = new cmlResponse<cmlResSubClassInsItem>();
                oResponse.roItem = aoScl;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoScl = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        /// <summary>
        /// Delete Product SubClass
        /// </summary>
        /// <param name="poParam">Information Subclass </param>
        /// <returns>
        /// 1 : success. <br/>
        /// 701 : validate parameter model false. <br/>
        /// 900 : service process false. <br/>
        /// 904 : key not allowed to use method. <br/>
        /// 905 : cannot connect database. <br/>
        /// 906 : this time not allowed to use method. <br/>
        /// </returns>
        [Route("Delete/Item")]
        public cmlResponse<cmlResSclDelItem> POST_SCLoDelSubclass([FromBody] cmlReqSclDelItem poParam  )
        {
            cSP oFunc;
            cMS oMsg;
            cCS oCons;

            StringBuilder oSql;
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            DataTable oDbtbl;

            cmlResponse<cmlResSclDelItem> oResponse;
            cmlResSclDelItem oItem;
            List<cmlResSclDelItem> aoType;

            int nRowEff, nConTme, nCmdTme;
            string tFuncName, tModelErr, tKeyApi, tErrCode, tErrDesc, tTblTmpName, tUnit, tPdt;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oCons = new cCS();

                aoSysConfig = new List<cmlTSysConfig>();
                oResponse = new cmlResponse<cmlResSclDelItem>();
                aoType = new List<cmlResSclDelItem>();
                oDatabase = new cDatabase();
                oItem = new cmlResSclDelItem();

                // Get method name.
                tFuncName = MethodBase.GetCurrentMethod().Name;

                tModelErr = "";
                if (oFunc.SP_CHKbPrmModel(ref tModelErr, ModelState))
                {
                    aoSysConfig = oFunc.SP_SYSaLoadConfig();

                    if(oFunc.SP_CHKbAllowRangeTime(aoSysConfig))
                    {
                        tKeyApi = "";
                        if (oFunc.SP_CHKbKeyAPI(ref tKeyApi, tFuncName, HttpContext.Current, aoSysConfig))
                        {
                            nConTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nConTme, cCS.nCS_ConTme, aoSysConfig, "003");
                            nCmdTme = 0;
                            oFunc.SP_DATxGetConfigFromMem<int>(ref nCmdTme, cCS.nCS_CmdTme, aoSysConfig, "004");

                            try
                            {
                                oSql = new StringBuilder();
                                oSql.AppendLine(" SELECT FTSclCode ");
                                oSql.AppendLine(" FROM TCNMPdtSubClass ");
                                oSql.AppendLine(" WHERE FTSclCode = '" + poParam.ptSclCode + "'");
                                tUnit = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());
                                if (!string.IsNullOrEmpty(tUnit))
                                {
                                    oSql.Clear();
                                    oSql.AppendLine(" SELECT TOP(1) FTSclCode ");
                                    oSql.AppendLine(" FROM TCNMPdtDCS ");
                                    oSql.AppendLine(" WHERE FTSclCode = '" + poParam.ptSclCode + "'");
                                    tPdt = oDatabase.C_DAToSqlQuery<string>(oSql.ToString());

                                    if (string.IsNullOrEmpty(tPdt))
                                    {

                                        oSql.Clear();
                                        oSql.AppendLine(" DELETE  FROM TCNMPdtSubClass  ");
                                        oSql.AppendLine(" WITH(ROWLOCK) ");
                                        oSql.AppendLine(" WHERE FTSclCode = '" + poParam.ptSclCode + "' ");
                                        oDatabase.C_DATnExecuteSql(oSql.ToString());
                                        oItem.rtSclCode = poParam.ptSclCode;
                                        aoType.Add(oItem);

                                        oResponse.roItem = aoType;
                                        oResponse.rtCode = oMsg.tMS_RespCode1;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc1;
                                    }
                                    else
                                    {
                                        oResponse.roItem = aoType;
                                        oResponse.rtCode = oMsg.tMS_RespCode813;
                                        oResponse.rtDesc = oMsg.tMS_RespDesc813PdtUnit;
                                    }


                                }
                                else
                                {
                                    oResponse.roItem = aoType;
                                    oResponse.rtCode = oMsg.tMS_RespCode800;
                                    oResponse.rtDesc = oMsg.tMS_RespDesc800;
                                }
                            }
                            catch (Exception oExn)
                            {
                                oResponse.roItem = aoType;
                                oResponse.rtCode = oMsg.tMS_RespCode900;
                                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                                return oResponse;
                            }
                        }
                        else
                        {
                            oResponse.rtCode = oMsg.tMS_RespCode904;
                            oResponse.rtDesc = oMsg.tMS_RespDesc904;
                        }
                    }
                    else
                    {
                        oResponse.rtCode = oMsg.tMS_RespCode906;
                        oResponse.rtDesc = oMsg.tMS_RespDesc906;
                    }
             

                }
                else
                {
                    oResponse.rtCode = oMsg.tMS_RespCode701;
                    oResponse.rtDesc = oMsg.tMS_RespDesc701 + tModelErr;
                }

                return oResponse;

            }
            catch (Exception oExn)
            {
                oMsg = new cMS();
                aoType = new List<cmlResSclDelItem>();
                oResponse = new cmlResponse<cmlResSclDelItem>();
                oResponse.roItem = aoType;
                oResponse.rtCode = oMsg.tMS_RespCode900;
                oResponse.rtDesc = oMsg.tMS_RespDesc900;

                return oResponse;
            }
            finally
            {
                oSql = null;
                oMsg = null;
                oFunc = null;
                aoSysConfig = null;
                oResponse = null;
                aoType = null;

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

        }


    }
}

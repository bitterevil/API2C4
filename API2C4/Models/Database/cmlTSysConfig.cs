﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.Database
{
    /// <summary>
    /// Class model config
    /// </summary>
    public class cmlTSysConfig
    {
        /// <summary>
        /// Field FTSysCode 
        /// </summary>
        public string FTSysCode { get; set; }

        /// <summary>
        /// Field FTSysSeq
        /// </summary>
        public string FTSysSeq { get; set; }

        /// <summary>
        /// Field FTSysStaUsrValue
        /// </summary>
        public string FTSysUsrValue { get; set; }

        /// <summary>
        /// Field FTSysStaUsrRef
        /// </summary>
        public string FTSysUsrRef { get; set; }

        ///// <summary>
        ///// Field FTSysKey
        ///// </summary>
        //public string FTSysKey { get; set; }
    }
}
﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.DCS
{
    public class cmlReqDelDcs
    {
        /// <summary>
        /// DCS code.
        /// </summary>
        [MaxLength(30, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptDcsCode { get; set; }
    }
}
﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.DynamicData;

namespace API2C4.Models.WebService.Request.DCS
{
    [TableName("TCNMPdtDCS")]
    public class cmlReqUptDcs
    {
        /// <summary>
        /// DCS code.
        /// </summary>
        [MaxLength(30, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptDcsCode { get; set; }

        /// <summary>
        /// DCS name.
        /// </summary>
        [MaxLength(150, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptDcsName { get; set; }

        /// <summary>
        /// Depart code.
        /// </summary>
        [MaxLength(10, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptDepCode { get; set; }

        /// <summary>
        /// Class code.
        /// </summary>
        [MaxLength(10, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptClsCode { get; set; }

        /// <summary>
        /// Sub class code.
        /// </summary>
        [MaxLength(10, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptSclCode { get; set; }

    }
}
﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Unit
{
    public class cmlReqUnitUpt
    {
        /// <summary>
        /// Unit code.
        /// </summary>
        [MaxLength(5, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPunCode { get; set; }

        /// <summary>
        /// Unit name.
        /// </summary>
        [MaxLength(50, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptPunName { get; set; }
    }
}
﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.DynamicData;

namespace API2C4.Models.WebService.Request.Scl
{

    public class cmlReqScl
    {
        /// <summary>
        /// Sub class
        /// </summary>
        [MaxLength(10, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptSclCode { get; set; }

        /// <summary>
        /// Sub class name
        /// </summary>
        [MaxLength(50, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptSclName { get; set; }
    }
}
﻿using API2C4.Class.Standard;
using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.AdjustPrice
{
    public class cmlReqPdtAdj
    {
        /// <summary>
        /// Document No.
        /// </summary>
        [MaxLength(20, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptIphDocRef { get; set; }

        /// <summary>
        /// Affective date.
        /// </summary>
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string pdIphAffect { get; set; }

        /// <summary>
        /// Adjust price type.
        /// </summary>
        public string ptIphPriType { get; set; }

        /// <summary>
        /// Finish date.
        /// </summary>
        public string pdIphDStop { get; set; }

        /// <summary>
        /// Branch code.
        /// </summary>
        [MaxLength(3, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptIphBchTo { get; set; }

        /// <summary>
        /// Zone code.
        /// </summary>
        [MaxLength(3, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptIphZneTo { get; set; }

        /// <summary>
        /// Product (List).
        /// </summary>
        [MaxLength(200, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public cmlReqPdtAdjInfo[] paoPdtAdjInsList { get; set; }





    }
}
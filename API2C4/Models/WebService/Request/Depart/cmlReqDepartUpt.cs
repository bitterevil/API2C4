﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Depart
{
    public class cmlReqDepartUpt
    {
        /// <summary>
        /// Depart code.
        /// </summary>
        [MaxLength(10, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptDepCode { get; set; }

        /// <summary>
        /// Depart name.
        /// </summary>
        [MaxLength(50, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptDepName { get; set; }
    }
}
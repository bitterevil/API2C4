﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Type
{
    public class cmlReqType
    {
        /// <summary>
        /// Type code.
        /// </summary>
        [MaxLength(5, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPtyCode { get; set; }

        /// <summary>
        /// Type name.
        /// </summary>
        [MaxLength(100, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPtyName { get; set; }
    }
}
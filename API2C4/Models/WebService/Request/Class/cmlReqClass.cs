﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Class
{
    public class cmlReqClass
    {
        /// <summary>
        /// Class code.
        /// </summary>
        [MaxLength(10, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptClsCode { get; set; }

        /// <summary>
        /// Class name.
        /// </summary>
        [MaxLength(50, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptClsName { get; set; }
    }
}
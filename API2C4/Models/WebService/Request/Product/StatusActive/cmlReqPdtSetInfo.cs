﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Product.StatusActive
{
    public class cmlReqPdtSetInfo
    {
        /// <summary>
        /// Product code.
        /// </summary>
        [MaxLength(20, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtCode { get; set; }
    }
}
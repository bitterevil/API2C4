﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Product.StatusActive
{
    public class cmlReqPdtSetActiveList
    {

        /// <summary>
        /// Status active.
        /// </summary>
        [MaxLength(1, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtStaActive { get; set; }

        /// <summary>
        /// Product List.
        /// </summary>
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public List<cmlReqPdtSetInfo> paoPdtSetList { get; set; }
    }
}
﻿using API2C4.Class.Standard;
using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Product.Image
{
    public class cmlReqPdtImgInfo
    {
        /// <summary>
        /// Product code.
        /// </summary>
        [MaxLength(20, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtCode { get; set; }

        /// <summary>
        /// Product image in type String Base64.
        /// </summary>
        //[Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtPicBase64 { get; set; }
    }
}
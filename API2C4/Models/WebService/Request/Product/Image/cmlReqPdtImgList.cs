﻿using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Product.Image
{
    public class cmlReqPdtImgList
    {
        /// <summary>
        /// Product image List.
        /// </summary>
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public cmlReqPdtImgInfo[] paoPdtImgList { get; set; }
    }
}
﻿using API2C4.Class.Standard;
using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Product.Insert
{
    /// <summary>
    /// Product information for insert.
    /// </summary>
    public class cmlReqPdtBarInfo
    {
        /// <summary>
        /// Product barcode.
        /// </summary>
        [MaxLength(25, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtBarCode { get; set; }

        /// <summary>
        /// Product Unit.
        /// </summary>
        [MaxLength(5, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtUnit { get; set; }
    }
}
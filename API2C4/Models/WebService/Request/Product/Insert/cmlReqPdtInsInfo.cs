﻿using API2C4.Class.Standard;
using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Product.Insert
{
    /// <summary>
    /// Product information for insert.
    /// </summary>
    public class cmlReqPdtInsInfo
    {
        /// <summary>
        /// Product code.
        /// </summary>
        [MaxLength(20, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtCode { get; set; }

        /// <summary>
        /// Product name.
        /// </summary>
        [MaxLength(100, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtName { get; set; }

        /// <summary>
        /// Product type.
        /// </summary>
        [MaxLength(5, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptPtyCode { get; set; }

        /// <summary>
        /// Product brand.
        /// </summary>
        [MaxLength(5, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptPbnCode { get; set; }

        /// <summary>
        /// Product DCS.
        /// </summary>
        [MaxLength(30, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptDcsCode { get; set; }

        /// <summary>
        /// Product depart.
        /// </summary>
        [MaxLength(10, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptFTDepCode { get; set; }

        /// <summary>
        /// Product class.
        /// </summary>
        [MaxLength(10, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptClsCode { get; set; }

        /// <summary>
        /// Product sub class
        /// </summary>
        [MaxLength(10, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptSclCode { get; set; }

        /// <summary>
        /// Product barcode (List).
        /// </summary>
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public List<cmlReqPdtBarInfo> paoPdtBarInsList { get; set; }
    }
}
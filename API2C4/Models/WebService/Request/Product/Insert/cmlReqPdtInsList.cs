﻿using API2C4.Class.Standard;
using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Product.Insert
{
    /// <summary>
    /// Product information for insert.
    /// </summary>
    public class cmlReqPdtInsList
    {
        /// <summary>
        /// Product code.
        /// </summary>
        [MaxLength(200, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public cmlReqPdtInsInfo[] paoPdtInsList { get; set; }
    }
}
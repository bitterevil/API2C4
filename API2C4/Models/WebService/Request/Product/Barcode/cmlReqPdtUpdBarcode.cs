﻿using API2C4.Class.Standard;
using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Request.Product.Barcode
{

    /// <summary>
    /// Product barcode information.
    /// </summary>
    public class cmlReqPdtUpdBarcode
    {
        /// <summary>
        /// Product code.
        /// </summary>
        [MaxLength(20, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtCode { get; set; }

        /// <summary>
        /// Product barcode
        /// </summary>
        [MaxLength(25, ErrorMessage = cCS.tCS_MaxLength)]
        [Required(ErrorMessage = cCS.tCS_MsgRequire)]
        public string ptPdtBarCode { get; set; }

        /// <summary>
        /// Product barcode update.
        /// </summary>
        [MaxLength(25, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptPdtBarCodeUpdate { get; set; }

        /// <summary>
        /// Product Unit.
        /// </summary>
        [MaxLength(5, ErrorMessage = cCS.tCS_MaxLength)]
        public string ptPdtUnit { get; set; }


    }
}
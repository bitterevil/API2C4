﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Brand
{
    public class cmlResBrandInsItem
    {
        /// <summary>
        /// Brand code.
        /// </summary>
        public string rtPbnCode { get; set; }

        /// <summary>
        /// Brand name.
        /// </summary>
        public string rtPbnName { get; set; }

    }
}
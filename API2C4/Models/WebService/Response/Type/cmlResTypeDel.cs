﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Type
{
    public class cmlResTypeDel
    {
        /// <summary>
        /// Type code.
        /// </summary>
        public string rtPtyCode { get; set; }
    }
}
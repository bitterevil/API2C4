﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.Insert
{
    /// <summary>
    /// Response product header.
    /// </summary>
    public class cmlResPdtInsListBarInto
    {
        /// <summary>
        /// System process status.
        /// </summary>
        public string rtCode { get; set; }

        /// <summary>
        /// System process description.
        /// </summary>
        public string rtDesc { get; set; }

        /// <summary>
        /// Response product detail.
        /// </summary>
        public List<cmlResPdtInsDT> raoPdtList { get; set; }

    }
}
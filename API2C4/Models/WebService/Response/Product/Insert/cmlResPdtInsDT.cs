﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.Insert
{
    /// <summary>
    /// Product insert response.
    /// </summary>
    public class cmlResPdtInsDT
    {
        /// <summary>
        /// Productcode
        /// </summary>
        public string rtPdtCode { get; set; }

        /// <summary>
        /// Barcode
        /// </summary>
        public string rtPdtBarCode { get; set; }
    }
}
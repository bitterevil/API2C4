﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.Insert
{

    /// <summary>
    /// สำหรับ execute ค่า Error ลงใน Model กรณี มีข้อมูลผิดพลาด
    /// </summary>
    public class cmlResPdtListInsExcute
    {
        /// <summary>
        /// Barcode
        /// </summary>
        public string rtPdtBarCode { get; set; }

        /// <summary>
        /// Product code ที่ เกิดข้อผิดพลาด
        /// </summary>
        public string rtPdtCode { get; set; }

        /// <summary>
        /// Product code & barcode dupicate.
        /// </summary>
        public string rtPdtDupicate { get; set; }

        /// <summary>
        /// Product unit not found.
        /// </summary>
        public string rtPdtLUnit { get; set; }

        /// <summary>
        /// Producttype not found
        /// </summary>
        public string rtPdtType { get; set; }

        /// <summary>
        /// Product band not found.
        /// </summary>
        public string rtPbnCode { get; set; }

        /// <summary>
        /// Product Dcs code
        /// </summary>
        public string rtPdtDcs { get; set; }

        /// <summary>
        /// Product department not found.
        /// </summary>
        public string rtPdtDepart { get; set; }

        /// <summary>
        /// Product class not found.
        /// </summary>
        public string rtPdtClass { get; set; }

        /// <summary>
        /// Product sub class not found.
        /// </summary>
        public string rtPdtSubClass { get; set; }
    }
}
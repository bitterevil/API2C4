﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.Insert
{
    /// <summary>
    /// For product list.
    /// </summary>
    public class cmlResPdtInsList
    {
        /// <summary>
        /// List for product detail
        /// </summary>
        public List<cmlResPdtInsListBarInto> raoPdtInsListBarInto { get; set; }
    }

}
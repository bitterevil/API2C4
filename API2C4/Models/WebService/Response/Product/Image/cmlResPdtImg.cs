﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.Image
{
    public class cmlResPdtImg
    {
        /// <summary>
        /// 
        /// </summary>
        public string rtCode { get; set; }

        /// <summary>
        /// System process status
        /// </summary>
        public string rtDesc { get; set; }

        /// <summary>
        /// System process description.
        /// </summary>
        public List<cmlResPdtImgList> roPdtList { get; set; }

    }
}
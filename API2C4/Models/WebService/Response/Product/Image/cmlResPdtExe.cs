﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.Image
{
    public class cmlResPdtExe
    {
        public string ptPdtCode { get; set; }
        public string ptErrorProductDup { get; set; }
        public string ptErrorDataNotfound { get; set; }
        public string ptErrorBase64 { get; set; }
        public string ptErrorSize { get; set; }
    }
}
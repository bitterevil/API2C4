﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.StatusActive
{
    public class cmlResPdtSetDetail
    {
        /// <summary>
        /// Product code.
        /// </summary>
        public string rtPdtCode { get; set; }
    }
}
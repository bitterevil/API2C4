﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.StatusActive
{
    public class cmlResPdtSetInfo
    {
        /// <summary>
        /// System process status
        /// </summary>
        public string rtCode { get; set; }

        /// <summary>
        /// System process description.
        /// </summary>
        public string rtDesc { get; set; }
        public List<cmlResPdtSetDetail> raoPdtList { get; set; }

    }
}
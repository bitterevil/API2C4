﻿using API2C4.Models.WebService.Response.Product.Update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.Barcode
{
    public class cmlResPdtUpdBarcodeItem
    {
        /// <summary>
        /// Product code.
        /// </summary>
        public string rtPdtCode { get; set; }

        /// <summary>
        /// Product barcode
        /// </summary>
        public string rtPdtBarCode { get; set; }

        /// <summary>
        /// Product barcode.
        /// </summary>
        public string rtPdtBarCodeUpdate { get; set; }

        /// <summary>
        /// Product Unit.
        /// </summary>
        public string rtPdtUnit { get; set; }
        public List<cmlResPdtUpdItemlist> aoPdtMaster { get; set; }


    }
}
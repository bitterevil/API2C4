﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Product.Update
{
    /// <summary>
    /// Response product list
    /// </summary>
    public class cmlResPdtUpdItem
    {
        /// <summary>
        /// Product code.
        /// </summary>
        public string rtPdtCode { get; set; }

        /// <summary>
        /// Product name.
        /// </summary>
        public string rtPdtName { get; set; }

        /// <summary>
        /// Product type.
        /// </summary>
        public string rtPtyCode { get; set; }

        /// <summary>
        /// Product brand.
        /// </summary>
        public string rtPbnCode { get; set; }

        /// <summary>
        /// Product DCS.
        /// </summary>
        public string rtDcsCode { get; set; }

        /// <summary>
        /// Product depart.
        /// </summary>
        public string rtFTDepCode { get; set; }

        /// <summary>
        /// Product class.
        /// </summary>
        public string rtClsCode { get; set; }

        /// <summary>
        /// Product sub class
        /// </summary>
        public string rtSclCode { get; set; }
    }
}
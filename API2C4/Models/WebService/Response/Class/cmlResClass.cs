﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Class
{
    public class cmlResClass
    {
        /// <summary>
        /// Class code.
        /// </summary>
        public string rtClsCode { get; set; }

        /// <summary>
        /// Class name.
        /// </summary>
        public string rtClsName { get; set; }
    }
}
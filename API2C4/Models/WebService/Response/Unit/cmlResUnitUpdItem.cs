﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Unit
{
    public class cmlResUnitUpdItem
    {
        /// <summary>
        /// Unit code.
        /// </summary>
        public string rtPunCode { get; set; }

        /// <summary>
        /// Unit name.
        /// </summary>
        public string rtPunName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Depart
{
    public class cmlResDepartInsItem
    {
        /// <summary>
        /// Depart code.
        /// </summary>
        public string rtDepCode { get; set; }

        /// <summary>
        /// Depart name.
        /// </summary>
        public string rtDepName { get; set; }
    }
}
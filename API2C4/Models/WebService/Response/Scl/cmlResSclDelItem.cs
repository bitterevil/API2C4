﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Scl
{
    public class cmlResSclDelItem
    {
        /// <summary>
        /// Sub class code.
        /// </summary>
        public string rtSclCode { get; set; }
    }
}
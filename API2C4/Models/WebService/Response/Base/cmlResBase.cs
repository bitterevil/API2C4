﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Base
{
    public class cmlResBase
    {
        public string rtCode { get; set; }
        public string rtDesc { get; set; }
    }
}
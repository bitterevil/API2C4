﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.Base
{
    public class cmlResponseList<T>
    {
        public List<T> roItem;
        public List<cmlResBase> raUpate { get; set; }
    }
}
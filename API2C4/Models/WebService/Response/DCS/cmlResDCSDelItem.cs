﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.DCS
{
    public class cmlResDCSDelItem
    {
        /// <summary>
        /// DCS code.
        /// </summary>
        public string rtDcsCode { get; set; }
    }
}
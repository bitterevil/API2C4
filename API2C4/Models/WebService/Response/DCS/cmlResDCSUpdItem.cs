﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.DCS
{
    public class cmlResDCSUpdItem
    {
        /// <summary>
        /// DCS code.
        /// </summary>
        public string rtDcsCode { get; set; }

        /// <summary>
        /// DCS name.
        /// </summary>
        public string rtDcsName { get; set; }

        /// <summary>
        /// Depart code.
        /// </summary>
        public string rtDepCode { get; set; }

        /// <summary>
        /// Class code.
        /// </summary>
        public string rtClsCode { get; set; }

        /// <summary>
        /// Sub class code.
        /// </summary>
        public string rtSclCode { get; set; }
    }
}
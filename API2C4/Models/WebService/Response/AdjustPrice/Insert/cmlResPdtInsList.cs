﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.AdjustPrice.Insert
{
    public class cmlResPdtInsList
    {
        /// <summary>
        /// Product code.
        /// </summary>
        public string rtPdtCode { get; set; }

        /// <summary>
        /// Product barcode.
        /// </summary>
        public string rtIpdBarCode { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.AdjustPrice.Insert
{
    public class cmlResPdtAdjInsIntoExe
    {
        public string ptPdtCode { get; set; }
        public string ptPdtBarCode { get; set; }
        public string ptIphDocRefRmkE { get; set; }
        public string ptErrorBchCode { get; set; }
        public string ptErrorZneCode { get; set; }
        public string ptErrorBarcodeDup { get; set; }
        public string ptErrorProductDup { get; set; }
        public string ptErrorPdtUnit { get; set; }
        public string ptErrorData { get; set; }
        public string ptPdtNotFound { get; set; }
    }
}
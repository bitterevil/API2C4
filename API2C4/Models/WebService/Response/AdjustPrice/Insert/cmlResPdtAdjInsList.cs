﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2C4.Models.WebService.Response.AdjustPrice.Insert
{
    public class cmlResPdtAdjInsList
    {
        /// <summary>
        /// Document No.
        /// </summary>
        public string rtIphDocRef { get; set; }

        /// <summary>
        /// Affective date.
        /// </summary>
        public string rdIphAffect { get; set; }

        /// <summary>
        /// Branch code.
        /// </summary>
        public string rtIphBchTo { get; set; }

        /// <summary>
        /// Zone code.
        /// </summary>
        public string rtIphZneTo { get; set; }

        /// <summary>
        /// Product (List).
        /// </summary>
        public List<cmlResPdtAdjInsInto> raoPdtAdjList { get; set; }

    }
}
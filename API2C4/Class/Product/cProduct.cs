﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API2C4.Models.WebService.Request.Product.Insert;
using API2C4.Models.Database;
using API2C4.Models.WebService.Response.Product.Insert;
using API2C4.Class.Standard;
using System.Text;
using System.Data;
using System.Reflection;
using API2C4.Models.WebService.Request.Product.Update;
using API2C4.Models.WebService.Request.Product.Image;
using API2C4.EF;
using System.Data.Common;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;
using System.Data.Entity.Core;
using API2PosV4Master.Class.Standard;

namespace API2C4.Class.Product
{
    /// <summary>
    /// Manage Product List
    /// </summary>
    public class cProduct
    {

        /// <summary>
        /// Check Product ที่รับเข้ามา
        /// </summary>
        /// <param name="poInfo"></param>
        /// <param name="poBarcode"></param>
        /// <returns></returns>
        public bool C_DATbChkProduct(cmlReqPdtInsInfo poInfo, cmlReqPdtBarInfo poBarcode)
        {
            cSP oFunc;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            DataTable oDbTbl;

            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oDatabase = new cDatabase();
                oSql = new StringBuilder();
                C_DATbGenTableProduct("TCNTPdtTemp"); //สร้าง Table Temp

                // Insert ค่าลง Temp

                if (C_DATbInsTemp(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode) == true)
                {

                    //เงื่อนไข : Check StkCode กับ Barcode ว่ามีซ้ำ กับสินค้าอื่นหรือไม่ และ Update ลง Table Temp
                    oSql.AppendLine(" SELECT FTPdtStkCode,FTPdtCode,FTPdtBarCode1, FTPdtBarCode2, FTPdtBarCode3");
                    oSql.AppendLine(" FROM TCNMPdt");
                    oSql.AppendLine(" WHERE FTPdtStkCode = '" + poInfo.ptPdtCode + "' ");
                    oSql.AppendLine(" AND  (FTPdtBarCode1 = '" + poBarcode.ptPdtBarCode + "' OR ");
                    oSql.AppendLine(" FTPdtBarCode2 = '" + poBarcode.ptPdtBarCode + "' OR ");
                    oSql.AppendLine(" FTPdtBarCode3 = '" + poBarcode.ptPdtBarCode + "')");
                    oDbTbl = oDatabase.C_DAToSqlQueryDt(oSql.ToString());

                    if (oDbTbl != null && oDbTbl.Rows.Count > 0)
                    {
                        C_DATbUpdProductTemp(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "702");
                    }

                    //เงื่อนไข : เช็คค่าทื่ส่งเข้ามาว่ามีใน Master หรือไม่และ Update ลง TableTemp

                    if (poInfo.ptPtyCode != null && poInfo.ptPtyCode != "")
                    {
                        if (C_DATbChkMaster("PdtType", poInfo.ptPtyCode) == false)
                        {
                            C_DATbUpdProductTemp(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "704");
                        }
                        else
                        {
                            C_DATbUpdProductTempMaster(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "ProductType", poInfo.ptPtyCode);
                        }

                    }

                    if (poInfo.ptPbnCode != null && poInfo.ptPbnCode != "")
                    {
                        if (C_DATbChkMaster("PdtBrand", poInfo.ptPbnCode) == false)
                        {
                            C_DATbUpdProductTemp(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "705");
                        }
                        else
                        {
                            C_DATbUpdProductTempMaster(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "Brand", poInfo.ptPbnCode);
                        }
                    }

                     if (poInfo.ptDcsCode != null && poInfo.ptDcsCode != "")
                    {
                        if (C_DATbChkMaster("DcsCode", poInfo.ptDcsCode) == false)
                        {
                            C_DATbUpdProductTemp(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "706");
                        }
                        else
                        {
                            C_DATbUpdProductTempMaster(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "Dcs", poInfo.ptDcsCode);
                        }
                    }

                     if (poInfo.ptFTDepCode != null && poInfo.ptFTDepCode != "")
                    {
                        if (C_DATbChkMaster("DepCode", poInfo.ptFTDepCode) == false)
                        {
                            C_DATbUpdProductTemp(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "707");
                        }
                        else
                        {
                            C_DATbUpdProductTempMaster(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "Department", poInfo.ptFTDepCode);
                        }
                    }

                     if (poInfo.ptClsCode != null && poInfo.ptClsCode != "")
                    {
                        if (C_DATbChkMaster("ClsCode", poInfo.ptClsCode) == false)
                        {
                            C_DATbUpdProductTemp(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "708");
                        }
                        else
                        {
                            C_DATbUpdProductTempMaster(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "Class", poInfo.ptClsCode);
                        }
                    }

                     if (poInfo.ptSclCode != null && poInfo.ptSclCode != "")
                    {
                        if (C_DATbChkMaster("SubClass", poInfo.ptSclCode) == false)
                        {
                            C_DATbUpdProductTemp(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "709");
                        }
                        else
                        {
                            C_DATbUpdProductTempMaster(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "Subclass", poInfo.ptSclCode);
                        }
                    }

                    //เช็ค Unit ใน Barcode
                    if (poBarcode.ptPdtUnit != null && poBarcode.ptPdtUnit != "")
                    {
                        if (C_DATbChkMaster("Unit", poBarcode.ptPdtUnit) == false)
                        {
                            C_DATbUpdProductTemp(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "703");
                        }
                        else
                        {
                            C_DATbUpdProductTempMaster(poInfo.ptPdtCode, poInfo.ptPdtName, poBarcode.ptPdtBarCode, "Unit", poBarcode.ptPdtUnit);
                        }
                    }

                    oDatabase.C_DATxConnectiosClose();
                    return true;
                }


                return false;

            }
         catch(Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
               
            }
        }

        /// <summary>
        /// Update Error ลงตาราง Temp
        /// </summary>
        /// <param name="ptPdtCode"> ProductCode</param>
        /// <param name="ptPdtName"> ProductName</param>
        /// <param name="ptBarcode"> BarCode</param>
        /// <param name="ptError"> Case Error</param>
        public void C_DATbUpdProductTemp(string ptPdtCode,string ptPdtName, string ptBarcode,string ptError)
        {
            cDatabase oDatabase;
            StringBuilder oSqlErr;

            try
            {
                    oDatabase = new cDatabase();
                    oSqlErr = new StringBuilder();
                    oSqlErr.AppendLine(" UPDATE TCNTPdtTemp ");
                    oSqlErr.AppendLine(" SET ");

                    switch(ptError)
                    {
                        case "702":
                            oSqlErr.AppendLine(" FTErrorPdtDupicate = N'702'");
                            oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                            oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                            break;
                        case "703":
                            oSqlErr.AppendLine(" FTErrorPdtLUnit = N'703'");
                            oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                            oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                            break;
                        case "704":
                            oSqlErr.AppendLine(" FTErrorPdtType = N'704'");
                            oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                            oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                            break;
                        case "705":
                            oSqlErr.AppendLine(" FTErrorPbnCode = N'705'");
                            oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                            oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                            break;
                        case "706":
                            oSqlErr.AppendLine(" FTErrorDcsCode = N'706'");
                            oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                            oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                            break;
                        case "707":
                            oSqlErr.AppendLine(" FTErrorDepCode = N'707'");
                            oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                            oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                            break;
                        case "708":
                            oSqlErr.AppendLine(" FTErrorClsCode = N'708'");
                            oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                            oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                            break;
                        case "709":
                            oSqlErr.AppendLine(" FTErrorSclCode = N'709'");
                            oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                            oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                            break;
                }
                oDatabase.C_DATnExecuteSql(oSqlErr.ToString());
                oDatabase.C_DATxConnectiosClose();

            }
            catch(Exception oExn)
            {
                throw oExn;
            }
            finally
            {

                oDatabase = null;
                oSqlErr = null;
            }
        }

        /// <summary>
        /// Update master field on Temp
        /// </summary>
        /// <param name="ptPdtCode"> Product code</param>
        /// <param name="ptPdtName"> Product Name</param>
        /// <param name="ptBarcode"> Barcode</param>
        /// <param name="ptStatus"> Type master </param>
        /// <param name="ptValue"> Value master </param>
        public void C_DATbUpdProductTempMaster(string ptPdtCode, string ptPdtName, string ptBarcode, string ptStatus,string ptValue)
        {
            cDatabase oDatabase;
            StringBuilder oSqlErr;

            try
            {
                oDatabase = new cDatabase();
                oSqlErr = new StringBuilder();
                oSqlErr.AppendLine(" UPDATE TCNTPdtTemp ");
                oSqlErr.AppendLine(" SET ");

                switch (ptStatus)
                {
                    case "Unit":
                        oSqlErr.AppendLine(" FTPdtLUnit = '" + ptValue  + "'");
                        oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                        oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                        break;
                    case "ProductType":
                        oSqlErr.AppendLine(" FTPdtType = '" + ptValue + "'");
                        oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                        oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                        break;
                    case "Brand":
                        oSqlErr.AppendLine(" FTPbnCode = '" + ptValue + "'");
                        oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                        oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                        break;
                    case "Dcs":
                        oSqlErr.AppendLine(" FTDcsCode = '" + ptValue + "'");
                        oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                        oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                        break;
                    case "Department":
                        oSqlErr.AppendLine(" FTDepCode = '" + ptValue + "'");
                        oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                        oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                        break;
                    case "Class":
                        oSqlErr.AppendLine(" FTClsCode = '" + ptValue + "'");
                        oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                        oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                        break;
                    case "Subclass":
                        oSqlErr.AppendLine(" FTSclCode = '" + ptValue + "'");
                        oSqlErr.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "' ");
                        oSqlErr.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                        break;
                }
                oDatabase.C_DATnExecuteSql(oSqlErr.ToString());
                oDatabase.C_DATxConnectiosClose();
            }
            catch (Exception oExn)
            {
                throw oExn;
            }
            finally
            {
                oDatabase = null;
                oSqlErr = null;
            }
        }

        /// <summary>
        /// Insert ข้อมูลหลักลง Table Temp
        /// </summary>
        /// <param name="ptPdtCode">Product Code</param>
        /// <param name="ptPdtName">Product Name</param>
        /// <param name="ptBarcode">Barcode </param>
        public bool C_DATbInsTemp(string ptPdtCode, string ptPdtName, string ptBarcode)
        {
            
            cDatabase oDatabase;
            StringBuilder oSqlErr;
            StringBuilder oSqlChk;
            DataTable oDbtb;
            int nSuccess;

            try
            {
                oDatabase = new cDatabase();
                oSqlChk = new StringBuilder();
                oSqlChk.AppendLine(" SELECT FTPdtCode,FTPdtBarCode");
                oSqlChk.AppendLine(" FROM TCNTPdtTemp");
                oSqlChk.AppendLine(" WHERE FTPdtCode ='" + ptPdtCode + "' AND FTPdtBarCode = '" + ptBarcode + "' ");
                oDbtb = oDatabase.C_DAToSqlQueryDt(oSqlChk.ToString());
                if(oDbtb.Rows.Count == 0)
                {
                    oSqlErr = new StringBuilder();
                    oSqlErr.AppendLine(" INSERT INTO ");
                    oSqlErr.AppendLine(" TCNTPdtTemp ");
                    oSqlErr.AppendLine(" (FTPdtCode,FTPdtName,FTPdtBarCode)");
                    oSqlErr.AppendLine(" VALUES");
                    oSqlErr.AppendLine(" (");
                    oSqlErr.AppendLine(" '" + ptPdtCode + "','" + ptPdtName + "','" + ptBarcode + "')");
                    nSuccess = oDatabase.C_DATnExecuteSql(oSqlErr.ToString());
                    oDatabase.C_DATxConnectiosClose();
                    return true;
                }

                oDatabase.C_DATxConnectiosClose();
                return false;
            }
            catch(Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oSqlChk = null;
                oSqlErr = null;
                oDbtb = null;
                oDatabase = null;
            }
        }

        /// <summary>
        /// สร้าง Table Temp
        /// </summary>
        /// <param name="TCNTPdtTemp"></param>
        public void C_DATbGenTableProduct(string TCNTPdtTemp)
        {
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSqlTmp;

            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();

                //สร้าง Table Temp ของรายการที่ Error
                oSqlTmp = new StringBuilder();
                oSqlTmp.AppendLine(" IF NOT EXISTS (SELECT * FROM sys.objects");
                oSqlTmp.AppendLine(" WHERE object_id = OBJECT_ID('" + TCNTPdtTemp + "') AND type in (N'U'))");
                oSqlTmp.AppendLine(" BEGIN");
                oSqlTmp.AppendLine(" CREATE TABLE " + TCNTPdtTemp + "");
                oSqlTmp.AppendLine(" (FTPdtCode          VARCHAR(20), ");
                oSqlTmp.AppendLine(" FTPdtName           VARCHAR(100), ");
                oSqlTmp.AppendLine(" FTPdtBarCode        VARCHAR(25),");
                oSqlTmp.AppendLine(" FTPdtType           VARCHAR(5),");
                oSqlTmp.AppendLine(" FTPbnCode           VARCHAR(5),");
                oSqlTmp.AppendLine(" FTDcsCode           VARCHAR(30),");
                oSqlTmp.AppendLine(" FTDepCode           VARCHAR(10),");
                oSqlTmp.AppendLine(" FTClsCode           VARCHAR(10),");
                oSqlTmp.AppendLine(" FTSclCode           VARCHAR(10),");
                oSqlTmp.AppendLine(" FTPdtLUnit          VARCHAR(5),");
                oSqlTmp.AppendLine(" FTErrorPdtDupicate  VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorPdtType      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorPbnCode      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorDcsCode      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorDepCode      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorClsCode      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorSclCode      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorPdtLUnit     VARCHAR(3)");
                oSqlTmp.AppendLine(" )");
                oSqlTmp.AppendLine(" END ");
                oDatabase.C_DATnExecuteSql(oSqlTmp.ToString());
                oDatabase.C_DATxConnectiosClose();
            }
            catch (Exception oExn)
            {

            }
            finally
            {
                oSqlTmp = null;
                oDatabase = null;   
            }
        }

        /// <summary>
        /// Check Product Master. 
        /// </summary>
        /// <param name="ptTypeCheck">ประเภทค่าที่ Check</param>
        /// <param name="ptValue"> ค่าที่นำมาตรวจสอบ Check</param>
        /// <returns></returns>
        public bool C_DATbChkMaster(string ptTypeCheck,string ptValue)
        {
            cSP oFunc;
            StringBuilder oSql;
            cDatabase oDatabase;
            DataTable oDbTbl;
            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();

                oSql = new StringBuilder();

                switch(ptTypeCheck)
                {
                    case "Unit":
                        oSql.AppendLine(" SELECT FTPunCode, FTPunName");
                        oSql.AppendLine(" FROM TCNMPdtUnit");
                        oSql.AppendLine(" WHERE FTPunCode = '" + ptValue +"' ");
                        break;
                    case "PdtType":
                        oSql.AppendLine(" SELECT FTPtyCode, FTPtyName ");
                        oSql.AppendLine(" FROM TCNMPdtType");
                        oSql.AppendLine(" WHERE FTPtyCode = '" + ptValue + "' ");
                        break;

                    case "PdtBrand":
                        oSql = new StringBuilder();
                        oSql.AppendLine(" SELECT FTPbnCode, FTPbnName");
                        oSql.AppendLine(" FROM TCNMPdtBrand");
                        oSql.AppendLine(" WHERE FTPbnCode = '" + ptValue + "' ");
                        break;

                    case "DcsCode":
                        oSql.AppendLine(" SELECT FTDcsCode, FTDcsName");
                        oSql.AppendLine(" FROM TCNMPdtDCS");
                        oSql.AppendLine(" WHERE FTDcsCode = '" + ptValue + "' ");
                        break;

                    case "DepCode":
                        oSql.AppendLine(" SELECT FTDepCode, FTDepName");
                        oSql.AppendLine(" FROM TCNMPdtDepart");
                        oSql.AppendLine(" WHERE FTDepCode = '" + ptValue + "' ");
                        break;
                    case "ClsCode":
                        oSql.AppendLine(" SELECT  FTClsCode, FTClsName");
                        oSql.AppendLine(" FROM TCNMPdtClass");
                        oSql.AppendLine(" WHERE FTClsCode = '" + ptValue + "' ");
                        break;

                    case "SubClass":
                        oSql.AppendLine(" SELECT FTSclCode, FTSclName");
                        oSql.AppendLine(" FROM TCNMPdtSubClass");
                        oSql.AppendLine(" WHERE FTSclCode = '" + ptValue + "' ");
                        break;
                    case "Branch":
                        oSql.AppendLine(" SELECT  FTBchCode, FTBchName");
                        oSql.AppendLine(" FROM TCNMBranch");
                        oSql.AppendLine(" WHERE FTBchCode = '" + ptValue +"'");
                        break;
                    case "Zone":
                        oSql.AppendLine(" SELECT  FTZneCode, FTZneID ");
                        oSql.AppendLine(" FROM TCNMZone");
                        oSql.AppendLine(" WHERE FTZneCode = '" + ptValue + "'");
                        break;
                }
             
                oDbTbl = oDatabase.C_DAToSqlQueryDt(oSql.ToString());
                if (oDbTbl != null && oDbTbl.Rows.Count > 0)
                {
                    oDatabase.C_DATxConnectiosClose();
                    return true;
                }

                oDatabase.C_DATxConnectiosClose();
                return false;
            }
            catch (Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oDbTbl = null;
                oSql = null;
                oFunc = null;
                oDatabase = null;
            }
        }
        
        /// <summary>
        /// Check Product ก่อน Insert 
        /// </summary>
        /// <param name="ptStkCode"></param>
        /// <param name="ptBarcode"></param>
        /// <returns></returns>
        public bool C_DATbInsProduct(string ptStkCode,string ptBarcode)
        {
            cSP oFunc;
            StringBuilder oSqlChk;
            StringBuilder oSqlIns;
            StringBuilder oSqlUpt;
            StringBuilder oSqlDel;
            StringBuilder oSqlPdt;
            cDatabase oDatabase;
            DataTable oDbTbl;
            DataTable oDbPdt;

            try
            {
                // Check ตาราง Temp ว่ามี ข้อมูลที่ ตรวจสอบแล้ว ไม่มี Error 
                oDatabase = new cDatabase();
                oSqlChk = new StringBuilder();
                oSqlChk.AppendLine(" SELECT ");
                oSqlChk.AppendLine(" FTPdtCode, FTPdtName, FTPdtBarCode,FTPdtType, ");
                oSqlChk.AppendLine(" FTPbnCode, FTDcsCode, FTDepCode, FTClsCode, FTSclCode,FTPdtLUnit ");
                oSqlChk.AppendLine(" FROM TCNTPdtTemp");
                oSqlChk.AppendLine(" WHERE  ");
                oSqlChk.AppendLine(" (FTPdtCode =	'" + ptStkCode +"') AND (FTPdtBarCode = '" + ptBarcode +"')");
                oSqlChk.AppendLine(" AND (FTErrorPbnCode IS NULL) AND  ");
                oSqlChk.AppendLine(" (FTErrorPdtDupicate IS NULL) AND (FTErrorPdtType IS NULL) AND  ");
                oSqlChk.AppendLine(" (FTErrorPbnCode IS NULL) AND (FTErrorDcsCode IS NULL) AND (FTErrorDepCode IS NULL) AND ");
                oSqlChk.AppendLine(" (FTErrorClsCode IS NULL) AND (FTErrorSclCode IS NULL) AND (FTErrorPdtLUnit IS NULL) ");

                oDbTbl = oDatabase.C_DAToSqlQueryDt(oSqlChk.ToString());
                if (oDbTbl != null && oDbTbl.Rows.Count > 0)
                {
                    //Check ว่ามี Product อยู่แล้วหรือไม่ และ Barcode มีที่ว่าง
                    oSqlPdt = new StringBuilder();
                    oSqlPdt.AppendLine(" SELECT FTPdtBarCode1, FTPdtBarCode2, FTPdtBarCode3, FTPdtStkCode");
                    oSqlPdt.AppendLine(" FROM TCNMPdt");
                    oSqlPdt.AppendLine(" WHERE (FTPdtStkCode = '" + ptStkCode +"') AND ");
                    oSqlPdt.AppendLine(" (FTPdtBarCode1 IS NULL OR FTPdtBarCode2 IS NULL OR FTPdtBarCode3 IS NULL) ");
                    oDbPdt = oDatabase.C_DAToSqlQueryDt(oSqlPdt.ToString());

                    oDbPdt = oDatabase.C_DAToSqlQueryDt(oSqlPdt.ToString());
                    if (oDbPdt != null && oDbPdt.Rows.Count > 0)
                    {
                        oSqlUpt = new StringBuilder();
                        oSqlUpt.AppendLine(" UPDATE TCNMPdt WITH(ROWLOCK) ");
                        oSqlUpt.AppendLine(" SET ");
                        
                       if(oDbPdt.Rows[0]["FTPdtBarCode1"].ToString() == "" || oDbPdt.Rows[0]["FTPdtBarCode1"].ToString() == null)
                        {
                            oSqlUpt.AppendLine(" FTPdtBarCode1  = '" + ptBarcode + "' ");
                            oSqlUpt.AppendLine(" , FTPdtSUnit   = '" + oDbTbl.Rows[0]["FTPdtLUnit"].ToString() + "'");
                            oSqlUpt.AppendLine(" , FCPdtSFactor = '1'  ");
                            oSqlUpt.AppendLine(" WHERE (FTPdtBarCode1 IS NULL) ");

                        }
                       else if(oDbPdt.Rows[0]["FTPdtBarCode2"].ToString() == "" || oDbPdt.Rows[0]["FTPdtBarCode2"].ToString() == null)
                        {
                            oSqlUpt.AppendLine(" FTPdtBarCode2  = '" + ptBarcode + "' ");
                            oSqlUpt.AppendLine(" , FTPdtMUnit   = '" + oDbTbl.Rows[0]["FTPdtLUnit"].ToString() + "' ");
                            oSqlUpt.AppendLine(" , FCPdtMFactor = '1' ");
                            oSqlUpt.AppendLine(" WHERE (FTPdtBarCode2 IS NULL) ");
                        }
                        else if (oDbPdt.Rows[0]["FTPdtBarCode3"].ToString() == "" || oDbPdt.Rows[0]["FTPdtBarCode3"].ToString() == null)
                        {
                            oSqlUpt.AppendLine(" FTPdtBarCode3  = '" + ptBarcode + "' ");
                            oSqlUpt.AppendLine(" , FTPdtLUnit   = '" + oDbTbl.Rows[0]["FTPdtLUnit"].ToString() + "' ");
                            oSqlUpt.AppendLine(" , FCPdtLFactor ='1' ");
                            oSqlUpt.AppendLine(" WHERE (FTPdtBarCode3 IS NULL) ");
                        }

                        oSqlUpt.AppendLine(" AND (FTPdtStkCode = '" + ptStkCode + "') ");
                        oDatabase.C_DATnExecuteSql(oSqlUpt.ToString());

                    }

                    else
                    {
                        // Insert Product ที่ Success.
                        oSqlIns = new StringBuilder();
                        oSqlIns.AppendLine("INSERT INTO TCNMPdt WITH(ROWLOCK)");
                        oSqlIns.AppendLine("(");
                        oSqlIns.AppendLine("	FTPdtCode, FTPdtName,");
                        oSqlIns.AppendLine("	FTPdtBarCode1, FTPdtStkCode, FCPdtStkFac,");
                        oSqlIns.AppendLine("	FTPdtStkControl, FCPdtQtyRet, FCPdtQtyWhs,");
                        oSqlIns.AppendLine("	FTPgpChain, FTPdtVatType, FTPdtSaleType,");
                        oSqlIns.AppendLine("	FTPdtSUnit, FCPdtSFactor, FTPdtPoint,");
                        oSqlIns.AppendLine("	FCPdtPointTime, FTPdtConType, FTPdtSrn,");
                        oSqlIns.AppendLine("	FTPdtAlwOrderS, FTPdtAlwOrderM, FTPdtAlwOrderL,");
                        oSqlIns.AppendLine("	FTPdtAlwBuyS, FTPdtAlwBuyM, FTPdtAlwBuyL,");
                        oSqlIns.AppendLine("	FCPdtRetPriS1,FCPdtRetPriS2,FCPdtRetPriS3, FTPdtWhsDefUnit, FTPdtPic,");
                        oSqlIns.AppendLine("	FTPdtStaSet, FTPdtStaSetPri,");
                        oSqlIns.AppendLine("	FTPdtStaActive, FTPdtTax, FTPdtGrpControl,");
                        oSqlIns.AppendLine("	FTPdtNoDis, FCPdtVatRate, FTPdtStaAlwBuy,");
                        oSqlIns.AppendLine("	FDPdtOrdStart, FDPdtOrdStop, FDPdtSaleStart,");
                        oSqlIns.AppendLine("	FDPdtSaleStop, FTPdtStaTouch, FTPdtStaAlwRepack,");
                        oSqlIns.AppendLine("    FTPtyCode,FTPbnCode,FTDcsCode,FTDepCode,FTClsCode,");
                        oSqlIns.AppendLine("    FTSclCode,");
                        oSqlIns.AppendLine("	FDDateUpd, FTTimeUpd, FTWhoUpd,");
                        oSqlIns.AppendLine("	FDDateIns, FTTimeIns, FTWhoIns");
                        oSqlIns.AppendLine(")");
                        oSqlIns.AppendLine("VALUES");
                        oSqlIns.AppendLine(" (");
                        oSqlIns.AppendLine("	'" + C_DATtGenPdtCode() + "',");
                        oSqlIns.AppendLine("    '" + oDbTbl.Rows[0]["FTPdtName"].ToString() + "' ,");
                        oSqlIns.AppendLine("	'" + ptBarcode +"', '" + ptStkCode +"' , 1,");
                        oSqlIns.AppendLine("	'1', 0, 0,");
                        oSqlIns.AppendLine("	NULL, '1', '1',");
                        oSqlIns.AppendLine("	'" + oDbTbl.Rows[0]["FTPdtLUnit"].ToString() + "', '1', '1',");
                        oSqlIns.AppendLine("	0, '1', '2',");
                        oSqlIns.AppendLine("	'1', '1', '1',");
                        oSqlIns.AppendLine("	'1', '1', '1',");
                        oSqlIns.AppendLine("	0,0,0, '1', '',");
                        oSqlIns.AppendLine("	'1','1',");
                        oSqlIns.AppendLine("	'1', '1', '2',");
                        //oSqlIns.AppendLine("	'2', pcPdtVatRate, '1',");
                        oSqlIns.AppendLine("	'2',  (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp), '1',");
                        oSqlIns.AppendLine("	CONVERT(VARCHAR(10), GETDATE(), 121), '9999-12-31', CONVERT(VARCHAR(10), GETDATE(), 121),");
                        oSqlIns.AppendLine("	'9999-12-31', '2', '1',");
                        oSqlIns.AppendLine("    '" + oDbTbl.Rows[0]["FTPdtType"].ToString() +"',");
                        oSqlIns.AppendLine("    '" + oDbTbl.Rows[0]["FTPbnCode"].ToString() +"',");
                        oSqlIns.AppendLine("    '" + oDbTbl.Rows[0]["FTDcsCode"].ToString() + "',");
                        oSqlIns.AppendLine("    '" + oDbTbl.Rows[0]["FTDepCode"].ToString() + "',");
                        oSqlIns.AppendLine("    '" + oDbTbl.Rows[0]["FTClsCode"].ToString() +"',");
                        oSqlIns.AppendLine("    '" + oDbTbl.Rows[0]["FTSclCode"].ToString() +"',");
                        oSqlIns.AppendLine("	CONVERT(VARCHAR(10), GETDATE(), 121), CONVERT(VARCHAR(8),GETDATE(),114), 'AdaLink',");
                        oSqlIns.AppendLine("   CONVERT(VARCHAR(10), GETDATE(), 121), CONVERT(VARCHAR(8),GETDATE(),114), 'AdaLink'");
                        oSqlIns.AppendLine(" )"); 
                        oDatabase.C_DATnExecuteSql(oSqlIns.ToString());
                    }

                    oSqlDel = new StringBuilder();
                    oSqlDel.AppendLine(" DELETE ");
                    oSqlDel.AppendLine(" FROM TCNTPdtTemp ");
                    oSqlDel.AppendLine(" WHERE FTPdtCode = '" + ptStkCode + "' ");
                    oSqlDel.AppendLine(" AND FTPdtBarCode = '" + ptBarcode + "' ");
                    oDatabase.C_DATnExecuteSql(oSqlDel.ToString());
                }

                oDatabase.C_DATxConnectiosClose();
                return true;
            }
            catch(Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Gen เลข Productcode ตาม Format code
        /// </summary>
        /// <returns></returns>
        public string C_DATtGenPdtCode()
        {
            cDatabase oDatabase;
            StringBuilder oSqlPdt;
            DataTable oDbtbl;
            string tPdtCode;

            try
            {
               
                oDatabase = new cDatabase();
                //Check Product ล่าสุด
                oSqlPdt = new StringBuilder();
                oSqlPdt.AppendLine(" SELECT TOP(1) FTPdtCode");
                oSqlPdt.AppendLine(" FROM TCNMPdt");
                oSqlPdt.AppendLine(" WHERE FTPdtCode LIKE 'IC%' ");
                oSqlPdt.AppendLine(" ORDER BY FTPdtCode DESC");
                oDbtbl = oDatabase.C_DAToSqlQueryDt(oSqlPdt.ToString());
                if(oDbtbl != null && oDbtbl.Rows.Count > 0)
                {
                    string[] tAry = oDbtbl.Rows[0]["FTPdtCode"].ToString().Split('-');
                    int nRunning = Int32.Parse(tAry[1]) + 1;
                    string tFormat =  "00000" + nRunning;
                    string tFormat2 = tFormat.ToString().Substring(tFormat.ToString().Length -6);
                    tPdtCode = "IC-" + tFormat2;

                }
                else
                {
                    //ดึง Format จาก Ada
                    tPdtCode = "IC-000001";
                }

                oDatabase.C_DATxConnectiosClose();
                return tPdtCode;
            }
            catch(Exception oExn)
            {
                return "";
                throw oExn;
            }
            finally
            {
                oSqlPdt = null;
                oDbtbl = null;
            }
        }

        /// <summary>
        ///     Save image from encoded string base64.
        /// </summary>
        /// 
        /// <param name="ptImgName">Image name.</param>
        /// <param name="ptPathSave">Path to save image.</param>
        /// <param name="ptImgEncStr">Encoded string base64.</param>
        /// <param name="ptFullImg">out Full path image.</param>
        /// <param name="pbDelOld">Status delete old image.</param>
        /// 
        /// <returns>
        ///     true : Save complete.<br/>
        ///     false : Save false.
        /// </returns>
        public string C_IMGbSaveStr2Img(string ptImgName, string ptPathSave, string ptImgEncStr, string ptFullImg, bool pbDelOld = false)
        {
            Image oImage;
            MemoryStream oMemStream;
            byte[] anBuffer;

            try
            {
                if (!string.IsNullOrEmpty(ptImgEncStr))
                {
                    if (ptImgEncStr.Split(',').Count() == 2)
                    {
                        ptImgEncStr = ptImgEncStr.Split(',')[1];
                    }

                    anBuffer = Convert.FromBase64String(ptImgEncStr);
                    using (oMemStream = new MemoryStream(anBuffer, 0, anBuffer.Length))
                    {
                        oImage = Image.FromStream(oMemStream, true);
                        
                        if(anBuffer.Length <= 204800)
                        {
                            if (!ptPathSave.EndsWith("\\"))
                            {
                                ptPathSave += "\\";
                            }

                            ptFullImg = ptPathSave + ptImgName + ".jpg";

                            if (pbDelOld)
                            {
                                if (File.Exists(ptFullImg))
                                {
                                    File.Delete(ptFullImg);
                                }
                            }

                            oImage.Save(ptFullImg, System.Drawing.Imaging.ImageFormat.Jpeg);
                            oMemStream.Dispose();
                            oImage.Dispose();
                        }
                        else
                        {
                            return "Sizeimagenotfound";
                        }
                    }
                }
                return "Success";
            }
            catch (Exception oExn)
            {
                return "Fail";
                throw oExn;
            }
            finally
            {
                oImage = null;
                oMemStream = null;
                anBuffer = null;
                ptFullImg = "";

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
         
        }

        /// <summary>
        /// Get Path Image
        /// </summary>
        /// <param name="ptObjImg"> Folder Image</param>
        /// <param name="ptImgName">Image Name</param>
        /// <param name="ptPath"> Path </param>
        /// <returns></returns>
        public string C_IMGtGetPath(string ptPath,string ptImgName)
        {
            string tImgSave = "";
            DirectoryInfo tPath = new DirectoryInfo(ptPath);
            FileInfo[] oGetFile = tPath.GetFiles();

            foreach (FileInfo oFile in oGetFile)
            {
                string tImg = ptImgName + ".jpg";

                var oImg = from Img in oGetFile
                           where Img.Name == tImg
                           select Img;

                if (oImg.FirstOrDefault() != null)
                {
                    tImgSave = ptPath + oImg.FirstOrDefault();
                }
            }

            return tImgSave;
        }

        /// <summary>
        ///     Delete image.
        /// </summary>
        /// 
        /// <param name="ptPathImg">Path image.</param>
        /// 
        /// <returns>
        ///     true : Delete success.<br/>
        ///     false : Delete false.
        /// </returns>
        public bool C_IMGbDeleteImage(string ptPathImg)
        {
            try
            { 
                if (File.Exists(ptPathImg))
                {
                    File.Delete(ptPathImg);
                }
              
            }
            catch (Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {

            }
            return true;

        }

        /// <summary>
        /// Create table temp for image
        /// </summary>
        public void C_DATtGenTableTmpImg()
        {
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSqlTmpSucc;
            StringBuilder oSqlTmp;

            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();

                //สร้าง Table Temp ของรายการที่ Error
                oSqlTmp = new StringBuilder();
                oSqlTmp.AppendLine(" CREATE TABLE TCNTImgErrorTmp");
                oSqlTmp.AppendLine(" (FTPdtCode          VARCHAR(20), ");
                oSqlTmp.AppendLine(" FTErrorProductDup  VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorDataNotfound  VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorBase64      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorSize      VARCHAR(3)");
                oSqlTmp.AppendLine(" )");

                oDatabase.C_DATnExecuteSql(oSqlTmp.ToString());
                oDatabase.C_DATxConnectiosClose();
            }
            catch (Exception oExn)
            {
            }
            finally
            {
                oSqlTmpSucc = null;
                oSqlTmp = null;
                oDatabase = null;
            }
        }

        /// <summary>
        /// Insert temp date to tempImage
        /// </summary>
        /// <param name="ptPdtCode"></param>
        /// <param name="ptError"></param>
        /// <returns></returns>
        public bool C_DATbInsTableTmpImg(string ptPdtCode,string ptError)
        {
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSqlTmp;
            string tFieldError = "";
            string tValue = "";

            switch(ptError)
            {
                case "707":
                    tFieldError += "FTErrorBase64";
                    tValue += "707";
                    break;
                case "706":
                    tFieldError += "FTErrorSize";
                    tValue += "706";
                    break;
                case "709":
                    tFieldError += "FTErrorProductDup";
                    tValue += "709";
                    break;
                case "800":
                    tFieldError += "FTErrorDataNotfound";
                    tValue += "800";
                    break;
               
            }

            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();
                
                //สร้าง Table Temp ของรายการที่ Error
                oSqlTmp = new StringBuilder();
                oSqlTmp.AppendLine(" INSERT INTO TCNTImgErrorTmp");
                oSqlTmp.AppendLine(" (FTPdtCode, " + tFieldError +") ");
                oSqlTmp.AppendLine(" VALUES('" + ptPdtCode + "','" + tValue + "')");

                oDatabase.C_DATnExecuteSql(oSqlTmp.ToString());
                oDatabase.C_DATxConnectiosClose();
                return true;
            }
            catch (Exception oExn)
            {
                return false;
            }
            finally
            {
                oSqlTmp = null;
                oDatabase = null;
            }
        }

        /// <summary>
        /// Delete picture on table.
        /// </summary>
        /// <param name="ptPdtCode"></param>
        /// <returns></returns>
        public bool C_DATbDelImageonTable(string ptPdtCode)
        {
            StringBuilder oSql;
            cDatabase oDatabase;

            try
            {
                oSql = new StringBuilder();
                oDatabase = new cDatabase();
                oSql = new StringBuilder();
                oSql.AppendLine(" UPDATE ");
                oSql.AppendLine(" TCNMPdt ");
                oSql.AppendLine(" SET FTPdtPic = N'' ");
                oSql.AppendLine(" WHERE FTPdtStkCode = '" + ptPdtCode + "' ");

                int nSuccess = oDatabase.C_DATnExecuteSql(oSql.ToString());
                oDatabase.C_DATxConnectiosClose();
                if (nSuccess != 0)
                {
                    return true;
                }
                return false;
            }
            catch(Exception oExn)
            {
                return false;
            }
            finally
            {

            }

        }
    }
}
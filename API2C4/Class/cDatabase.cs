﻿using System;
using System.Collections.Generic;
using System.Linq;
using API2C4.EF;
using API2C4.Class.Standard;
using System.Data.Entity.Core;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Data.Common;
using API2PosV4Master.Class.Standard;
using API2C4.Models.Database;

namespace API2C4.Class
{
    /// <summary>
    /// Database Connect
    /// </summary>

    public class cDatabase
    {
        AdaBUFCEntities oC_AdaC4;

        /// <summary>
        /// Contructor Function Connection open.
        /// </summary>
        /// <param name="pnConTme">Connection timeout.</param>
        public cDatabase(int pnConTme = cCS.nCS_ConTme)
        {
            EntityConnectionStringBuilder oEntConStr;
            SqlConnectionStringBuilder oSqlConStr;
            string tConstr;

            try
            {
                tConstr = ConfigurationManager.ConnectionStrings["AdaBUFCEntities"].ConnectionString;
                oEntConStr = new EntityConnectionStringBuilder(tConstr);
                oSqlConStr = new SqlConnectionStringBuilder(oEntConStr.ProviderConnectionString);
                oSqlConStr.ConnectTimeout = pnConTme;
                oEntConStr.ProviderConnectionString = oSqlConStr.ConnectionString;

                oC_AdaC4 = new AdaBUFCEntities(oEntConStr.ConnectionString);
                oC_AdaC4.Database.Connection.Open();

            }
            catch(SqlException oSqlExp)
            {
                throw oSqlExp;
            }
            catch(EntityException oEttExp)
            {
                throw oEttExp;
            }
            catch(Exception oExt)
            {
                throw oExt;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Close connections
        /// </summary>
        public void C_DATxConnectiosClose()
        {
            oC_AdaC4.Database.Connection.Close();
        }
       

        /// <summary>
        /// Database Sql Query List
        /// </summary>
        /// <typeparam name="T"> Model List </typeparam>
        /// <param name="ptSqlCmd">  Sql Query </param>
        /// <param name="pnCmdTme"> Command Timeout</param>
        /// <returns></returns>
        public List<T> C_DATaSqlQuery<T>(string ptSqlCmd, int pnCmdTme = cCS.nCS_CmdTme)
        {
            try
            {
                List<T> aoResult = new List<T>();
                oC_AdaC4.Database.CommandTimeout = pnCmdTme;
                aoResult = oC_AdaC4.Database.SqlQuery<T>(ptSqlCmd).ToList();

                return aoResult;
            }
            catch (SqlException oSqlExp)
            {
                throw oSqlExp;
            }
            catch (EntityException oEntExp)
            {
                throw oEntExp;
            }
            catch (Exception oExp)
            {
                throw oExp;
            }
        }

        public T C_DAToSqlQuery<T>(string ptSqlCmd, int pnCmdTme = cCS.nCS_CmdTme)
        {
            try
            {
                T oResult = default(T);
                oC_AdaC4.Database.CommandTimeout = pnCmdTme;
                oResult = oC_AdaC4.Database.SqlQuery<T>(ptSqlCmd).FirstOrDefault();

                //T oResult = (T)Activator.CreateInstance(typeof(T)); //*[ANUBIS][][2018-05-03] - ใช้ default(T) แทน เพราะใช้ได้ทั้ง string, int, model class.
                return oResult;
            }
            catch (SqlException oSqlEct)
            {
                throw oSqlEct;
            }
            catch (EntityException oEtyExn)
            {
                throw oEtyExn;
            }
            catch (Exception oExn)
            {
                throw oExn;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Execute query
        /// </summary>
        /// <param name="ptSqlCmd"></param>
        /// <param name="pnCmdTme"></param>
        /// <returns></returns>
        public int C_DATnExecuteSql(string ptSqlCmd, int pnCmdTme = cCS.nCS_CmdTme)
        {
            int nRowEff = 0;

            try
            {
                oC_AdaC4.Database.CommandTimeout = pnCmdTme;
                nRowEff = oC_AdaC4.Database.ExecuteSqlCommand(ptSqlCmd);
            }
            catch (SqlException oSqlExn)
            {
                throw oSqlExn;
            }
            catch (EntityException oEtyExn)
            {
                throw oEtyExn;
            }
            catch (Exception oExn)
            {
                throw oExn;
            }
            finally
            {

            }

            return nRowEff;
        }

        /// <summary>
        /// ExecuteDataTable
        /// </summary>
        /// <param name="ptSqlCmd"></param>
        /// <param name="pnCmdTme"></param>
        /// <param name="ptTblName"></param>
        /// <returns></returns>
        public DataTable C_DAToSqlQueryDt(string ptSqlCmd, int pnCmdTme = cCS.nCS_CmdTme/*, string ptTblName*/)
        {
            DataTable oDbTblResult;

            try
            {
                DbProviderFactory oDbFactory = DbProviderFactories.GetFactory(oC_AdaC4.Database.Connection);
                using (DbCommand oDbCmd = oDbFactory.CreateCommand())
                {
                    oDbCmd.Connection = oC_AdaC4.Database.Connection;
                    oDbCmd.CommandType = CommandType.Text;
                    oDbCmd.CommandText = ptSqlCmd;
                    using (DbDataAdapter oDbAdp = oDbFactory.CreateDataAdapter())
                    {
                        oDbAdp.SelectCommand = oDbCmd;

                        oDbTblResult = new DataTable();
                        //oDbTblResult.TableName = ptTblName;
                        oDbAdp.Fill(oDbTblResult);
                    }
                }
            }
            catch (SqlException oSqlEct)
            {
                throw oSqlEct;
            }
            catch (EntityException oEtyExn)
            {
                throw oEtyExn;
            }
            catch (Exception oExn)
            {
                throw oExn;
            }
            finally
            {

            }

            return oDbTblResult;
        }


        /// <summary>
        ///     Get configuration from memory.
        /// </summary>
        /// 
        /// <typeparam name="T">Type data user value.</typeparam>
        /// <typeparam name="E">Type data user default.</typeparam>
        /// <param name="poUsrValue">ref Data user value.</param>
        /// <param name="poDefValue">Default data user value.</param>
        /// <param name="poUsrRef">ref Data user referent.</param>
        /// <param name="poDefRef">Default data user referent.</param>
        /// <param name="paoSysConfig">Valiable TSysConfig in memory.</param>
        /// <param name="ptSysSeq">Sequence.</param>
        public void SP_DATxGetConfigurationFromMem<T, E>(ref T poUsrValue, T poDefValue, ref E poUsrRef, E poDefRef,
            List<cmlTSysConfig> paoSysConfig, string ptSysSeq)
        {
            try
            {
                // UsrValue.
                try
                {
                    poUsrValue = (T)Convert.ChangeType(paoSysConfig.Where(
                        oItem => string.Equals(oItem.FTSysSeq, ptSysSeq)).Select(oItem => oItem.FTSysUsrValue).FirstOrDefault(), typeof(T));
                }
                catch (Exception)
                {
                    poUsrValue = poDefValue;
                }

                // UsrRef.
                try
                {
                    poUsrRef = (E)Convert.ChangeType(paoSysConfig.Where(
                        oItem => string.Equals(oItem.FTSysSeq, ptSysSeq)).Select(oItem => oItem.FTSysUsrRef).FirstOrDefault(), typeof(E));
                }
                catch (Exception)
                {
                    poUsrRef = poDefRef;
                }
            }
            catch (Exception)
            {

            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http.ModelBinding;
using API2C4.Models.Database;
using API2C4.EF;
using System.Data.Common;
using System.Web.DynamicData;
using System.Reflection;
using System.Data.Entity.Core;
using System.Drawing;
using System.IO;
using System.Data;

namespace API2C4.Class.Standard
{
    /// <summary>
    /// All Function
    /// </summary>
    /// 

    public class cSP
    {
        AdaBUFCEntities oC_AdaC4;

        /// <summary>
        /// Check Database Connected
        /// </summary>
        /// <returns></returns>
        public string SP_CHKtReturnStatusDbConnect()
        {
            try
            {
                using (AdaBUFCEntities oDb = new AdaBUFCEntities())
                {
                    using (DbConnection oConn = oDb.Database.Connection)
                    {
                        oConn.Open();
                        return "True";
                    }
                }
            }
            catch (Exception oExn)
            {
                return "False";
            }
        }

        /// <summary>
        /// Get config value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="poUsrValue"></param>
        /// <param name="poDefValue"></param>
        /// <param name="paoSysConfig"></param>
        /// <param name="ptSysSeq"></param>
        public void SP_DATxGetConfigFromMem<T>(ref T poUsrValue, T poDefValue, List<cmlTSysConfig> paoSysConfig, string ptSysSeq)
        {
            try
            {
                poUsrValue = (T)Convert.ChangeType(paoSysConfig.Where(
                    oItem => string.Equals(oItem.FTSysSeq, ptSysSeq)).Select(oItem => oItem.FTSysUsrValue).FirstOrDefault(), typeof(T));
            }
            catch (Exception oExn)
            {
                poUsrValue = poDefValue;
            }
        }

        /// <summary>
        /// Load config
        /// </summary>
        /// <returns></returns>
        public List<cmlTSysConfig> SP_SYSaLoadConfig()
        {
            cDatabase oDatabase;
            List<cmlTSysConfig> aoSysConfig;
            StringBuilder oSql;

            try
            {
                oSql = new StringBuilder();
                oSql.AppendLine(" SELECT FTSysCode, FTSysSeq,FTSysUsrValue,FTSysUsrRef");
                oSql.AppendLine(" FROM TSysConfig WITH(NOLOCK) ");
                oSql.AppendLine(" WHERE FTSysCode='AdaC4' ");
                oSql.AppendLine(" ORDER BY FTSysSeq");
                oDatabase = new cDatabase();
                aoSysConfig = oDatabase.C_DATaSqlQuery<cmlTSysConfig>(oSql.ToString());

                return aoSysConfig;
            }
            catch (Exception oExn)
            {
                throw oExn;
            }
        }

        /// <summary>
        /// Check Parameter Model false
        /// </summary>
        /// <param name="ptModelErr"></param>
        /// <param name="poModelState"></param>
        /// <returns></returns>
        public bool SP_CHKbPrmModel(ref string ptModelErr, ModelStateDictionary poModelState)
        {
            try
            {
                if (poModelState.IsValid)
                {
                    return true;
                }
                else
                {
                    IEnumerable<string> atErrList = from oState in poModelState.Values
                                                    from oError in oState.Errors
                                                    where !string.IsNullOrEmpty(oError.ErrorMessage)
                                                    select oError.ErrorMessage;

                    ptModelErr = string.Join("|", atErrList);

                    return false;
                }
            }
            catch (Exception oExn)
            {
                throw oExn;
            }
        }


        /// <summary>
        /// CheckParameterEmpty
        /// </summary>
        /// <param name="ptModelErr"></param>
        /// <param name="poModelState"></param>
        /// <returns></returns>
        //public bool SP_CHKbPrmModelEmpty(ref string ptModelErr, ModelStateDictionary poModelState)
        //{
        //    try
        //    {
        //        if (poModelState.IsValid)
        //        {
        //            return true;
        //        }
        //        else
        //        {
        //            IEnumerable<string> atErrList = from oState in poModelState.Values
        //                                            from oError in oState.Errors
        //                                            where !string.isn(oError.ErrorMessage)
        //                                            select oError.ErrorMessage;
        //            ptModelErr = string.Join("|", atErrList);

        //            return false;
        //        }
        //    }
        //    catch (Exception oExn)
        //    {
        //        throw oExn;
        //    }
        //}

        /// <summary>
        /// Check Key API
        /// </summary>
        /// <param name="ptKeyAPI"> Parameter for key</param>
        /// <param name="ptFuncName"> Function name</param>
        /// <param name="poHttpContext"> Key header</param>
        /// <param name="paoSysConfig"> Model Config</param>
        /// <returns></returns>
        public bool SP_CHKbKeyAPI(ref string ptKeyAPI, string ptFuncName, HttpContext poHttpContext, List<cmlTSysConfig> paoSysConfig)
        {
            NameValueCollection oReqHeader;
            string tXKeyApi, tKeyApi;

            try
            {
                oReqHeader = poHttpContext.Request.Headers;
                tXKeyApi = oReqHeader.Get("X-Api-Key");

                tKeyApi = "";
                SP_DATxGetConfigFromMem<string>(ref tKeyApi, "", paoSysConfig, "001");
                if (tXKeyApi == tKeyApi)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (SqlException oSqlExn)
            {
                throw oSqlExn;
            }
            catch (Exception oExn)
            {
                throw oExn;
            }
        }

        /// <summary>
        /// Generate query update
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="poModel"></param>
        /// <returns></returns>
        public string SP_SQLtGeneralCmdUpdate<T>(T poModel)
        {
            Type oType;
            StringBuilder oSqlUpd, oSql;
            TableNameAttribute oTblNameAtb;
            string tSqlUpd, tSqlFmt, tValue;
            try
            {
                oTblNameAtb = (TableNameAttribute)poModel.GetType().GetCustomAttributes(false).FirstOrDefault();
                oSql = new StringBuilder();
                oSql.AppendLine(" UPDATE");
                oSql.AppendLine(" " + oTblNameAtb.Name + "");
                oSql.AppendLine(" WITH(ROWLOCK)");
                oSql.AppendLine(" SET");

                tSqlFmt = " {0} = '{1}', ";
                oType = poModel.GetType();

                oSqlUpd = new StringBuilder();
                foreach (PropertyInfo oProperty in oType.GetProperties())
                {
                    string tName = oProperty.Name.Substring(0, 2);
                    string tField = oProperty.Name.Substring(2);
                    
                    switch (oProperty.Name.Substring(0, 2))
                    {
                        case "pn":
                            tField = "FN" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            break;
                        case "pc":
                            tField = "FC" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            break;
                        case "pd":
                            tField = "FD" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            if (!string.Equals(tValue, "null"))
                            {
                                tValue = DateTime.Parse(tValue).ToString("yyyy-MM-dd HH:mm:ss:fff");
                            }
                            break;
                        case "pt":
                            tField = "FT" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            break;
                        default:
                            tValue = "";
                            break;
                    }

                    if (!string.IsNullOrEmpty(tValue) && !string.Equals(tValue, "null"))
                    {
                        oSqlUpd.AppendLine(string.Format(tSqlFmt, tField, tValue));
                    }
                }

                tSqlUpd = oSql.ToString() + oSqlUpd.ToString().Remove(oSqlUpd.Length - 4);

                return tSqlUpd;
            }
            catch (Exception oExn)
            {
                return null;
            }
            finally
            {

            }
        }

        public string SP_SQLtGeneralCmdUpdate<T>(T poModel,string ptClass)
        {
            Type oType;
            StringBuilder oSqlUpd, oSql;
            TableNameAttribute oTblNameAtb;
            string tSqlUpd, tSqlFmt, tValue;
            try
            {
                oTblNameAtb = (TableNameAttribute)poModel.GetType().GetCustomAttributes(false).FirstOrDefault();
                oSql = new StringBuilder();
                oSql.AppendLine(" UPDATE");
                oSql.AppendLine(" " + oTblNameAtb.Name + "");
                oSql.AppendLine(" WITH(ROWLOCK)");
                oSql.AppendLine(" SET");

                tSqlFmt = " {0} = '{1}', ";
                oType = poModel.GetType();

                oSqlUpd = new StringBuilder();
                foreach (PropertyInfo oProperty in oType.GetProperties())
                {
                    if(ptClass == "cProduct" && oProperty.Name == "ptPdtCode")
                    {
                        continue;
                    }

                    string tName = oProperty.Name.Substring(0, 2);
                    string tField = oProperty.Name.Substring(2);

                    switch (oProperty.Name.Substring(0, 2))
                    {
                        case "pn":
                            tField = "FN" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            break;
                        case "pc":
                            tField = "FC" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            break;
                        case "pd":
                            tField = "FD" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            if (!string.Equals(tValue, "null"))
                            {
                                tValue = DateTime.Parse(tValue).ToString("yyyy-MM-dd HH:mm:ss:fff");
                            }
                            break;
                        case "pt":
                            tField = "FT" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            break;
                        default:
                            tValue = "";
                            break;
                    }

                    // ถ้า tValue <> "" หรือ null ไม่ต้อง add Query
                    if (!string.IsNullOrEmpty(tValue) && !string.Equals(tValue, "null"))
                    {
                        oSqlUpd.AppendLine(string.Format(tSqlFmt, tField, tValue));
                    }
                }

                tSqlUpd = oSql.ToString() + oSqlUpd.ToString().Remove(oSqlUpd.Length - 4);

                return tSqlUpd;
            }
            catch (Exception oExn)
            {
                return null;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Setting Time Allow to use 
        /// </summary>
        /// <param name="paoSysConfig">Configuration.</param>
        /// <returns>
        ///   true : Allow to use.<br/>
        ///     false : Not allow to use.
        /// </returns>
        public bool SP_CHKbAllowRangeTime(List<cmlTSysConfig> paoSysConfig)
        {
            cDatabase oDatabase;
            TimeSpan oTmeStart, oTmeEnd, oTmeNow;
            string tTmeStart, tTmeEnd, tTmeNow;

            try
            {
                oDatabase = new cDatabase();
                oTmeNow = DateTime.Now.TimeOfDay;
                tTmeNow = oTmeNow.ToString("hh\\:mm");
                tTmeStart = "";
                tTmeEnd = "";

                oDatabase.SP_DATxGetConfigurationFromMem<string, string>(ref tTmeStart, tTmeNow, ref tTmeEnd, tTmeNow, paoSysConfig, "002");

                oTmeStart = TimeSpan.Parse(tTmeStart);
                oTmeEnd = TimeSpan.Parse(tTmeEnd);

                // Check time use function.
                if ((oTmeNow >= oTmeStart) && (oTmeNow <= oTmeEnd))
                {
                    return true;
                }
            }
            catch (Exception oEx)
            {

            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }

            return false;
        }

        /// <summary>
        /// Generate Query insert 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="poModel"></param>
        /// <returns></returns>
        public string SP_SQLtGeneralCmdInsert<T>(T poModel)
        {
            Type oType;
            StringBuilder oSql, oSqlField, oSqlValue;
            TableNameAttribute oTblNameAtb;
            string tSql, tValue, tFormat;


            try
            {
                tFormat = "'{0}'";
                oSql = new StringBuilder();
                oSqlField = new StringBuilder();
                oSqlValue = new StringBuilder();

                oType = poModel.GetType();
                oTblNameAtb = (TableNameAttribute)poModel.GetType().GetCustomAttributes(false).FirstOrDefault();

                oSql.AppendLine(" INSERT INTO");
                oSql.AppendLine("" + oTblNameAtb.Name + "");
                oSql.AppendLine(" WITH(ROWLOCK) ");

                foreach (PropertyInfo oProperty in oType.GetProperties())
                {
                    string tName = oProperty.Name.Substring(0, 2);
                    string tField = oProperty.Name.Substring(2);
                    tValue = null;
                    switch (oProperty.Name.Substring(0, 2))
                    {
                        case "pn":
                            tField = "FN" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            break;
                        case "pc":
                            tField = "FC" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            break;
                        case "pd":
                            tField = "FD" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString() ?? "null";
                            if (!string.Equals(tValue, "null"))
                            {
                                tValue = DateTime.Parse(tValue).ToString("yyyy-MM-dd HH:mm:ss:fff");
                            }
                            break;
                        case "pt":
                            tField = "FT" + tField;
                            tValue = oProperty.GetValue(poModel)?.ToString();
                            break;
                        default:
                            tValue = "";
                            break;
                    }

                    if (!string.IsNullOrEmpty(tValue) && !string.Equals(tValue, "null"))
                    {
                        oSqlField.AppendLine(tField + ",");
                        oSqlValue.AppendLine(string.Format(tFormat, tValue) + ",");
                    }
                }

                oSql.AppendLine("(");
                oSql.AppendLine(oSqlField.ToString()).ToString().Replace("'null'", "null");
                oSql.AppendLine(" FDDateUpd, FTTimeUpd, FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns)");
                oSql.AppendLine("VALUES");
                oSql.AppendLine("(");
                oSql.AppendLine(oSqlValue.ToString()).ToString().Replace("'null'", "null");
                oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114),'AdaLink' ");
                oSql.AppendLine(",CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),114),'AdaLink'");
                oSql.AppendLine(")");

                tSql = oSql.ToString();
                return tSql;
            }
            catch (Exception oExn)
            {
                return "";
            }
            finally
            {

            }
        }




    }
}
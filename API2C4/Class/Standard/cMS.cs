﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2PosV4Master.Class.Standard
{
    public class cMS
    {
        // Response code.
        public readonly string tMS_RespCode1 = "1";
        public readonly string tMS_RespCode700 = "700";
        public readonly string tMS_RespCode701 = "701";
        public readonly string tMS_RespCode702 = "702";
        public readonly string tMS_RespCode703 = "703";
        public readonly string tMS_RespCode704 = "704";
        public readonly string tMS_RespCode705 = "705";
        public readonly string tMS_RespCode706 = "706";
        public readonly string tMS_RespCode707 = "707";
        public readonly string tMS_RespCode708 = "708";
        public readonly string tMS_RespCode709 = "709";
        public readonly string tMS_RespCode710 = "710";
        public readonly string tMS_RespCode711 = "711";
        public readonly string tMS_RespCode800 = "800";
        public readonly string tMS_RespCode801 = "801";
        public readonly string tMS_RespCode802 = "802";
        public readonly string tMS_RespCode813 = "813";
        public readonly string tMS_RespCode900 = "900";
        public readonly string tMS_RespCode904 = "904";
        public readonly string tMS_RespCode905 = "905";
        public readonly string tMS_RespCode906 = "906";

        // Response descript.
        public readonly string tMS_RespDesc1 = "success.";
        public readonly string tMS_RespDesc700 = "all parameter is null.";
        public readonly string tMS_RespDesc701 = "validate parameter model false.|";
        public readonly string tMS_RespDesc710 = "barcode parameter duplicate in yourself.";
        public readonly string tMS_RespDesc711 = "vat rate not allow less than 0.";
        public readonly string tMS_RespDesc800 = "data not found.";
        public readonly string tMS_RespDesc801 = "data is duplicate.";
        public readonly string tMS_RespDesc802 = "formate data incorrect.";
        public readonly string tMS_RespDesc813 = "data is referent in another data.";
        public readonly string tMS_RespDesc900 = "service process false.";
        public readonly string tMS_RespDesc904 = "key not allowed to use method.";
        public readonly string tMS_RespDesc905 = "cannot connect database.";
        public readonly string tMS_RespDesc906 = "this time not allowed to use method.";

        //For Product
        public readonly string tMS_RespDesc702 = "barcode duplicate.";
        public readonly string tMS_RespDesc703 = "product unit not found in unit master.";
        public readonly string tMS_RespDesc704 = "product type not found in type master.";
        public readonly string tMS_RespDesc705 = "product brand not found in brand master.";
        public readonly string tMS_RespDesc706 = "product DCS not found in DCS master.";
        public readonly string tMS_RespDesc707 = "product depart not found in depart master.";
        public readonly string tMS_RespDesc708 = "product class not found in class master.";
        public readonly string tMS_RespDesc709 = "product sub class not found in sub class master.";


        //for Img
        public readonly string tMS_RespDesc706Img = "image size not found.";
        public readonly string tMS_RespDesc707Img = "cannot convert base64 to image.";
        public readonly string tMS_RespDesc709Img = "product code parameter duplicate in yourself.";


        //for adjustprice
        public readonly string tMS_RespCode704Adjust = "704";
        public readonly string tMS_RespCode705Adjust = "705";
        public readonly string tMS_RespCode706Adjust = "706";
        public readonly string tMS_RespCode707Adjust = "707";
        public readonly string tMS_RespCode708Adjust = "708";
        public readonly string tMS_RespCode709Adjust = "709";
        public readonly string tMS_RespCode710Adjust = "710";

        public readonly string tMS_RespDesc704Adjust = "branch code not found in branch code master.";
        public readonly string tMS_RespDesc705Adjust = "zone code not found in zone code master.";
        public readonly string tMS_RespDesc706Adjust = "product and barcode duplicate in this document.";
        public readonly string tMS_RespDesc707Adjust = "product code duplicate in this document.";
        public readonly string tMS_RespDesc708Adjust = "product unit not found in unit master.";
        public readonly string tMS_RespDesc709Adjust = "Document Number duplicate.";
        public readonly string tMS_RespDesc710Adjust = "product not found.";


        //for PdtUpdate
        public readonly string tMS_RespDesc706PdtUpdate = "product type not found in type master.";
        public readonly string tMS_RespDesc707PdtUpdate = "product brand not found in brand master.";
        public readonly string tMS_RespDesc708PdtUpdate = "product DCS not found in DCS master.";
        public readonly string tMS_RespDesc709PdtUpdate = "product depart not found in depart master.";
        public readonly string tMS_RespDesc710PdtUpdate = "product class not found in class master.";
        public readonly string tMS_RespDesc711PdtUpdate = "product sub class not found in sub class master.";

        //for PdtUpdate barcode
        public readonly string tMS_RespDesc704PdtUpdateBar = "product unit not found in unit master.";

        //for Product Unit
        public readonly string tMS_RespDesc702PdtUnit = "unit code duplicate.";
        public readonly string tMS_RespDesc813PdtUnit = "data is referent in another data.";

        //for Product Type
        public readonly string tMS_RespDesc702PdtType = "product type code duplicate.";

        //for Product Brand
        public readonly string tMS_RespDesc702PdtPbn = "product brand duplicate.";

        //for Product SubClass
        public readonly string tMS_RespDesc702SclPdt = "product sub class duplicate.";

        //for Product Class
        public readonly string tMS_RespDesc702ClsPdt = "product class duplicate.";

        //for Product DCS
        public readonly string tMS_RespDesc706DCSPdt = "product depart not found in depart master.";
        public readonly string tMS_RespDesc707DCSPdt = "product class not found in class master.";
        public readonly string tMS_RespDesc708DCSPdt = " product sub class not found in sub class master.";
        public readonly string tMS_RespDesc702DCSPdt = "DCS code duplicate.";

        //for Department 
        public readonly string tMS_RespDesc702DepPdt = "product depart duplicate.";



    }
}
﻿using API2C4.Class.Product;
using API2C4.Class.Standard;
using API2C4.Models.WebService.Request.AdjustPrice;
using API2PosV4Master.Class.Standard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace API2C4.Class.AdjustPrice
{
    public class cAdjustPrice
    {
        public string C_DATtGenDocRefCode(string ptBchCode)
        {
            cDatabase oDatabase;
            StringBuilder oSqlPdt;
            DataTable oDbtbl;
            string tDate = Convert.ToDateTime(DateTime.Now).ToString("yyyy", new System.Globalization.CultureInfo("en-US"));
            string tPdtCode;

            try
            {
                oDatabase = new cDatabase();

                //Check Product ล่าสุด
                oSqlPdt = new StringBuilder();
                oSqlPdt.AppendLine(" SELECT TOP(1) FTIphDocNo");
                oSqlPdt.AppendLine(" FROM TCNTPdtAjpHD");
                oSqlPdt.AppendLine(" WHERE FTIphDocNo LIKE 'IP"+ ptBchCode + "%' ");
                oSqlPdt.AppendLine(" ORDER BY FTIphDocNo DESC");
                oDbtbl = oDatabase.C_DAToSqlQueryDt(oSqlPdt.ToString());
                if (oDbtbl != null && oDbtbl.Rows.Count > 0)
                {
                    string[] tAry = oDbtbl.Rows[0]["FTIphDocNo"].ToString().Split('-');
                    int nRunning = Int32.Parse(tAry[1]) + 1;
                    string tFormatStart = "00000" + nRunning;
                    string tFmtBch = ptBchCode + tDate.ToString().Substring(tDate.ToString().Length - 2);
                    string tFormatRunning = tFormatStart.ToString().Substring(tFormatStart.ToString().Length - 6);
                    tPdtCode = "IP" + tFmtBch + "-" + tFormatRunning;
                }
                else
                {
                    //ดึง Format จาก Ada
                    string tFormatStart = "000001";
                    string tFmtBch = ptBchCode + tDate.ToString().Substring(tDate.ToString().Length - 2);
                    string tFormatRunning = tFormatStart.ToString().Substring(tFormatStart.ToString().Length - 6);

                    tPdtCode = "IP" + tFmtBch + "-" + tFormatRunning;

                }
                oDatabase.C_DATxConnectiosClose();
                return tPdtCode;
            }
            catch (Exception oExn)
            {
                return "";
            }
            finally
            {
                oSqlPdt = null;
                oDbtbl = null;
            }
        }

        public void C_DATxGenTableTemp()
        {
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSqlTmp;

            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();

                //สร้าง Table Temp ของรายการที่ Error
                oSqlTmp = new StringBuilder();
                oSqlTmp.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects");
                oSqlTmp.AppendLine("WHERE object_id = OBJECT_ID(N'TCNTPdtAjpTmp') AND type in (N'U'))");
                oSqlTmp.AppendLine("BEGIN");
                oSqlTmp.AppendLine(" CREATE TABLE  TCNTPdtAjpTmp ");
                oSqlTmp.AppendLine(" (FTIphDocRef        VARCHAR(25), ");
                oSqlTmp.AppendLine(" FTIphDocRefRmk      VARCHAR(25), ");
                oSqlTmp.AppendLine(" FDIphAffect         datetime, ");
                oSqlTmp.AppendLine(" FTIphPriType        VARCHAR(1),");
                oSqlTmp.AppendLine(" FDIphDStop          datetime,");
                oSqlTmp.AppendLine(" FTIphBchTo          VARCHAR(3),");
                oSqlTmp.AppendLine(" FTZneTo             VARCHAR(3),");
                oSqlTmp.AppendLine(" FTPdtCode           VARCHAR(20), ");
                oSqlTmp.AppendLine(" FTPdtBarCode        VARCHAR(25),");
                oSqlTmp.AppendLine(" FTPdtName           VARCHAR(100),");
                oSqlTmp.AppendLine(" FTtPunCode          VARCHAR(5),");
                oSqlTmp.AppendLine(" FTZneName           VARCHAR(100),");
                oSqlTmp.AppendLine(" FTUnitName          VARCHAR(100), ");
                oSqlTmp.AppendLine(" FTIpdPriNew          float,");
                oSqlTmp.AppendLine(" FTErrorBchCode      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorZneCode      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorBarcodeDup   VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorProductDup      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorPdtUnit      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorData      VARCHAR(3),");
                oSqlTmp.AppendLine(" FTErrorPdtNotFound VARCHAR(3)");
                oSqlTmp.AppendLine(" )");
                oSqlTmp.AppendLine("END");

                oDatabase.C_DATnExecuteSql(oSqlTmp.ToString());
                oDatabase.C_DATxConnectiosClose();
            }
            catch (Exception oExn)
            {

            }
            finally
            {
                oSqlTmp = null;
                oDatabase = null;
            }
        }
        
        public bool C_DATxInsTemp(cmlReqPdtAdj poAjp, cmlReqPdtAdjInfo poAjpInfo)
        {
            
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSqlTmp;
            StringBuilder oSqlChk;
            DataTable oDbtbl;
            DataTable oDbtblChk;
            string tPritype, tIphDStop, tPdtCode;
            
            try
            {
                oDatabase = new cDatabase();
                oFunc = new cSP();

                oSqlChk = new StringBuilder();
                oSqlChk.AppendLine(" SELECT FTIphRmk ");
                oSqlChk.AppendLine(" FROM  TCNTPdtAjpHD");
                oSqlChk.AppendLine(" WHERE FTIphRmk = '" + poAjp.ptIphDocRef + "' ");
                oDbtblChk = new DataTable();
                oDbtblChk = oDatabase.C_DAToSqlQueryDt(oSqlChk.ToString());
                oDatabase.C_DATxConnectiosClose();
                if (oDbtblChk != null && oDbtblChk.Rows.Count > 0 )
                {
                    oDatabase.C_DATxConnectiosClose();
                    return false;
                }

                //check Pritype
                tPritype = "";
                tIphDStop = "";
                if (poAjp.ptIphPriType == null)
                {
                    tPritype = "1";
                }
                else
                {
                    tPritype = poAjp.ptIphPriType;
                }

                //Check Datestop
                if (poAjp.pdIphDStop == null)
                {
                    tIphDStop = "9999-12-31 00:00:00.000";
                }
                else
                {
                    tIphDStop = poAjp.pdIphDStop;
                }
                
                
                oSqlTmp = new StringBuilder();
                oSqlTmp.AppendLine(" INSERT INTO  TCNTPdtAjpTmp  WITH(ROWLOCK) ");
                oSqlTmp.AppendLine(" (FTIphDocRef, ");
                oSqlTmp.AppendLine(" FTIphDocRefRmk, ");
                oSqlTmp.AppendLine(" FDIphAffect, ");
                oSqlTmp.AppendLine(" FTIphPriType,");
                oSqlTmp.AppendLine(" FDIphDStop,");
                oSqlTmp.AppendLine(" FTIphBchTo,");
                oSqlTmp.AppendLine(" FTZneTo,");
                oSqlTmp.AppendLine(" FTPdtCode, ");
                oSqlTmp.AppendLine(" FTPdtBarCode,");
                oSqlTmp.AppendLine(" FTPdtName,");
                oSqlTmp.AppendLine(" FTtPunCode,");
                oSqlTmp.AppendLine(" FTIpdPriNew");
                oSqlTmp.AppendLine(" )");
                oSqlTmp.AppendLine("VALUES");
                oSqlTmp.AppendLine("( '" + C_DATtGenDocRefCode(poAjp.ptIphBchTo) + "',");
                oSqlTmp.AppendLine("   '" + poAjp.ptIphDocRef +"',");
                oSqlTmp.AppendLine("   '" + poAjp.pdIphAffect +"',");
                oSqlTmp.AppendLine("   '" + tPritype + "',");
                oSqlTmp.AppendLine("   '" + tIphDStop + "',");
                oSqlTmp.AppendLine("   '" + poAjp.ptIphBchTo +"',");
                oSqlTmp.AppendLine("   '" + poAjp.ptIphZneTo +"',");
                oSqlTmp.AppendLine("   '" + poAjpInfo.ptPdtCode +"',");
                oSqlTmp.AppendLine("   '" + poAjpInfo.ptIpdBarCode + "',");
                oSqlTmp.AppendLine("   '" + poAjpInfo.ptPdtName +"',");
                oSqlTmp.AppendLine("   '" + poAjpInfo.ptPunCode + "',");
                oSqlTmp.AppendLine("   '" + poAjpInfo.pcIpdPriNew + "'");
                oSqlTmp.AppendLine(")");

                int nSucc =  oDatabase.C_DATnExecuteSql(oSqlTmp.ToString());
                if (nSucc != 0)
                {
                    oSqlTmp = new StringBuilder();
                    oSqlTmp.AppendLine(" SELECT FTPdtCode,FTPdtBarCode ");
                    oSqlTmp.AppendLine(" FROM TCNTPdtAjpTmp");
                    oSqlTmp.AppendLine(" WHERE FTPdtCode = '" + poAjpInfo.ptPdtCode + "' AND FTPdtBarCode = '" + poAjpInfo.ptIpdBarCode + "' ");
                    oDbtbl = new DataTable();
                    oDbtbl = oDatabase.C_DAToSqlQueryDt(oSqlTmp.ToString());
                    if (oDbtbl != null && oDbtbl.Rows.Count > 1)
                    {
                        C_DATxUptTempError(poAjpInfo.ptPdtCode, poAjpInfo.ptIpdBarCode, "706");
                    }

                    oSqlTmp = new StringBuilder();
                    oSqlTmp.AppendLine(" SELECT FTPdtStkCode ");
                    oSqlTmp.AppendLine(" FROM TCNMPdt");
                    oSqlTmp.AppendLine(" WHERE FTPdtStkCode = '" + poAjpInfo.ptPdtCode + "' AND ");
                    oSqlTmp.AppendLine(" FTPdtBarCode1 = '" + poAjpInfo.ptIpdBarCode + "' OR ");
                    oSqlTmp.AppendLine(" FTPdtBarCode2 = '" + poAjpInfo.ptIpdBarCode + "' OR ");
                    oSqlTmp.AppendLine(" FTPdtBarCode3 = '" + poAjpInfo.ptIpdBarCode + "' ");
                    tPdtCode = oDatabase.C_DAToSqlQuery<string>(oSqlTmp.ToString());

                    if (string.IsNullOrEmpty(tPdtCode))
                    {
                        C_DATxUptTempError(poAjpInfo.ptPdtCode, poAjpInfo.ptIpdBarCode, "710");
                   
                    }

             }

                oDatabase.C_DATxConnectiosClose();
                return true;

            }
            catch (Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oSqlTmp = null;
                oDatabase = null;
            }
        }

        public bool C_CHKbChekDataAdjust(cmlReqPdtAdj poAjp, cmlReqPdtAdjInfo poAjpInfo)
        {
            cSP oFunc;
            cMS oMsg;
            cDatabase oDatabase;
            StringBuilder oSql;
            DataTable oDbTbl;
            cProduct oPdt;
            try
            {
                oFunc = new cSP();
                oMsg = new cMS();
                oDatabase = new cDatabase();
                oSql = new StringBuilder();
                oPdt = new cProduct();

                C_DATxGenTableTemp(); //สร้าง Table Temp
                // Insert ค่าลง Temp
               if(C_DATxInsTemp(poAjp, poAjpInfo) == true)
                {
                    //Check ค่า Master 
                    if (poAjp.ptIphBchTo != null && poAjp.ptIphBchTo != "")
                    {
                        if (oPdt.C_DATbChkMaster("Branch", poAjp.ptIphBchTo) == false)
                        {
                            C_DATxUptTempError(poAjpInfo.ptPdtCode, poAjpInfo.ptIpdBarCode, "704");
                        }
                    }

                    if (poAjp.ptIphZneTo != null && poAjp.ptIphZneTo != "")
                    {
                        if (oPdt.C_DATbChkMaster("Zone", poAjp.ptIphZneTo) == false)
                        {
                            C_DATxUptTempError(poAjpInfo.ptPdtCode, poAjpInfo.ptIpdBarCode, "705");
                        }
                        else
                        {
                            C_DATxUptTempSuccess(poAjpInfo.ptPdtCode, poAjpInfo.ptIpdBarCode, "Zone", poAjp.ptIphZneTo);
                        }
                       
                    }

                    if (poAjpInfo.ptPunCode != null && poAjpInfo.ptPunCode != "")
                    {
                        if (oPdt.C_DATbChkMaster("Unit", poAjpInfo.ptPunCode) == false)
                        {
                            C_DATxUptTempError(poAjpInfo.ptPdtCode, poAjpInfo.ptIpdBarCode, "708");
                        }
                        {
                            C_DATxUptTempSuccess(poAjpInfo.ptPdtCode, poAjpInfo.ptIpdBarCode, "Unit", poAjpInfo.ptPunCode);
                        }
                    }
                    oDatabase.C_DATxConnectiosClose();
                    return true;
                }

                oDatabase.C_DATxConnectiosClose();
                return false;

            }
            catch (Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oFunc = null;
                oMsg = null;
                oDatabase = null;
                oSql = null;
            }
        }

        public bool C_DATxUptTempSuccess(string ptPdtCode, string ptPdtBarcode, string ptSuccessType,string ptValue)
        {
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSqlTmp;

            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();
                oSqlTmp = new StringBuilder();

                oSqlTmp.AppendLine(" UPDATE TCNTPdtAjpTmp");

                switch (ptSuccessType)
                {
                    case "Zone ":
                        oSqlTmp.AppendLine(" SET FTZneName = (SELECT FTZneName FROM TCNMZone WHERE  (FTZneCode = '" + ptValue + "'))  ");
                        break;
                    case "Unit":
                        oSqlTmp.AppendLine(" SET FTUnitName = (SELECT FTPunName FROM TCNMPdtUnit WHERE (FTPunCode = '" + ptValue  + "')) ");
                        break;
                }
                oSqlTmp.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "'  AND FTPdtBarCode = '" + ptPdtBarcode + "'");
                int nSucc = oDatabase.C_DATnExecuteSql(oSqlTmp.ToString());
                if (nSucc != 0)
                {
                    oDatabase.C_DATxConnectiosClose();
                    return true;
                }
                oDatabase.C_DATxConnectiosClose();
                return false;

            }
            catch (Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oSqlTmp = null;
                oDatabase = null;
            }
        }

        public bool C_DATxUptTempError(string ptPdtCode,string ptPdtBarcode,string ptErrorType)
        {
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSqlTmp;

            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();
                oSqlTmp = new StringBuilder();
              
                oSqlTmp.AppendLine(" UPDATE TCNTPdtAjpTmp");

                switch(ptErrorType)
                {
                    case "704":
                        oSqlTmp.AppendLine(" SET FTErrorBchCode = N'704' ");
                        break;
                    case "705":
                        oSqlTmp.AppendLine(" SET FTErrorZneCode = N'705' ");
                        break;
                    case "706":
                        oSqlTmp.AppendLine(" SET FTErrorBarcodeDup = N'706' ");
                        break;
                    case "707":
                        oSqlTmp.AppendLine(" SET FTErrorProductDup = N'707' ");
                        break;
                    case "708":
                        oSqlTmp.AppendLine(" SET FTErrorPdtUnit  = N'708' ");
                        break;
                    case "710":
                        oSqlTmp.AppendLine(" SET FTErrorPdtNotFound  = N'710' ");
                        break;
                    case "800":
                        oSqlTmp.AppendLine(" SET FTErrorData  = N'800' ");
                        break;
                }
                oSqlTmp.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "'  AND FTPdtBarCode = '" + ptPdtBarcode + "'");
                int nSucc = oDatabase.C_DATnExecuteSql(oSqlTmp.ToString());
                if (nSucc != 0)
                {
                    oDatabase.C_DATxConnectiosClose();
                    return true;
                }
                oDatabase.C_DATxConnectiosClose();
                return false;

            }
            catch (Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oSqlTmp = null;
                oDatabase = null;
            }
        }

        public bool C_DATbInsAjpHD()
        {
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSql;
            StringBuilder oSqlTmp;
            DataTable oDbtbl;
            string tIphStadoc = "";
            string tIphStsPrcDoc = "";
            string tIphStaPrcPri = "";
            string tDate = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd", new System.Globalization.CultureInfo("en-US"));
            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();


                oSqlTmp = new StringBuilder();
                oSqlTmp.AppendLine(" SELECT TOP(1) FTIphDocRef, ");
                oSqlTmp.AppendLine(" FDIphAffect, ");
                oSqlTmp.AppendLine(" FTIphDocRefRmk,");
                oSqlTmp.AppendLine(" FTIphPriType,");
                oSqlTmp.AppendLine(" FDIphDStop,");
                oSqlTmp.AppendLine(" FTIphBchTo,");
                oSqlTmp.AppendLine(" FTZneTo,");
                oSqlTmp.AppendLine(" FTPdtCode, ");
                oSqlTmp.AppendLine(" FTPdtBarCode,");
                oSqlTmp.AppendLine(" FTPdtName,");
                oSqlTmp.AppendLine(" FTtPunCode,");
                oSqlTmp.AppendLine(" FTIpdPriNew,");
                oSqlTmp.AppendLine(" (SELECT FTBchCode FROM TCNMBranch WHERE (FTBchHQ = N'1')) AS FTBchCode ");
                oSqlTmp.AppendLine(" FROM TCNTPdtAjpTmp ");
                oSqlTmp.AppendLine(" WHERE FTErrorBchCode  IS NULL AND");
                oSqlTmp.AppendLine(" FTErrorZneCode IS NULL     AND ");
                oSqlTmp.AppendLine(" FTErrorBarcodeDup  IS NULL   AND");
                oSqlTmp.AppendLine(" FTErrorProductDup  IS NULL    AND");
                oSqlTmp.AppendLine(" FTErrorPdtUnit  IS NULL    AND");
                oSqlTmp.AppendLine(" FTErrorData   IS NULL AND ");
                oSqlTmp.AppendLine(" FTErrorPdtNotFound IS NULL ");
                oDbtbl = new DataTable();
                oDbtbl = oDatabase.C_DAToSqlQueryDt(oSqlTmp.ToString());

                //for (int nRow = 0; nRow < oDbtbl.Rows.Count;nRow ++)
                //{
                //Check วันที่ใบปรับราคา

               

                    DateTime dAffort = Convert.ToDateTime(oDbtbl.Rows[0]["FDIphAffect"].ToString());
                    if (dAffort == DateTime.Now | dAffort <= DateTime.Now)
                    {

                    //กรณี BranchTo มีค่า เท่ากับ สำนักงานใหญ่
                    if(oDbtbl.Rows[0]["FTIphBchTo"].ToString() == oDbtbl.Rows[0]["FTBchCode"].ToString())
                    {
                        tIphStadoc = "1";
                        tIphStsPrcDoc = "1";
                        tIphStaPrcPri = "1";
                    }
                    else
                    {
                        tIphStadoc = "1";
                        tIphStsPrcDoc = "1";
                        tIphStaPrcPri = null;
                    }

                    }
                    else
                    {
                        tIphStadoc = "1";
                        tIphStsPrcDoc = "1";
                        tIphStaPrcPri = null;
                }

                    oSql = new StringBuilder();
                    oSql.AppendLine(" INSERT INTO TCNTPdtAjpHD(FTIphDocNo,FTIphDocType,FDIphDocDate");
                    oSql.AppendLine(",FTIphDocTime,FTBchCode,FTDptCode,FTUsrCode");
                    oSql.AppendLine(",FTIphApvCode,FCIphTotal,FTIphStaDoc,FTIphStaPrcDoc,FTIphStaPrcPri");
                    oSql.AppendLine(",FNIphStaDocAct,FTIphRmk,FTIphAdjType,FDIphAffect,FTIphBchTo,FTIphZneTo");
                    oSql.AppendLine(",FTIphPriType,FDIphDStop,FDDateUpd,FTTimeUpd,FTWhoUpd");
                    oSql.AppendLine(",FDDateIns,FTTimeIns,FTWhoIns) ");
                    oSql.AppendLine(" VALUES(");
                    oSql.AppendLine(" N'" + oDbtbl.Rows[0]["FTIphDocRef"].ToString() + "',");
                    oSql.AppendLine(" N'1',");
                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),");
                    oSql.AppendLine(" CONVERT(VARCHAR(8),GETDATE(),108),");
                    oSql.AppendLine(" (SELECT FTBchCode FROM TCNMBranch WHERE (FTBchHQ = N'1')),");
                    oSql.AppendLine(" N'001',");
                    oSql.AppendLine(" N'009',");
                    oSql.AppendLine(" N'009',");
                    oSql.AppendLine(" (SELECT SUM(FCIpdPriNew) FROM TCNTPdtAjpDT DT WHERE (DT.FTIphDocNo = '" + oDbtbl.Rows[0]["FTIphDocRef"].ToString() + "')) ,");
                    oSql.AppendLine(" '" + tIphStadoc + "',");
                    oSql.AppendLine(" '" + tIphStsPrcDoc + "',");
                    oSql.AppendLine(" '" + tIphStaPrcPri + "',");
                    oSql.AppendLine(" N'1',");
                    oSql.AppendLine(" '" + oDbtbl.Rows[0]["FTIphDocRefRmk"].ToString() + "',");
                    oSql.AppendLine(" N'1',");
                    oSql.AppendLine(" '" + oDbtbl.Rows[0]["FDIphAffect"].ToString() + "',");
                    oSql.AppendLine(" '" + oDbtbl.Rows[0]["FTIphBchTo"].ToString() + "',");
                    oSql.AppendLine(" '" + oDbtbl.Rows[0]["FTZneTo"].ToString() + "',");
                    oSql.AppendLine(" '" + oDbtbl.Rows[0]["FTIphPriType"].ToString() + "',");
                    oSql.AppendLine(" '" + oDbtbl.Rows[0]["FDIphDStop"].ToString() + "',");
                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),");
                    oSql.AppendLine(" CONVERT(VARCHAR(8),GETDATE(),108),");
                    oSql.AppendLine(" N'AdaLink', ");
                    oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),");
                    oSql.AppendLine(" CONVERT(VARCHAR(8),GETDATE(),108),");
                    oSql.AppendLine(" N'AdaLink' ");
                    oSql.AppendLine(" )");
                   int nSucc =  oDatabase.C_DATnExecuteSql(oSql.ToString());
                    if(nSucc == 0)
                    {
                    oDatabase.C_DATxConnectiosClose();
                    return false;
                    }

                //}
                oDatabase.C_DATxConnectiosClose();
                return true;

            }
            catch (Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oSql = null;
                oDatabase = null;
            }
        }
        public bool C_DATbInsAjpDT()
        {
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSql;
            StringBuilder oSqlTmp;
            DataTable oDbtbl;

            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();
                oSql = new StringBuilder();
                oSql.AppendLine(" SELECT FTIphDocRef,FTPdtCode");
                oSql.AppendLine(",FTPdtName,N'1',CONVERT(VARCHAR(10), GETDATE(), 121),N'001'");
                oSql.AppendLine(",FTPdtBarCode,FTPdtCode,N'1',FTtPunCode,FTUnitName");
                oSql.AppendLine(",N'1',N'1',(SELECT TOP(1) FCPdtRetPriS1 FROM TCNMPdt PDT WHERE(PDT.FTPdtStkCode = TMP.FTPdtCode)) AS FCPdtRetPriS1,FTIpdPriNew");
                oSql.AppendLine(",'1',FTIphBchTo,CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),108)");
                oSql.AppendLine(",'AdaLink',CONVERT(VARCHAR(10), GETDATE(), 121),CONVERT(VARCHAR(8),GETDATE(),108),'AdaLink' ");
                oSql.AppendLine(" FROM TCNTPdtAjpTmp TMP");
                oSql.AppendLine(" WHERE FTErrorBchCode  IS NULL AND");
                oSql.AppendLine(" FTErrorZneCode        IS NULL AND ");
                oSql.AppendLine(" FTErrorBarcodeDup     IS NULL AND");
                oSql.AppendLine(" FTErrorProductDup     IS NULL AND");
                oSql.AppendLine(" FTErrorPdtUnit        IS NULL AND");
                oSql.AppendLine(" FTErrorData           IS NULL AND ");
                oSql.AppendLine(" FTErrorPdtNotFound IS NULL");
                oDbtbl = oDatabase.C_DAToSqlQueryDt(oSql.ToString());
                if (oDbtbl !=null && oDbtbl.Rows.Count > 0)
                {
                    for (int nRow = 0; nRow < oDbtbl.Rows.Count; nRow++)
                    {
                        oSql = new StringBuilder();
                        oSql.Clear();
                        oSql.AppendLine(" INSERT INTO TCNTPdtAjpDT  WITH(ROWLOCK)  (FTIphDocNo,FNIpdSeqNo,FTPdtCode");
                        oSql.AppendLine(",FTPdtName,FTIphDocType,FDIphDocDate,FTBchCode");
                        oSql.AppendLine(",FTIpdBarCode,FTIpdStkCode,FCIpdStkFac,FTPunCode");
                        oSql.AppendLine(",FCIpdFactor,FNIpdAdjLev,FCIpdPriOld,FCIpdPriNew");
                        oSql.AppendLine(",FNIpdUnitType,FTIpdBchTo,FDDateUpd,FTTimeUpd");
                        oSql.AppendLine(",FTWhoUpd,FDDateIns,FTTimeIns,FTWhoIns,FTIpdUnitName) ");
                        oSql.AppendLine(" VALUES( ");
                        oSql.AppendLine(" '" + oDbtbl.Rows[nRow]["FTIphDocRef"].ToString() + "' ,");
                        oSql.AppendLine(" '" + (nRow + 1) +"',");
                        oSql.AppendLine(" '" + oDbtbl.Rows[nRow]["FTPdtCode"].ToString() + "', ");
                        oSql.AppendLine(" '" + oDbtbl.Rows[nRow]["FTPdtName"].ToString()  + "', ");
                        oSql.AppendLine(" N'1' ,");
                        oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121),");
                        oSql.AppendLine(" (SELECT FTBchCode FROM TCNMBranch WHERE (FTBchHQ = N'1')) , ");
                        oSql.AppendLine(" '" + oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString() + "', ");
                        oSql.AppendLine(" '" + oDbtbl.Rows[nRow]["FTPdtCode"].ToString() + "', ");
                        oSql.AppendLine(" N'1', ");
                        oSql.AppendLine("  '" + oDbtbl.Rows[nRow]["FTtPunCode"].ToString() + "', ");
                        oSql.AppendLine("  N'1', ");
                        oSql.AppendLine("  N'1', ");
                        oSql.AppendLine("   '" + C_GETtRetPriceOld(oDbtbl.Rows[nRow]["FTPdtCode"].ToString(), oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString()) + "', ");
                        oSql.AppendLine("  '" + oDbtbl.Rows[nRow]["FTIpdPriNew"].ToString() + "', ");
                        oSql.AppendLine("  N'1', ");
                        oSql.AppendLine("  '" + oDbtbl.Rows[nRow]["FTIphBchTo"].ToString() + "', ");
                        oSql.AppendLine("  CONVERT(VARCHAR(10), GETDATE(), 121), ");
                        oSql.AppendLine("  CONVERT(VARCHAR(8),GETDATE(),108), ");
                        oSql.AppendLine("  N'AdaLink', ");
                        oSql.AppendLine(" CONVERT(VARCHAR(10), GETDATE(), 121), ");
                        oSql.AppendLine(" CONVERT(VARCHAR(8),GETDATE(),108), ");
                        oSql.AppendLine("  N'AdaLink', ");
                        oSql.AppendLine("  '" + oDbtbl.Rows[nRow]["FTUnitName"].ToString() + "' ");
                        oSql.AppendLine(" ) ");

                        oDatabase.C_DATnExecuteSql(oSql.ToString());
                        oDatabase.C_DATxConnectiosClose();
                    }

                }

                oDatabase.C_DATxConnectiosClose();
                return true;

            }
            catch (Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oSql = null;
                oDatabase = null;
            }
        }

        public int C_GETtSeqNo(string ptPdtCode)
        {
            cDatabase oDatabase;
            StringBuilder oSql;
            DataTable oDbtbl;
            int nSeq = 1;
            int nCal;
            try
            {
                oSql = new StringBuilder();
                oDbtbl = new DataTable();
                oDatabase = new cDatabase();

                oSql.AppendLine(" SELECT TOP(1) FNIpdSeqNo ");
                oSql.AppendLine(" FROM TCNTPdtAjpDT");
                oSql.AppendLine(" WHERE FTPdtCode = '" + ptPdtCode + "'");
                oSql.AppendLine(" ORDER BY FTIphDocNo DESC ");
                oDbtbl = oDatabase.C_DAToSqlQueryDt(oSql.ToString());
                if(oDbtbl != null && oDbtbl.Rows.Count > 0)
                {
                    nCal = Convert.ToInt32(oDbtbl.Rows[0]["FNIpdSeqNo"]);
                    int nReSult = nSeq + nCal;
                    oDatabase.C_DATxConnectiosClose();
                    return nReSult;
                }
                oDatabase.C_DATxConnectiosClose();
                return nSeq;

            }
            catch
            {
                return nSeq;
            }
                
        }

        public string C_GETtRetPriceOld(string ptPdtCode,string ptPdtBarcode)
        {
            cDatabase oDatabase;
            StringBuilder oSql;
            StringBuilder oSqlRet;
            DataTable oDbtbl;
            DataTable oDbtblRet;
            string nResult = "";
            string nRet = "";
            try
            {
                oSqlRet = new StringBuilder();
                oSql = new StringBuilder();
                oDbtbl = new DataTable();
                oDbtblRet = new DataTable();
                oDatabase = new cDatabase();

                oSql.AppendLine(" SELECT ISNULL(FTPdtBarCode1,'') AS FTPdtBarCode1 , ");
                oSql.AppendLine(" ISNULL(FTPdtBarCode2,'') AS FTPdtBarCode2, ISNULL(FTPdtBarCode3,'') AS FTPdtBarCode3 ");
                oSql.AppendLine(" FROM TCNMpdt");
                oSql.AppendLine(" WHERE FTPdtStkCode = '" + ptPdtCode + "'");
                oSql.AppendLine(" AND FTPdtBarCode1 = '" + ptPdtBarcode + "' OR ");
                oSql.AppendLine("  FTPdtBarCode2 = '" + ptPdtBarcode + "' OR ");
                oSql.AppendLine("  FTPdtBarCode3 = '" + ptPdtBarcode + "' ");
                oDbtbl = oDatabase.C_DAToSqlQueryDt(oSql.ToString());
                if (oDbtbl != null && oDbtbl.Rows.Count > 0)
                {
                   if(oDbtbl.Rows[0]["FTPdtBarCode1"].ToString() == ptPdtBarcode)
                    {
                        nRet = "FCPdtRetPriS1";
                        oSqlRet.AppendLine(" SELECT TOP(1) FCPdtRetPriS1 FROM TCNMPdt ");
                        oSqlRet.AppendLine(" WHERE FTPdtStkCode = '" + ptPdtCode + "' AND FTPdtBarCode1 = '" + ptPdtBarcode + "' ");
                    }
                   else if(oDbtbl.Rows[0]["FTPdtBarCode2"].ToString() == ptPdtBarcode)
                    {
                        nRet = "FCPdtRetPriS2";
                        oSqlRet.AppendLine(" SELECT TOP(1) FCPdtRetPriS2 FROM TCNMPdt ");
                        oSqlRet.AppendLine(" WHERE FTPdtStkCode = '" + ptPdtCode + "' AND FTPdtBarCode2 = '" + ptPdtBarcode + "' ");
                    }
                   else if(oDbtbl.Rows[0]["FTPdtBarCode3"].ToString() == ptPdtBarcode)
                    {
                        nRet = "FCPdtRetPriS3";
                        oSqlRet.AppendLine(" SELECT TOP(1) FCPdtRetPriS3 FROM TCNMPdt ");
                        oSqlRet.AppendLine(" WHERE FTPdtStkCode = '" + ptPdtCode + "' AND FTPdtBarCode3 = '" + ptPdtBarcode + "' ");
                    }


                    oDbtblRet = oDatabase.C_DAToSqlQueryDt(oSqlRet.ToString());
                    if (oDbtblRet != null && oDbtblRet.Rows.Count > 0)
                    {
                        nResult = oDbtblRet.Rows[0][""+ nRet + ""].ToString();
                    }

                }

                oDatabase.C_DATxConnectiosClose();
                return nResult;

            }
            catch(Exception oExn)
            {
                throw oExn;
            }
        }
           
        public bool C_DATbUpdateProduct()
        {
            cSP oFunc;
            cDatabase oDatabase;
            StringBuilder oSql;
            StringBuilder oSqlPdt;
            StringBuilder oSqlTmp;
            DataTable oDbtbl;
            DataTable oDbtblPdt;

            try
            {
                oFunc = new cSP();
                oDatabase = new cDatabase();
                oSql = new StringBuilder();
                oSqlTmp = new StringBuilder();

             


                oSqlTmp.AppendLine(" SELECT FTPdtBarCode ,FTPdtCode,FTIpdPriNew,FTIphDocRef,");
                oSqlTmp.AppendLine(" (SELECT FTIphStaPrcDoc FROM TCNTPdtAjpHD HD WHERE FTIphDocRef = HD.FTIphDocNo) AS FTIphStaPrcDoc ");
                oSqlTmp.AppendLine(" FROM TCNTPdtAjpTmp ");
                oSqlTmp.AppendLine(" WHERE FTErrorBchCode  IS NULL AND");
                oSqlTmp.AppendLine(" FTErrorZneCode        IS NULL AND ");
                oSqlTmp.AppendLine(" FTErrorBarcodeDup     IS NULL AND");
                oSqlTmp.AppendLine(" FTErrorProductDup     IS NULL AND");
                oSqlTmp.AppendLine(" FTErrorPdtUnit        IS NULL AND");
                oSqlTmp.AppendLine(" FTErrorData           IS NULL");

                oDbtbl = new DataTable();
                oDbtbl = oDatabase.C_DAToSqlQueryDt(oSqlTmp.ToString());

                if(oDbtbl != null && oDbtbl.Rows.Count > 0)
                {
                    for(int nRow = 0; nRow < oDbtbl.Rows.Count;nRow ++)
                    {
                        if(oDbtbl.Rows[nRow]["FTIphStaPrcDoc"].ToString() == "1")
                        {
                            oSqlPdt = new StringBuilder();
                            oSqlPdt.AppendLine(" SELECT FTPdtBarCode1 ,FTPdtBarCode2,FTPdtBarCode3");
                            oSqlPdt.AppendLine(" FROM TCNMPdt");
                            oSqlPdt.AppendLine(" WHERE FTPdtStkCode = '" + oDbtbl.Rows[nRow]["FTPdtCode"].ToString() + "' AND ");
                            oSqlPdt.AppendLine(" FTPdtBarCode1 = '" + oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString() + "' ");
                            oSqlPdt.AppendLine(" OR FTPdtBarCode2 = '" + oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString() + "'  ");
                            oSqlPdt.AppendLine(" OR FTPdtBarCode3 = '" + oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString() + "' ");
                            oDbtblPdt = new DataTable();
                            oDbtblPdt = oDatabase.C_DAToSqlQueryDt(oSqlPdt.ToString());

                            oSql.Clear();
                            oSql.AppendLine(" UPDATE TCNMPdt");
                            if (oDbtblPdt.Rows[0]["FTPdtBarCode1"].ToString() == oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString())
                            {
                                oSql.AppendLine(" SET FCPdtRetPriS1 = '" + oDbtbl.Rows[nRow]["FTIpdPriNew"].ToString() + "'");
                                oSql.AppendLine(" WHERE (FTPdtBarCode1 ='" + oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString() + "')  ");
                            }
                            else if (oDbtblPdt.Rows[0]["FTPdtBarCode2"].ToString() == oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString())
                            {
                                oSql.AppendLine(" SET FCPdtRetPriS2  = '" + oDbtbl.Rows[nRow]["FTIpdPriNew"].ToString() + "'");
                                oSql.AppendLine(" WHERE (FTPdtBarCode2 = '" + oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString() + "')  ");
                            }
                            else if (oDbtblPdt.Rows[0]["FTPdtBarCode3"].ToString() == oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString())
                            {
                                oSql.AppendLine(" SET FCPdtRetPriS3  = '" + oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString() + "'");
                                oSql.AppendLine(" WHERE (FTPdtBarCode3 ='" + oDbtbl.Rows[nRow]["FTPdtBarCode"].ToString() + "')  ");
                            }

                            oSql.AppendLine(" AND (FTPdtStkCode = '" + oDbtbl.Rows[nRow]["FTPdtCode"].ToString() + "')  ");

                            oDatabase.C_DATnExecuteSql(oSql.ToString());
                        }
                      
                    }
                   
                }

                int nSucc = oDatabase.C_DATnExecuteSql(oSql.ToString());
                if (nSucc == 0)
                {
                    oDatabase.C_DATxConnectiosClose();
                    return false;
                }
                oDatabase.C_DATxConnectiosClose();
                return true;

            }
            catch (Exception oExn)
            {
                return false;
                throw oExn;
            }
            finally
            {
                oSql = null;
                oDatabase = null;
            }

        }

        public void C_DATxDropTableTmp()
        {
            StringBuilder oSql;
            cDatabase oDatabase;
            try
            {
                oDatabase = new cDatabase();
                oSql = new StringBuilder();
                oSql.AppendLine(" DROP TABLE TCNTPdtAjpTmp");
                oDatabase.C_DATnExecuteSql(oSql.ToString());
            }
            catch(Exception oExn)
            {

            }
          
        }

       
    }
}
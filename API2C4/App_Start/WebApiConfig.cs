﻿using API2C4.Areas.HelpPage;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API2C4
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //*[BITTER][][2018-07-19]
            // Force return json format.
            config.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings();

            //*[BITTER][][2018-07-19]
            // Set api document.
            config.SetDocumentationProvider(new XmlDocumentationProvider
                (HttpContext.Current.Server.MapPath("App_Data/API2C4.XML")));
        }
    }
}
